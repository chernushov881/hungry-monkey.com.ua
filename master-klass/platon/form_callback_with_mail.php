<?php

// client pass to verify signature
$pass = 'PASSWORD';

$log = __DIR__ . '/callback.log';

if (! $_POST) die("ERROR: Empty POST");

// log callback data
file_put_contents($log, var_export($_POST, 1) . "\n\n", FILE_APPEND);

$callbackParams = $_POST;

// generate signature from callback params
$sign =  md5(strtoupper(
		strrev($callbackParams['email']) .
		$pass .
		$callbackParams['order'] .
		strrev(substr($callbackParams['card'], 0, 6) . substr($callbackParams['card'], -4))
));

// verify signature
if ($callbackParams['sign'] !== $sign) {
	// log failure
	file_put_contents($log, "Invalid signature" . "\n\n", FILE_APPEND);

	// answer with fail response
	die("ERROR: Invalid signature");
	
} else {
	// log success
	file_put_contents($log, "Callback signature OK" . "\n\n", FILE_APPEND);

	// do processing stuff
	switch ($callbackParams['status']) {
		case 'SALE':
			file_put_contents($log, "Order {$callbackParams['order']} processed as successfull sale\n\n", FILE_APPEND);			
			// send mail
			$to = "president@whitehouse.us"; 
			$subject = "Заказ №{$callbackParams['order']}";
			$message = "Заказ №{$callbackParams['order']} на сумму {$callbackParams['amount']}{$callbackParams['currency']} успешно оплачен.";
			$headers = "From: example@site.com";
			if (mail($to, $subject, $message, $headers)) {
				file_put_contents($log, "Mail was successfully sent\n\n", FILE_APPEND);
			}
			break;
		case 'REFUND':
			file_put_contents($log, "Order {$callbackParams['order']} processed as successfull refund\n\n", FILE_APPEND);			
			// send mail
			$to = "president@whitehouse.us";
			$subject = "Заказ №{$callbackParams['order']}";
			$message = "По заказу №{$callbackParams['order']} успешный возврат средств.";
			$headers = "From: example@site.com";
			if (mail($to, $subject, $message, $headers)) {
				file_put_contents($log, "Mail was successfully sent\n\n", FILE_APPEND);
			}
			break;
		case 'CHARGEBACK':
			file_put_contents($log, "Order {$callbackParams['order']} processed as successfull chargeback\n\n", FILE_APPEND);
			break;
		default:
			file_put_contents($log, "Invalid callback data\n\n", FILE_APPEND);
			die("ERROR: Invalid callback data");
	}

	// answer with success response
	exit("OK");
}

