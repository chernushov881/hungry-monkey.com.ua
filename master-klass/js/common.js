$(function() {

	// $('.screen-5 h2').click(function(){
	// 	$('.screen-5-list-wrapper').slideToggle(600);
	// });

	// ANIMATION CSS
	$('.screen-1 span.screen-1-span-1, .screen-1 .col-md-3, .screen-1 .col-md-9').animated('bounceInRight');
	$('.screen-1 .border-container').animated('bounceInLeft');
	$('.screen-2 h2').animated('fadeInDown');
	$('.screen-2 .screen-2-left-ul').animated('fadeInLeft');
	$('.screen-2 .screen-2-right-ul').animated('fadeInRight');
	$('.screen-3 h2').animated('flipInX');
	$('.screen-4 .screen-4-cards').animated('flipInY');
	$('.screen-6-btn').animated('bounce');
	$('.screen-7 h2').animated('fadeInDown');
	$('.screen-7 .screen-7-image-anim').animated('fadeInLeft');
	$('.screen-7 .screen-7-text-anim').animated('fadeInRight');
	// $('.screen-8-reviews-button-prew').animated('bounceInLeft');
	// $('.screen-8-reviews-button-next').animated('bounceInRight');
	$('.screen-8 h2').animated('fadeInDown');
	$('.screen-6-wrapper-elem').animated('zoomIn');
	$('.screen-6-slogan').animated('zoomInDown');

	$(".user_phone").mask("+38(999) 999-9999");
	$(".header-master-klass-btn, .screen-6-btn").on("click", function(){
		target = $("#main_form");
		$('html, body').animate({scrollTop: $(target).offset().top}, 1000);
	});

//code from prev projects
// GEOLOCATION
// $('form button').click(function(){
// 	var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
// 	$("#geoloc").val(loc);
// });


//FORM VALIDATION + AJAX SEND
function get_cookie ( cookie_name ){
	var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

	if ( results )
		return ( unescape ( results[2] ) );
	else
		return null;
}

var href = document.location.href;
var new_url = href.split('?')[1];
var url = href.split('?')[0];
var utm_catch = '&' + new_url + "&page_url="+url;
var ref = '&ref=' + document.referrer;
var leade_name;
$('#grow_business, #footer_callback_button, #change_situation, #scale_business, #register_now, #top_callback_button').click(function(){
	var id = $(this).attr('id');
	leade_name = "&lead_name="+id;
});
var lead_price = "&lead_price=" + $('#price').html();

var invite_id = "&invite_id="+href.split('invite_id=')[1];
var cook_ga;
var hmid;
var hmid_link;

		// присваиваем hmid и ложим в базу
$(document).ready(function(){

	var cookie_check = setInterval(function(){
		var ga = get_cookie('_ga');
		if (ga == null) {
			console.log('111');
		}
		else {
			cook_ga = "&_ga="+get_cookie('_ga');
			hmid =  cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
			hmid_link =  "?&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
			// var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
			var page_url = "&page_url="+url;
			var temp_date = new Date();
			var temp_month = temp_date.getMonth();
			temp_month++;
			var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
			var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
			var data = hmid_link+utm_catch+invite_id+date_submitted+time_submitted+ref;
			clearInterval(cookie_check);
			$.ajax({
						type: "GET",
						url: "id.php",
						data: data,
						success: function() {
				console.log('done');
						}
					});
		}
	} ,500);
});
$.validate({
	form : 'form',
	addValidClassOnAll : true,
	borderColorOnError: "#eca29b ",
	onSuccess : function($form){
			 if ($form.attr('id') == 'main_form' ) {
					var data = $("#main_form").serialize();

				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				if (cook_ga === void 0) {
					var hmid_f = 'analitics-blocked';
				}else{
					var hmid_f =  "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				}
				data += utm_catch;
				data += date_submitted;
				data += time_submitted;
				data += ref;
				data += cook_ga;
				data += hmid_f;

				$.ajax({
					type: "GET",
					url: "register_mail.php",
					data: data,
					success: function() {
						console.log('register_mail ok');
					}
				});
				$.ajax({
					type: "POST",
					url: "invite_id.php",
					data: data,
					success: function() {
						console.log('invite_id ok');
					}
				});
				$.ajax({
					type: "POST",
					url:"amo/amocontactlist.php",
					data: data,
					success: function() {
						console.log('amocontactlist ok!');
						window.location.href = "thanks.html";
					}
				});

			return false;
			}
	}

});

});

$(document).ready(function() {

	var owl = $("#owl-carousel");
	$("#owl-carousel").owlCarousel({
			slideSpeed : 500,
			items : 1,
			loop: true
	});
// Custom Navigation Events
	$(".screen-8-reviews-button-prew").click(function(){
		owl.trigger('prev.owl.carousel');
	})
	$(".screen-8-reviews-button-next").click(function(){
		owl.trigger('next.owl.carousel');
	});
});
