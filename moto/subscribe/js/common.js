$(function() {

	//AOS Animation
	AOS.init({
		disable: 'mobile',
		once: 'true',
		offset: 100
	});

	//MASK INPUT
	$(".user_phone").mask("+38(099) 999-9999");

	//FORM VALIDATION + AJAX SEND
	function get_cookie ( cookie_name ){
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}

	var href = document.location.href;
	var new_url = href.split('?')[1];
	var id = 'Moto_Subscribe';
	var url = href.split('?')[0];
	var utm_catch = '&' + new_url + "&page_url=" + url;
	var ref = '&ref=' + document.referrer;
	var leade_name = "&lead_name=" + id;
	var lead_price = "&lead_price=" + $('#price').html();
	var invite_id = "&invite_id="+href.split('invite_id=')[1];
	var cook_ga;
	var hmid;
			// присваиваем hmid и ложим в базу
	$(document).ready(function(){
		var cookie_check = setInterval(function(){
			var ga = get_cookie('_ga');
			if (ga === null) {
				console.log('111');
			}
			else {
				cook_ga = "&_ga="+get_cookie('_ga');
				var hmid = "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
				var page_url = "&page_url="+url;
				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+"."+temp_month+"." +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				var data = hmid+utm_catch+invite_id+date_submitted+time_submitted+ref+loc;
				clearInterval(cookie_check);
				$.ajax({
							type: "GET",
							url: "visits.php",
							data: data,
							success: function() {
					console.log('done');
							}
						});
			}
		} ,500);
	});
	$.validate({
		form : 'form',
		addValidClassOnAll : true,
		borderColorOnError : '',
		onSuccess : function($form){
			var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
			$form.find('#geoloc').val(loc);
			var data = $form.serialize();
			var data_form = $form.attr('data-form');
			var temp_date = new Date();
			var temp_month = temp_date.getMonth();
			temp_month++;
			var date_submitted = '&date_submitted=' +temp_date.getDate()+"."+temp_month+"." +temp_date.getFullYear();
			var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
			data += utm_catch;
			data += date_submitted;
			data += time_submitted;
			data += ref;
			data += cook_ga;
			data += leade_name;
			data += '&data_form=' + data_form;
			data += '&hmid=' + hmid;
			$.ajax({
				type: "GET",
				url: "register_mail.php",
				data: data,
				beforeSend: function() {
					$form.find('button').prop( "disabled", true ).text('');
					$form.find('.spinner').fadeIn();
				}
			});
			$.ajax({
				type: "POST",
				url:"amo/amocontactlist.php",
				data: data,
				success: function() {
					$form.find('.spinner').fadeOut();
					setTimeout(function() {
						$form.find('button').text('✔ Отправлено');
					}, 350);
					// dataLayer.push({'event': 'FormSumit_main', 'form_type': data_form});
				}
			});
		return false;
		}
	});

});
