$(function() {

	//Scroll to bottom form
	$('.header-btn button').click(function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop: $('footer form').offset().top}, 800);
	});

	//Fix Blinking menu on load page
	$(document).ready(function(){
		$('header .header-head .header-nav').css('transition', 'transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0)');
	});

	//Menu
	$('#menuToggle').click(function(){
		$('#menuToggle input').prop('checked') ? $('header').addClass('active-menu') : $('header').removeClass('active-menu');
	});

	//Pop-up form
	$('.show-pop-up').click(function(e){
		e.preventDefault();
		$('.pop-up-form-wrap').addClass('pop-up-active');
		var moto_type = $(this).text();
		$('.pop-up-form-wrap form').data('form', 'pop-up-form ' + moto_type).attr('data-form', 'pop-up-form ' + moto_type);
		$('.pop-up-form-wrap form button').text(moto_type);
	});
	$('.pop-up-form-close').click(function(){
		$('.pop-up-form-wrap').removeClass('pop-up-active');
	});

	//Callback pop-up
	$('.hader-callme-btn').click(function(e){
		e.preventDefault();
		$('.callback-form-wrap').addClass('callback-active');
	});
	$('.callback-form-close').click(function(){
		$('.callback-form-wrap').removeClass('callback-active');
	});

	//Moto section slider
	$('.moto-bikes-col:first-child img, .moto-bikes-col:last-child img').click(function(){
		if ($(window).width() > 767) {
			var active_img = $('.active-moto-img img').attr('src'),
				current_img = $(this).attr('src'),
				current_number_img = $(this).attr('data-number'),
				active_number_img = $('.active-moto-img img').attr('data-number'),
				active_model = $('.active-moto-img figcaption').text();
				current_model = $(this).next('figcaption').text(),
				old_descr = $('.active-moto-descr').text();
				new_descr = $(this).siblings('.moto-bikes-descr').text();
			console.log(new_descr);
			console.log(old_descr);
			$('.active-moto-descr').text(new_descr);
			$(this).siblings('.moto-bikes-descr').text(old_descr);
			$(this).attr('src', active_img).attr('data-number', active_number_img).next('figcaption').text(active_model);
			$('.active-moto-img img').attr('src', current_img).attr('data-number', current_number_img).next('figcaption').text(current_model);
		}
	});

	//Hide pop-ups on ESC
		$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$('.pop-up-form-wrap').removeClass('pop-up-active');
			$('.callback-form-wrap').removeClass('callback-active');
		}
	});

	//Hide pop-ups on outside click
		$(document).mouseup(function(e) {
			if ($(e.target).hasClass("callback-form-wrap") || $(e.target).hasClass("pop-up-form-wrap") || $(e.target).hasClass("active-menu")) {
			$('.callback-form-wrap').removeClass('callback-active');
			$('.pop-up-form-wrap').removeClass('pop-up-active');
			$('header').removeClass('active-menu');
			$('#menuToggle input').prop('checked', false);
			};
		});

	//Mobile menu navigation
	$('nav ul a[href^="#"]').on('click',function (e) {
		e.preventDefault();
		var target = this.hash;
		var $target = $(target);
		var scroll = $target.offset().top;
		$('html, body').animate({
			'scrollTop': scroll
		}, 700, 'swing');
		$('header').removeClass('active-menu');
		$('#menuToggle input').prop('checked', false);
	});

	//MASK INPUT
	$(".user_phone").mask("+38(999) 999-9999");

	//Lights Slider
	$(document).ready(function(){
		var slider = $(".testimonials-slider");
		slider.lightSlider({
			item: 1,
			controls: false,
			// pager: false,
			adaptiveHeight: true,
			slideMargin: 30,
		});
	});

	//FORM VALIDATION + AJAX SEND
	function get_cookie ( cookie_name ){
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}

	var href = document.location.href;
	var new_url = href.split('?')[1];
	var url = href.split('?')[0];
	var utm_catch = '&' + new_url + "&page_url=" + url;
	var ref = '&ref=' + document.referrer;
	var lead_price = "&lead_price=" + $('#price').html();
	var invite_id = "&invite_id="+href.split('invite_id=')[1];
	var cook_ga;
	var hmid;
	var leade_name;
			// присваиваем hmid и ложим в базу
	$(document).ready(function(){
		var cookie_check = setInterval(function(){
			var ga = get_cookie('_ga');
			if (ga === null) {
				console.log('111');
			}
			else {
				cook_ga = "&_ga="+get_cookie('_ga');
				var hmid = "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				// var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
				var page_url = "&page_url="+url;
				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+"."+temp_month+"." +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				var data = hmid+utm_catch+invite_id+date_submitted+time_submitted+ref;
				clearInterval(cookie_check);
				$.ajax({
							type: "GET",
							url: "visits.php",
							data: data,
							success: function() {
					console.log('done');
							}
						});
			}
		} ,500);
	});
	$.validate({
		form : 'form',
		addValidClassOnAll : true,
		onSuccess : function($form){
			// var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
			// $form.find('#geoloc').val(loc);
			var data = $form.serialize();
			var data_form = $form.attr('data-form');
			var data_button = $form.find('button').text();
			var temp_date = new Date();
			var temp_month = temp_date.getMonth();
			temp_month++;
			var date_submitted = '&date_submitted=' +temp_date.getDate()+"."+temp_month+"." +temp_date.getFullYear();
			var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
			data += utm_catch;
			data += date_submitted;
			data += time_submitted;
			data += ref;
			data += cook_ga;
			data += leade_name;
			data += '&data_form=' + data_form;
			data += '&hmid=' + hmid;
			data += '&lead_name=' + data_button;
			$.ajax({
				type: "GET",
				url: "register_mail.php",
				data: data,
				beforeSend: function() {
					$form.find('button').prop( "disabled", true ).text('');
					$form.find('.spinner').fadeIn();
				}
			});
			$.ajax({
				type: "POST",
				url:"amo/amocontactlist.php",
				data: data,
				success: function() {
					$form.find('.spinner').fadeOut();
					setTimeout(function() {
						$form.find('button').text('✔ Отправлено');
					}, 350);
					// dataLayer.push({'event': 'FormSumit_main', 'form_type': data_form});
				}
			});
		return false;
		}
	});

});
