$(function() {

	//SMOOTH SCROLL
	$(".header-btn").on("click", function(){
		target = $("#main_form");
		$('html, body').animate({scrollTop: $(target).offset().top}, 1000);
	});

	$('section.programm h3').on('click', function(){
		$(this).next('ol').slideToggle(280);
	});

	// INPUT MASK
	$(".user_phone").mask("+38(999) 999-9999");

	// //OWL CARIUSEL
	// $('.testimonials-slider').owlCarousel({
	// 	items:3,
	// 	loop:true
	// });
	//code from prev projects
	// GEOLOCATION
	// $('form button').click(function(){
	// 	var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
	// 	$("#geoloc").val(loc);
	// });


	//FORM VALIDATION + AJAX SEND
	function get_cookie ( cookie_name ){
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}

	var href = document.location.href;
	var new_url = href.split('?')[1];
	var url = href.split('?')[0];
	var utm_catch = '&' + new_url + "&page_url="+url;
	var ref = '&ref=' + document.referrer;
	var id = "shkiper";
	var leade_name = "&lead_name=" + id;
	var lead_price = "&lead_price=" + $('#price').html();
	var invite_id = "&invite_id="+href.split('invite_id=')[1];
	var cook_ga;
	var hmid;
	var hmid_link;

			// присваиваем hmid и ложим в базу
	$(document).ready(function(){

		var cookie_check = setInterval(function(){
			var ga = get_cookie('_ga');
			if (ga === null) {
				console.log('111');
			}
			else {
				cook_ga = "&_ga="+get_cookie('_ga');
				hmid =  cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				hmid_link =  "?&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				// var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
				var page_url = "&page_url="+url;
				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				var data = hmid_link+utm_catch+invite_id+date_submitted+time_submitted+ref;
				clearInterval(cookie_check);
				$.ajax({
							type: "GET",
							url: "visits.php",
							data: data,
							success: function() {
					console.log('done');
							}
						});
			}
		} ,500);
	});
	$.validate({
		form : 'form',
		addValidClassOnAll : true,
		borderColorOnError: "#eca29b ",
		onSuccess : function($form){
				 if ($form.attr('id') == 'main_form' ) {
						var data = $("#main_form").serialize();

					var temp_date = new Date();
					var temp_month = temp_date.getMonth();
					temp_month++;
					var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
					var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();

					data += utm_catch;
					data += date_submitted;
					data += time_submitted;
					data += ref;
					data += cook_ga;
					data += leade_name;

					$.ajax({
						type: "GET",
						url: "register_mail.php",
						data: data,
						success: function() {
							console.log('register_mail ok');
						}
					});
					$.ajax({
						type: "POST",
						url:"amo/amocontactlist.php",
						data: data
					});
				// setTimeout(function() {
				// 			window.location.href = "thanks.html";
				// 	}, 1500);

				return false;
				}
		}

	});

	//ANIMATE CSS
	$('.about-kurs-col-l').animated('slideInLeft');
	$('.about-kurs-col-r').animated('slideInRight');
	$('.shkip-work-img').animated('flipInY');
	$('.shkip-work-text').animated('fadeIn');
	$('.header-icon-block-question').animated('bounceInLeft');
	$('.header-icon-block-answer').animated('bounceInRight');
	$('.work-icon, .work-text').animated('fadeIn');
	$('.miffs-cols').animated('fadeIn');
	// $('.header-bt-wrap').animated('fadeIn');
});
