// GEOLOCATION
$('form button').click(function(){
  var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
  $("#geoloc").val(loc);
});


//Плавный скролл до блока по клику
$("#header_button").click(function() {
	var offset = $('.main-footer').offset();
	$("html, body").animate({
	  scrollTop:offset.top
	}, 500);
	return false;
});
//MASKED INPUT
jQuery(function($){
$(".user_phone").mask("+38(099) 999-9999");
});

//FORM ANIMATION
$(document).ready(function(){
  var winHeight = $(window).height();
  var winCenter = winHeight/2;
  var item = $('#key_popup .form__screen');
  var itemHeight = $(item).outerHeight()+120;
  var itemLenght = $(item).size();
  var itemCenter = itemHeight/2;
  var totalCenter = winCenter-itemCenter;
  var activeItem = $('#key_popup .form__screen.active');
  var wrapper = $('#key_popup .form__wrapper');
  var okbtn =  $(activeItem).find('.btn_ok'); 
  var prevCount = 1;
  
  $(wrapper).css({
    '-webkit-transform' : 'translateY(' + totalCenter + 'px)',
    '-moz-transform'    : 'translateY(' + totalCenter + 'px)',
    '-ms-transform'     : 'translateY(' + totalCenter + 'px)',
    '-o-transform'      : 'translateY(' + totalCenter + 'px)',
    'transform'         : 'translateY(' + totalCenter + 'px)'
  });
  
  

  $('#key_popup  .btn_ok').click(function(){
    function get_cookie ( cookie_name )
    {
      var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
     
      if ( results )
        return ( unescape ( results[2] ) );
      else
        return null;
    }

    var btnParent = $(this).parents('.form__screen');
    var inpt = $(this).siblings('.input__container').find('input');

   var href = document.location.href;
  var new_url = href.split('?')[1];
  var url = href.split('?')[0];
  var utm_catch = "&page_url="+url;
  var ref = '&ref=' + document.referrer;
  var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
  var city = "&city=" +loc;
  var cook_ga = "&_ga="+get_cookie('_ga');
  var temp_date = new Date();
  var temp_month = temp_date.getMonth();
  temp_month++;
  var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
  var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
  var formData = "";
  formData += utm_catch;
  formData += date_submitted;
  formData += time_submitted;
  formData += ref;
  formData += city; 
  formData += cook_ga; 
  formData += "&"+new_url;

    function checkInput(i){

      var val =  $(i).val();
      var name = $(i).attr('name');

      function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
      }

      if(name == "name") {
        if (val == "" ) {
            $(i).parents('.form__screen').addClass('error__input');
            return false;
        }
        else {
              return true;
        }
      }

      if(name == "mail") {
        if (val == "" ) {
            $(i).parents('.form__screen').addClass('error__input');
            return false;
        }
        if (!isValidEmailAddress(val)) {
            $(i).parents('.form__screen').addClass('error__input');
            return false;
        }
        if (isValidEmailAddress(val)) {
            $(i).parents('.form__screen').removeClass('error__input');
            return true;
        }
      }

      if(name == "phone") {
        console.log(l);
        var l = $(i).val().length;
        if (l < 17) {
          $(i).parents('.form__screen').addClass('error__input');
          return false;
        }
        else {
          $(i).parents('.form__screen').removeClass('error__input');
          return true;
        }
      }
      if(name == "key") {
        var l = $(i).val().length;
        if (l < 3) {
          $(i).parents('.form__screen').addClass('error__input');
          return false;
        }
        else {
          $(i).parents('.form__screen').removeClass('error__input');
          var data = $('#key_popup form').serialize();
          var sendData = data+formData;
          $.ajax({
            url:"register_mail.php",
            type:"POST",
            data:sendData
          });
          $.ajax({
            url:"keys.php",
            type: "POST",
            data:data,
            dataType:"text",
            success: function(result){
                $(i).parents('.form__screen').addClass('error__input');
                $(i).parents('.form__screen').find('.ribbon').html('Не правильный код');
                
                if (result) {
                  console.log('111');
                  $(i).parents('.form__screen').removeClass('error__input');
                  $.ajax({
                    url:"lead_with_key.php",
                    type:"POST",
                    data:sendData                    
                  });
                  $.ajax({
                    url:"remove.php",
                    type:"GET",
                    data:data,
                    dataType:"text",
                    success: function(){
                      setTimeout(function(){
                        window.location.href = 'thanks.html'
                      }, 1000);
                    }
                  });
                }
            }
          });
        }
      }


    }

    
    if(checkInput(inpt)) {
      if ($(btnParent).hasClass('active')) {
        var tmpCount = $(btnParent).prevAll().size(); 
        if (tmpCount >= 1) {
          prevCount = $(btnParent).prevAll().size()+1;
        }
        else  {
          prevCount = 1;
        }
        $(btnParent).next().addClass('active').removeClass('not_visible');
        $(btnParent).removeClass('active error__input').addClass('transparent');
        console.log(prevCount);
        $(wrapper).css({
          '-webkit-transform' : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
          '-moz-transform'    : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
          '-ms-transform'     : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
          '-o-transform'      : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
          'transform'         : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)'
        });
      }
    }   
  });

    $('#key_popup  .form__wrapper').click(function(e){
        var trgt = e.target;
        var trgtP = $(trgt).parents('.form__screen');
        if ($(trgt).hasClass('transparent')) {

          prevCount = $(trgt).prevAll().size();

          $(trgt).next().addClass('not_visible').removeClass('active');
          $(trgt).removeClass('transparent').addClass('active');
          console.log(prevCount);
          $(wrapper).css({
            '-webkit-transform' : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
            '-moz-transform'    : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
            '-ms-transform'     : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
            '-o-transform'      : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
            'transform'         : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)'
          });
        }
  });


  $('form').submit(function(){
    return false;
  });
});

//МНЕ ПОВЕЗЕТ
$(document).ready(function(){
  var winHeight = $(window).height();
  var winCenter = winHeight/2;
  var item = $('#luck_popup .form__screen');
  var itemHeight = $(item).outerHeight()+120;
  var itemLenght = $(item).size();
  var itemCenter = itemHeight/2;
  var totalCenter = winCenter-itemCenter;
  var activeItem = $('#luck_popup .form__screen.active');
  var wrapper = $('#luck_popup .form__wrapper');
  var okbtn =  $(activeItem).find('.btn_ok'); 
  var prevCount = 1;
  
  $(wrapper).css({
    '-webkit-transform' : 'translateY(' + totalCenter + 'px)',
    '-moz-transform'    : 'translateY(' + totalCenter + 'px)',
    '-ms-transform'     : 'translateY(' + totalCenter + 'px)',
    '-o-transform'      : 'translateY(' + totalCenter + 'px)',
    'transform'         : 'translateY(' + totalCenter + 'px)'
  });
  
  

  $('#luck_popup  .btn_ok').click(function(){
    function get_cookie ( cookie_name )
    {
      var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
     
      if ( results )
        return ( unescape ( results[2] ) );
      else
        return null;
    }




    var btnParent = $(this).parents('.form__screen');
    var inpt = $(this).siblings('.input__container').find('input');

   var href = document.location.href;
  var new_url = href.split('?')[1];
  var url = href.split('?')[0];
  var page_url = "&page_url="+url;
  var ref = '&ref=' + document.referrer;
  var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
  var city = "&city=" +loc;
  var cook_ga = "&_ga="+get_cookie('_ga');
  var temp_date = new Date();
  var temp_month = temp_date.getMonth();
  temp_month++;
  var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
  var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
  var luckData = "";
  luckData += page_url;
  luckData += date_submitted;
  luckData += time_submitted;
  luckData += ref;
  luckData += city; 
  luckData += cook_ga; 
  luckData += "&"+new_url; 

    function checkInput(i){

      var val =  $(i).val();
      var name = $(i).attr('name');

      function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
      }

      if(name == "name") {
        if (val == "" ) {
            $(i).parents('.form__screen').addClass('error__input');
            return false;
        }
        else {
              return true;
        }
      }

      if(name == "mail") {
        if (val == "" ) {
            $(i).parents('.form__screen').addClass('error__input');
            return false;
        }
        if (!isValidEmailAddress(val)) {
            $(i).parents('.form__screen').addClass('error__input');
            return false;
        }
        if (isValidEmailAddress(val)) {
            $(i).parents('.form__screen').removeClass('error__input');
            return true;
        }
      }

      if(name == "phone") {
        console.log(l);
        var l = $(i).val().length;
        if (l < 17) {
          $(i).parents('.form__screen').addClass('error__input');
          return false;
        }
        else {
          $(i).parents('.form__screen').removeClass('error__input');
          var data = $('#luck_popup form').serialize();
          var sendData = data+luckData;
          $.ajax({
            url:"luck_mail.php",
            type:"POST",
            data:sendData,
            success: function(){
              setTimeout(function(){
                window.location.href = 'thanks_luck.html'
              }, 1000);
            }
          });
        }
      }

    }

    
    if(checkInput(inpt)) {
      if ($(btnParent).hasClass('active')) {
        var tmpCount = $(btnParent).prevAll().size(); 
        if (tmpCount >= 1) {
          prevCount = $(btnParent).prevAll().size()+1;
        }
        else  {
          prevCount = 1;
        }
        $(btnParent).next().addClass('active').removeClass('not_visible');
        $(btnParent).removeClass('active error__input').addClass('transparent');
        console.log(prevCount);
        $(wrapper).css({
          '-webkit-transform' : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
          '-moz-transform'    : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
          '-ms-transform'     : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
          '-o-transform'      : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
          'transform'         : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)'
        });
      }
    }


  });

  /*$('#luck_popup  .form__wrapper').click(function(e){
        var trgt = e.target;


        if ($(trgt).hasClass('transparent')) {

          prevCount = $(trgt).prevAll().size();

          $(trgt).next().addClass('not_visible').removeClass('active');
          $(trgt).removeClass('transparent').addClass('active');
          console.log(prevCount);
          $(wrapper).css({
            '-webkit-transform' : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
            '-moz-transform'    : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
            '-ms-transform'     : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
            '-o-transform'      : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)',
            'transform'         : 'translateY(' + (totalCenter-(itemHeight*prevCount)) + 'px)'
          });
        }

  });*/


  $('form').submit(function(){
    return false;
  });
});

//POPUP
$('#key_button, #key_popup .btn__close').click(function(){
  $('#key_popup').toggleClass('visible');
});
$('#luck_button, #luck_popup .btn__close').click(function(){
  $('#luck_popup').toggleClass('visible');
});

//
$('#video_button').click(function(){
  window.open("https://www.youtube.com/watch?v=zYUEm2itVng");
});