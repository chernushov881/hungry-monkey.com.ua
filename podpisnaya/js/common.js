// GEOLOCATION
// $('form button').click(function(){
//   var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
//   $("#geoloc").val(loc);
// });


//FORM VALIDATION + AJAX SEND
function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
 
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

var href = document.location.href;
var new_url = href.split('?')[1];
var url = href.split('?')[0];
var utm_catch = '&' + new_url + "&page_url="+url;
var ref = '&ref=' + document.referrer;
var leade_name;
$('#grow_business, #footer_callback_button, #change_situation, #scale_business, #register_now, #top_callback_button').click(function(){
  var id = $(this).attr('id');
  leade_name = "&lead_name="+id;
});
var lead_price = "&lead_price=" + $('#price').html();

var invite_id = "&invite_id="+href.split('invite_id=')[1];
var cook_ga;
var hmid;
var hmid_link = "";
var hmid_f;

		// присваиваем hmid и ложим в базу
$(document).ready(function(){
	var cookie_check = setInterval(function(){
		var ga = get_cookie('_ga');
		if (ga == null) {
			console.log(ga);
		}
		else {
			cook_ga = "&_ga="+get_cookie('_ga');
			hmid =  cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
			hmid_link =  "?&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
         	// var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;

	        var temp_date = new Date();
	        var temp_month = temp_date.getMonth();
	        temp_month++;
	        var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
	        var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
			// var data = hmid_link+utm_catch+invite_id+date_submitted+time_submitted+ref+'&city='+loc;
			var data = hmid_link+utm_catch+invite_id+date_submitted+time_submitted+ref;
			clearInterval(cookie_check);
			$.ajax({
	          type: "GET",
	          url: "id.php",
	          data: data,
	          success: function() {
				console.log('done');
	          }
	        });
		}
	} ,500);
});
$.validate({
  form : 'form',
  addValidClassOnAll : true,
  borderColorOnError: "#eca29b ",
  onSuccess : function($form){
       if ($form.attr('id') == 'main_form' ) {
          var data = $("#main_form").serialize();
         
        var temp_date = new Date();
        var temp_month = temp_date.getMonth();
        temp_month++;
        var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
        var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				// console.log(cook_ga);
				// return false;
				if (cook_ga !== undefined){
					hmid_f =  "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				}
        data += utm_catch;
        data += date_submitted;
        data += time_submitted;
        data += ref;
        data += cook_ga;
        data += hmid_f;

        	$("#main_form").find('.form__button').addClass('disabled').prop('disabled', true);
          $.ajax({
          type: "GET",
          url: "register_mail.php",
          data: data,
          success: function() {
					dataLayer.push({'event': 'FormSumit_main', 'form_type': 'main-form'});
          }
        });
        $.ajax({
        	type: "POST",
        	url: "invite_id.php",
        	data: data
        });
        $.ajax({
          type: "POST",
          url:"amo/amocontactlist.php",
          data: data
        });
     	setTimeout(function() {
            window.location.href = "thanks.html"+hmid_link;
        }, 1500);
          
      return false;
      }
  }

});
//HM ID

//ANIMATION 
$(document).ready(function(){
	function isVisible(e) {
	    var wh = $(window).height() + $(window).scrollTop(); 
	    var el = $(e);
	    var eh = el.offset().top ;
	    if (eh < wh) {
	      return true;
	    }  
	}
	if (!$('body').hasClass('thankyou-page')) {
		if(isVisible('.main-footer')) {
			$('.main-footer').addClass('visible');
		}
		if(isVisible('.header__col.book_col')) {
			$('.header__col.book_col, .header__title').addClass('visible');
			setTimeout(function(){
				$('.header__list li:nth-child(1)').addClass('visible');
			},200);
			setTimeout(function(){
				$('.header__list li:nth-child(2)').addClass('visible');
			},400);
			setTimeout(function(){
				$('.header__list li:nth-child(3)').addClass('visible');
			},600);
			setTimeout(function(){
				$('.header__description').addClass('visible');
			},800);
			setTimeout(function(){
				$('.header__button').addClass('visible');
			},1300);
			setTimeout(function(){
				$('.button__container').addClass('visible');
			},1200);
		}
		if(isVisible('.first-screen__text')) {
			$('.first-screen__text').addClass('visible');
		}
		if(isVisible('.container__row:nth-child(1) .row__col:nth-child(1)')) {
			$('.container__row:nth-child(1) .row__col:nth-child(1)').addClass('visible');
			$('.container__row:nth-child(1) .row__col:nth-child(2)').addClass('visible');
		}
		if(isVisible('.container__row:nth-child(2) .row__col')) {
			$('.container__row:nth-child(2) .row__col').addClass('visible');
		}
		if(isVisible('.second-screen__container')) {
			$('.second-screen__title ').addClass('visible');
		}
		if(isVisible('.second-screen__container .container__item')) {
			setTimeout(function(){
				$('.second-screen__container .container__item:nth-child(1)').addClass('visible');
			},200);
			setTimeout(function(){
				$('.second-screen__container .container__item:nth-child(2)').addClass('visible');
			},400);
			setTimeout(function(){
				$('.second-screen__container .container__item:nth-child(3)').addClass('visible');
			},600);
		}
	}
	
	if (!$('body').hasClass('thankyou-page')) {
		$(window).scroll(function(){
			if(isVisible('.main-footer')) {
				$('.main-footer').addClass('visible');
			}
			if(isVisible('.header__col.book_col')) {
				$('.header__col.book_col, .header__title').addClass('visible');
				setTimeout(function(){
					$('.header__list li:nth-child(1)').addClass('visible');
				},200);
				setTimeout(function(){
					$('.header__list li:nth-child(2)').addClass('visible');
				},400);
				setTimeout(function(){
					$('.header__list li:nth-child(3)').addClass('visible');
				},600);
				setTimeout(function(){
					$('.header__description').addClass('visible');
				},800);
				setTimeout(function(){
					$('.header__button').addClass('visible');
				},1300);
				setTimeout(function(){
					$('.button__container').addClass('visible');
				},1200);
			}
			if(isVisible('.first-screen__text')) {
				$('.first-screen__text').addClass('visible');
			}
			if(isVisible('.container__row:nth-child(1) .row__col:nth-child(1)')) {
				$('.container__row:nth-child(1) .row__col:nth-child(1)').addClass('visible');
				$('.container__row:nth-child(1) .row__col:nth-child(2)').addClass('visible');
			}
			if(isVisible('.container__row:nth-child(2) .row__col')) {
				$('.container__row:nth-child(2) .row__col').addClass('visible');
			}
			if(isVisible('.second-screen__container')) {
				$('.second-screen__title ').addClass('visible');
			}
			if(isVisible('.second-screen__container .container__item')) {
				setTimeout(function(){
					$('.second-screen__container .container__item:nth-child(1)').addClass('visible');
				},200);
				setTimeout(function(){
					$('.second-screen__container .container__item:nth-child(2)').addClass('visible');
				},400);
				setTimeout(function(){
					$('.second-screen__container .container__item:nth-child(3)').addClass('visible');
				},600);
			}
	});
	}
});

//Плавный скролл до блока по клику
$("#header_button").click(function() {
	var offset = $('.main-footer').offset();
	$("html, body").animate({
	  scrollTop:offset.top
	}, 500);
	return false;
});
//MASKED INPUT
jQuery(function($){
$(".user_phone").mask("+38(999) 999-9999");
});
