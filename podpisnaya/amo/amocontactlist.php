<?PHP header("Content-Type: text/html; charset=utf-8");?>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<?php

$mail = trim($_POST["mail"]);
$phone = trim($_POST["phone"]);
$name = trim($_POST["name"]);
$city = trim($_POST["city"]);
$utm_source= trim($_POST["utm_source"]);
$utm_campaign= trim($_POST["utm_campaign"]);
$utm_medium= trim($_POST["utm_medium"]);
$date_submitted= trim($_POST["date_submitted"]);
$time_submitted= trim($_POST["time_submitted"]);
$ip_address= $_SERVER["REMOTE_ADDR"];
$page_variant_name= trim($_POST["page_variant_name"]);
$page_uuid= trim($_POST["page_uuid"]);
$page_name= trim($_POST["page_name"]);
$page_url= trim($_POST["page_url"]);
$ref= trim($_POST["ref"]);
$src= trim($_POST["src"]);
$utm_term= trim($_POST["utm_term"]);
$utm_content= trim($_POST["utm_content"]);
$lead_name= trim($_POST["lead_name"]);
$lead_price= trim($_POST["lead_price"]);
$event_id= trim($_POST["event_id"]);
$landing_version= trim($_POST["landing_version"]);
$event_type= trim($_POST["event_type"]);
$event_subject= trim($_POST["event_subject"]);
$event_source= trim($_POST["event_source"]);
$event_motivation= trim($_POST["event_motivation"]);
$message_template_id= trim($_POST["message_template_id"]);
$product_id= trim($_POST["product_id"]);
$GA_client_id = $_COOKIE['_ga'];
$parts = parse_url($page_url);
parse_str($parts['query'], $query);
$GA_invite_id = $query['invite_id'];
$month = strftime('%B');
$year = date("Y");

require  'amoinit.php';

$link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/contacts/list';
$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
 
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
curl_close($curl);

$Response=json_decode($out, true);
$Response=$Response['response']['contacts'];
$response_arr =$Response; 

$link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/contacts/list?limit_rows=500&limit_offset=500';

$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
 
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
curl_close($curl);

$Response=json_decode($out, true);
$Response=$Response['response']['contacts'];

$final_arr = array_merge($Response, $response_arr );

$account_id = "1";
 
$mail_query=false;
$phone_query=false;
foreach($final_arr as $v) {
  if ($v['custom_fields'][1]['values'][0]['value'] == $mail) {
    $mail_query = true;
    $account_id = $v['id'];
  }
}
foreach($final_arr as $v) {
  if ($v['custom_fields'][0]['values'][0]['value'] == $phone) {
    $phone_query = true;
    $account_id = $v['id'];
  }
}
/*
if ($phone_query) {
  echo "Телефон совпадает";
}
else {
  echo "Телефон не совпадает"; 
}

if ($mail_query) {
  echo "Mail совпадает";
}
else {
  echo "Mail не совпадает"; 
}*/

if(!$phone_query && !$mail_query) {
  // совпадений нет, добавляем новый лид+контакт
  include  'amolead.php'; 
  include  'amoadd.php'; 
}
else {
  // mail или phone совпадаeт

    if ($phone_query && !$mail_query) {
      // Телефон совпадает, Mail не совпадает
      //include  'amolead.php'; 
      //include  'amocontact_mail.php';  
    }
    if ($mail_query && !$phone_query) {
      // Mail совпадает, Телефон не совпадает
      //include  'amolead.php'; 
      //include  'amocontact_phone.php';
    }
    if ($phone_query && $mail_query) {
      // Mail и phone совпадают
      //include  'amolead.php'; 
      //include  'amolead_exist.php'; 
  }
}



?>