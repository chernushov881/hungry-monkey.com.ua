$(function() {

	//MENU
	$('#menuToggle').click(function(){
		$('.mask').fadeToggle(300);
		$('nav ul').toggleClass('active-menu');
	});
	$('.header-call-me').click(function(){
		$('.mask-pop-up').fadeToggle(300);
		$('.pop-up-form').fadeToggle(300);
		return false;
	});
	$('.close-pop-btn').click(function(){
		$('.mask-pop-up').fadeOut(300);
		$('.pop-up-form').fadeOut(300);
		});
	$('.header-btn').on("click", function(){
		target = $("#main_form");
	  $('html, body').animate({scrollTop: $(target).offset().top}, 1500);
	});
	$('.calendar-btn').click(function(){
		$('.mask-pop-up').fadeToggle(300);
		$('.pop-up-res-form').fadeToggle(300);
		var loc_selected = $(this).prevAll('p').html();
		var date_selected = $(this).prevAll('span').html();
		$('h5.loc-data').remove();
		$('<h5 class="loc-data">' + loc_selected + ' <br>' + date_selected + '</h5>').prependTo('form.pop-up-res-form');
		return false;
	});
	$('.close-pop-res-btn').click(function(){
		$('.mask-pop-up').fadeOut(300);
		$('.pop-up-res-form').fadeOut(300);
	});

		//FOOTER FORM
		$('.form__container input').change(function(){
			if( $(this).val() !== '') {
				$(this).closest('.form__container').addClass('has-data');
			};
			if( $(this).val() == '') {
				$(this).closest('.form__container').removeClass('has-data');
			};
	});

		//MASK INPUT
		$(".user_phone").mask("+38(999) 999-9999");

		//Клик вне области - скрытие формы
	$(document).mouseup(function(e) {
			if (!$(e.target).closest(".nav-wrapper").length) {
					$('nav ul').removeClass('active-menu');
					$('#menuToggle input').prop("checked", false);
					$('.mask').fadeOut(300);
			}
			if (!$(e.target).closest(".pop-up-form-wrap, .pop-up-res-form-wrap, .header-call-me").length) {
					$('.pop-up-form').fadeOut(300);
					$('.pop-up-res-form').fadeOut(300);
					$('.mask-pop-up').fadeOut(300);
					return false;
			}
	});

	// OWL CARoUSELLE
	$(document).ready(function(){
	  var owl = $("#owl-carousel");
	  owl.owlCarousel({
	      slideSpeed : 300,
	      singleItem: true,
	      pagination: true,
	  });
		$(".testimonials-prev").click(function(){
			owl.trigger('owl.prev');
		})
		$(".testimonials-next").click(function(){
			owl.trigger('owl.next');
		});

		//VIDEO PLAYER PRYR SETUP
		// plyr.setup({
		// 	// controls: ['play-large'],
		// 	hideControls: true,
		// 	clickToPlay: true,
		// 	showPosterOnEnd: true,
		// });
		
	});



	//MENU ACTIVE CLASS & SCROLL
	$(window).scroll(function(){
		if ($(window).scrollTop() > $('header').outerHeight()) {
			$('.nav-wrapper').addClass('fixed');
		} else {
			$('.nav-wrapper').removeClass('fixed');
		}
	});

	$(document).ready(function(){
	    $('nav ul a[href^="#"]').on('click',function (e) {
	        e.preventDefault();
	        var target = this.hash;
	        var $target = $(target);
	        var scroll;
	        if($(window).scrollTop()==0){
	            scroll =  ($target.offset().top) - 160
	        }else{
	            scroll =  ($target.offset().top) - 60
	        }
	        if($('nav ul').hasClass('active-menu')){
	        	$('.mask').fadeOut(300);
	        	$('nav ul').removeClass('active-menu');
	        	$('#menuToggle input').prop("checked", false);
	        }
	        $('html, body').stop().animate({
	            'scrollTop': scroll
	        }, 900, 'swing', function () {
	            //window.location.hash = target;
	        });
	    });
	});

	$(window).on('scroll', function () {
	   var sections = $('section')
	    , nav = $('nav')
	    , nav_height = nav.outerHeight()
	    , cur_pos = $(this).scrollTop();
	  sections.each(function() {
	    var top = $(this).offset().top - nav_height,
	        bottom = top + $(this).outerHeight();
	 
	    if (cur_pos >= top && cur_pos <= bottom) {
	      nav.find('li a').removeClass('active');
	      sections.removeClass('active');
	 
	      $(this).addClass('active');
	      nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
	    }
	  });
	});
});

	//ANIMATE CSS
	$('.location-your-choose-eu').animated('fadeInLeft');
	$('.location-your-choose-ua').animated('fadeInRight');
	$('.location-cost li, .control-yacht-col, .curs-programm').animated('fadeIn');
	$('.calendar-col-1').animated('slideInLeft');
	$('.calendar-col-2').animated('slideInUp');
	$('.calendar-col-3').animated('slideInRight');
	$('#why .col-md-6').animated('zoomIn');
	$('.about-us-col').animated('fadeInUp');


	// $('form button').click(function(){
	// 	var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
	// 	$("#geoloc").val(loc);
	// });


	//FORM VALIDATION + AJAX SEND
	function get_cookie ( cookie_name ){
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}

	var href = document.location.href;
	var new_url = href.split('?')[1];
	var id = 'practice';
	var url = href.split('?')[0];
	var utm_catch = '&' + new_url + "&page_url="+url;
	var ref = '&ref=' + document.referrer;
	var leade_name = "&lead_name=" + id;
	var lead_price = "&lead_price=" + $('#price').html();
	var invite_id = "&invite_id="+href.split('invite_id=')[1];
	var cook_ga;
	var hmid;
	var hmid_link;

			// присваиваем hmid и ложим в базу
	$(document).ready(function(){

		var cookie_check = setInterval(function(){
			var ga = get_cookie('_ga');
			if (ga === null) {
				console.log('111');
			}
			else {
				cook_ga = "&_ga="+get_cookie('_ga');
				hmid = "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				// var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
				var page_url = "&page_url="+url;
				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				var id = "practice";
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				var data = hmid+utm_catch+invite_id+date_submitted+time_submitted+ref;
				clearInterval(cookie_check);
				$.ajax({
							type: "GET",
							url: "visits.php",
							data: data,
							success: function() {
					console.log('done');
							}
						});
			}
		} ,500);
	});
	$.validate({
		form : 'form',
		addValidClassOnAll : true,
		borderColorOnError: "#eca29b",
		onSuccess : function($form){
					// var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
					// $form.find('#geoloc').val(loc);
					var data = $form.serialize();
					var data_form = $form.attr('data-form');
					var country;
					if(data_form == 'Reservation'){
						country = $form.find('h5').text();
					}
					var temp_date = new Date();
					var temp_month = temp_date.getMonth();
					temp_month++;
					var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
					var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
					data += utm_catch;
					data += date_submitted;
					data += time_submitted;
					data += ref;
					data += cook_ga;
					data += leade_name;
					data += hmid;
					data += '&data_form=' + data_form;
					data += '&country=' + country;

					$.ajax({
						type: "GET",
						url: "register_mail.php",
						data: data,
						beforeSend: function() {
							$form.find('button').prop( "disabled", true ).text('');
							$form.find('.spinner').fadeIn();
						}
					});
					$.ajax({
						type: "POST",
						url:"amo/amocontactlist.php",
						data: data,
						success: function() {
							console.log('ok!');
							$form.find('.spinner').fadeOut();
							setTimeout(function() {
								$form.find('button').text('✔ Отправлено');
							}, 350);
							dataLayer.push({'event': 'FormSumit_practice', 'form_type': data_form});
						}
					});
				// setTimeout(function() {
				// 			window.location.href = "thanks.html";
				// 	}, 1500);

				return false;
		}

	});