<?php
function makeTime(){
   $hour = (Date('H')+3 == 24? 0: Date('H')+3);
  $minute = Date('i');
  $weekday = Date('w');
  if ($weekday == 0){ // если воскресенье
   $increment = 1;
  }
  if ($weekday == 6){ // суббота
   $increment = 2;
  }
  if ( ($weekday <= 0) || ($weekday >= 6) ){ // суббота или воскресенье
  $gotime = mktime(8,30,0, date('m'), date('d')+$increment, date('Y'));
  }else{ // будние
   if ($hour>11 && $hour<19)
  {
   $gotime = time();
  }
  else 
  if (($hour == 19)&&($minute<=30)){
    $gotime = time();
  }
  else{
     if ($hour>19) $inc = 1; else $inc = 0;
   $gotime = mktime(8,30,0, date('m'), date('d')+$inc, date('Y'));
  }
  }
  return $gotime;
 }

$task=array();

$task['element_type'] = 1;
$task['task_type'] = 141324;
$task['text'] = 'Имя клиента: '.$data['name'];
$task['element_id'] = $account_id;
$task['complete_till'] = makeTime();
$task['responsible_user_id'] = $responsibl;

$set['request']['tasks']['add'][]=$task;
#Формируем ссылку для запроса
$subdomain='odlexstatuscomua'; #Наш аккаунт - поддомен
$link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/tasks/set';
$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($set));
curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
print_r($code);
CheckCurlResponse($code);
 
/**
 * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
 * нам придётся перевести ответ в формат, понятный PHP
 */
$Response=json_decode($out,true);
$Response=$Response['response']['tasks']['add'];