<?php

$link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/contacts/list';
$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
 
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
curl_close($curl);

$Response=json_decode($out, true);
$Response=$Response['response']['contacts'];

$GA_client_id = $_COOKIE['_ga'];
$account_id = "1";
$sdelka_id = "2";

foreach($Response as $v) {
  if ($v['custom_fields'][2]['values'][0]['value'] == $GA_client_id) {
    $account_id = $v['id'];
    $sdelka_id = $v['linked_leads_id'][0];
  }
}




?>