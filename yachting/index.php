<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<title>Яхтенная школа Hungry Monkey</title>
	<meta content="Хотите обучиться яхтингу? Приходите в Hungry Monkey – лучшую парусную школу Киева!" name="description" />
	<meta content="https://hungry-monkey.com.ua/img/og_image.jpg" property="og:image" />
	<meta content="Хотите обучиться яхтингу? Приходите в Hungry Monkey – лучшую парусную школу Киева!" property="og:description" />
	<meta content="hungry-monkey.com.ua" property="og:site_name" />
	<meta content="website" property="og:type" />

	<meta content="telephone=no" name="format-detection" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="shortcut icon" href="favicon.png" />

	<style>
		html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font: inherit;
  font-size: 100%;
  vertical-align: baseline;
  box-sizing: border-box; }

html {
  line-height: 1; }

body {
  line-height: 1; }

ol, ul {
  list-style: none; }

table {
  border-collapse: collapse;
  border-spacing: 0; }

caption, th, td {
  text-align: left;
  font-weight: normal;
  vertical-align: middle; }

q, blockquote {
  quotes: none; }
  q:before, q:after, blockquote:before, blockquote:after {
    content: "";
    content: none; }

img {
  max-width: 100%;
  height: auto;
  border: none; }

a {
  text-decoration: none; }

article, aside, details, figcaption, figure, footer, header, main, menu, nav, section, summary {
  display: block;
  box-sizing: border-box; }

:focus {
  outline: none; }

h1, h2, h3, h4, h5, h6 {
  font-weight: normal; }

.cf:before, .cf:after {
  content: '.';
  display: block;
  overflow: hidden;
  visibility: hidden;
  font-size: 0;
  line-height: 0;
  width: 0;
  height: 0; }

.cf:after {
  clear: both; }

span {
  display: inline-block; }

/*@font-face {
	font-family: "bebasneue";
	src: url("../fonts/BebasNeue/Bebas_Neue_Cyrillic.eot");
	src: url("../fonts/BebasNeue/Bebas_Neue_Cyrillic.eot?#iefix")format("embedded-opentype"),
	url("../fonts/BebasNeue/BebasNeueCyrillic.woff") format("woff"),
	url("../fonts/BebasNeue/BebasNeueCyrillic.ttf") format("truetype");
	font-style: normal;
	font-weight: normal;
}

*/
body {
  overflow-x: hidden;
  min-width: 320px; }

.pp {
  width: 100%;
  position: absolute;
  top: -866px;
  opacity: 0.6;
  display: none; }
  .pp img {
    display: block;
    position: absolute;
    left: 50%;
    -webkit-transform: translate(-50%, 0);
        -ms-transform: translate(-50%, 0);
            transform: translate(-50%, 0);
    width: 100%; }

.site-wrapper {
  max-width: 1008px;
  width: 100%;
  margin: 0 auto;
  padding: 0 14px; }

.clear-fix:after {
  content: "";
  display: table;
  clear: both; }

.doyoutry__container .container__item .item__wrapper {
  background-image: url("../img/doyotry_sprite.png");
  background-repeat: no-repeat;
  display: block; }

.doyoutry__container .container__item:nth-child(1) .item__wrapper {
  width: 307px;
  height: 420px;
  background-position: -5px -5px; }

.doyoutry__container .container__item:nth-child(2) .item__wrapper {
  width: 307px;
  height: 420px;
  background-position: -322px -5px; }

.doyoutry__container .container__item:nth-child(3) .item__wrapper {
  width: 307px;
  height: 420px;
  background-position: -639px -5px; }

.icons-sections__container .container__item:nth-child(4) .item__header .header__icon {
  width: 233px;
  height: 182px;
  background-position: -5px -5px;
  top: -10px;
  left: -5px; }

.icons-sections__container .container__item:nth-child(2) .item__header .header__icon {
  width: 233px;
  height: 182px;
  background-position: -5px -197px;
  top: -10px;
  left: -22px; }

.icons-sections__container .container__item:nth-child(1) .item__header .header__icon {
  width: 233px;
  height: 182px;
  background-position: -5px -389px;
  top: -10px;
  left: -21px; }

.icons-sections__container .container__item:nth-child(3) .item__header .header__icon {
  width: 282px;
  height: 256px;
  background-position: -5px -581px;
  top: -60px;
  left: -53px; }

/************************************************************************
// 1. HERO-HEADER
*************************************************************************/
.hero-header {
  position: relative;
  border-bottom: 5px solid #bfd7e3;
  overflow: hidden; }
  .hero-header .site-wrapper {
    position: absolute;
    z-index: 1;
    top: 0;
    left: 50%;
    -webkit-transform: translate(-50%, 0);
        -ms-transform: translate(-50%, 0);
            transform: translate(-50%, 0);
    text-align: right;
    padding-top: 33px; }

.video-wrapper {
  position: relative;
  height: 100vh;
  width: 100%;
  overflow: hidden;
  z-index: 1; }
  .video-wrapper video {
    position: fixed;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
    min-width: 100%;
    min-height: 100%; }

/* .header__overlay {
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	height: 100%;
	width: 100%;
	background: rgba(17, 19, 38, 0.4);
	z-index: 2;
} */
.overlay {
  position: absolute;
  top: 0;
  left: 0;
  background: url("../img/snapshot.jpg") no-repeat 50% 50%;
  background-size: cover;
  height: 100vh;
  width: 100%; }

.text-wrapper {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  z-index: 3;
  color: #fff;
  font-family: "Roboto Slab", serif;
  text-align: center;
  text-shadow: 0 -1px 0 #000; }

.header__title {
  font-weight: 400;
  font-size: 48px; }

.header__subtitle {
  font-size: 72px;
  font-weight: 400;
  margin-bottom: 40px;
  white-space: nowrap; }

.header__description {
  font-size: 36px;
  font-weight: 400;
  margin-bottom: 70px; }

.header__button {
  max-width: 336px;
  width: 100%;
  height: 56px;
  background-color: #fff;
  border: none;
  border-radius: 28px;
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  font-size: 20px;
  text-transform: uppercase;
  cursor: pointer;
  position: relative;
  box-shadow: 0 4px 0 #999999;
  color: #ff5722;
  text-shadow: 0px -1px 0 #c02e00;
  -webkit-transform: translate(0);
      -ms-transform: translate(0);
          transform: translate(0);
  -webkit-transition: box-shadow 0.4s ease, -webkit-transform 0.4s ease;
  transition: box-shadow 0.4s ease, -webkit-transform 0.4s ease;
  transition: transform 0.4s ease, box-shadow 0.4s ease;
  transition: transform 0.4s ease, box-shadow 0.4s ease, -webkit-transform 0.4s ease; }
  .header__button:hover {
    -webkit-transform: translate(0, 2px);
        -ms-transform: translate(0, 2px);
            transform: translate(0, 2px);
    box-shadow: 0 2px 0 #999999;
    -webkit-transition: box-shadow 0.4s ease, -webkit-transform 0.4s ease;
    transition: box-shadow 0.4s ease, -webkit-transform 0.4s ease;
    transition: transform 0.4s ease, box-shadow 0.4s ease;
    transition: transform 0.4s ease, box-shadow 0.4s ease, -webkit-transform 0.4s ease; }
  .header__button:active {
    -webkit-transform: translate(0, 4px);
        -ms-transform: translate(0, 4px);
            transform: translate(0, 4px);
    box-shadow: 0 0px 0 #999999;
    -webkit-transition: box-shadow 0.4s ease, -webkit-transform 0.4s ease;
    transition: box-shadow 0.4s ease, -webkit-transform 0.4s ease;
    transition: transform 0.4s ease, box-shadow 0.4s ease;
    transition: transform 0.4s ease, box-shadow 0.4s ease, -webkit-transform 0.4s ease; }

.buttons__container {
  position: absolute;
  top: 20px;
  right: 20px;
  z-index: 6; }

.menu__buton {
  background-color: transparent;
  border: 2px solid #b3e5fc;
  border-radius: 50%;
  vertical-align: top;
  width: 48px;
  height: 48px;
  position: relative;
  cursor: pointer;
  /* span {
		height: 2px;
		width: 27px;
		background: #b3e5fc;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		&:after, &:before {
			content:"";
			display: block;
			height: 2px;
			width: 27px;
			background: #b3e5fc;
			position: absolute;
			left: 0;
		}
		&:after {
			top: -7px;
		}
		&:before {
			bottom: -7px;
		}
	} */ }
  .menu__buton svg {
    width: 60px;
    height: 60px;
    cursor: pointer;
    position: absolute;
    top: -10px;
    left: -9.5px;
    -webkit-transform: translate3d(0, 0, 0);
    -ms-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0); }
  .menu__buton path {
    fill: none;
    -webkit-transition: stroke-dashoffset 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25), stroke-dasharray 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25);
    transition: stroke-dashoffset 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25), stroke-dasharray 0.5s cubic-bezier(0.25, -0.25, 0.75, 1.25);
    stroke-width: 40px;
    stroke-linecap: round;
    stroke: #b3e5fc;
    stroke-dashoffset: 0px; }
  .menu__buton path#top,
  .menu__buton path#bottom {
    stroke-dasharray: 240px 950px; }
  .menu__buton path#middle {
    stroke-dasharray: 240px 240px; }
  .menu__buton.cross path#top,
  .menu__buton.cross path#bottom {
    stroke-dashoffset: -650px;
    stroke-dashoffset: -650px; }
  .menu__buton.cross path#middle {
    stroke-dashoffset: -115px;
    stroke-dasharray: 1px 220px; }
  .menu__buton:hover {
    background: #fff;
    border: 2px solid #fff;
    -webkit-transition: border 0.4s ease, background 0.4s ease;
    transition: border 0.4s ease, background 0.4s ease; }
    .menu__buton:hover svg path {
      stroke: #00355b; }

.callback__button {
  background-color: transparent;
  border: 2px solid #b3e5fc;
  border-radius: 23px;
  height: 48px;
  text-align: right;
  padding-right: 15px;
  width: 48px;
  font-family: "Roboto", sans-serif;
  font-size: 18px;
  font-weight: 300;
  color: #b3e5fc;
  position: relative;
  cursor: pointer;
  margin-right: 20px;
  -webkit-transition: color 0.4s ease, background 0.4s ease;
  transition: color 0.4s ease, background 0.4s ease;
  z-index: 999;
  overflow: hidden; }
  .callback__button .contacts_container {
    color: #333; }
  .callback__button.opened {
    width: 300px;
    height: 300px; }

.callback__icon {
  display: block;
  width: 27px;
  height: 27px;
  position: absolute;
  top: 11px;
  right: 11px; }
  .callback__icon svg {
    width: 22px;
    height: 22px;
    fill: #b3e5fc;
    -webkit-transition: fill 0.3s ease;
    transition: fill 0.3s ease; }

.callback__button:hover {
  background: #fff;
  border: 2px solid #fff;
  color: #00355b;
  -webkit-transition: color 0.4s ease, background 0.4s ease;
  transition: color 0.4s ease, background 0.4s ease; }
  .callback__button:hover .callback__icon svg {
    fill: #00355b;
    -webkit-transition: fill 0.3s ease;
    transition: fill 0.3s ease; }

.main-menu {
  position: absolute;
  top: 0;
  left: 0;
  z-index: 5;
  width: 100%;
  text-align: center;
  background-color: transparent;
  -webkit-transition: background 0.4s ease-out;
  transition: background 0.4s ease-out; }
  .main-menu.opened {
    height: 100vh;
    background: rgba(0, 0, 0, 0.5);
    -webkit-transition: background 0.4s ease-out;
    transition: background 0.4s ease-out; }
  .main-menu nav {
    position: absolute;
    top: 110px;
    right: -35px;
    z-index: 2;
    max-width: 320px;
    text-align: left;
    -webkit-transform: translate(2000px, 0);
        -ms-transform: translate(2000px, 0);
            transform: translate(2000px, 0); }
    .main-menu nav a {
      display: inline-block;
      color: #fff;
      font-family: "Roboto", sans-serif;
      font-size: 20px;
      font-weight: 300;
      margin: 10px;
      margin-bottom: 25px;
      text-align: right;
      position: relative; }
      .main-menu nav a:after {
        content: "";
        display: block;
        height: 2px;
        width: 0;
        background: #fff;
        position: absolute;
        bottom: -8px;
        left: 0;
        -webkit-transition: width 0.3s ease-out;
        transition: width 0.3s ease-out; }
      .main-menu nav a:nth-child(1) {
        -webkit-transform: translate(0, 2000px);
            -ms-transform: translate(0, 2000px);
                transform: translate(0, 2000px); }
      .main-menu nav a:nth-child(3) {
        -webkit-transform: translate(0, 2500px);
            -ms-transform: translate(0, 2500px);
                transform: translate(0, 2500px); }
      .main-menu nav a:nth-child(4) {
        -webkit-transform: translate(0, 3000px);
            -ms-transform: translate(0, 3000px);
                transform: translate(0, 3000px); }
      .main-menu nav a:nth-child(5) {
        -webkit-transform: translate(0, 3500px);
            -ms-transform: translate(0, 3500px);
                transform: translate(0, 3500px); }
      .main-menu nav a:nth-child(6) {
        -webkit-transform: translate(0, 4000px);
            -ms-transform: translate(0, 4000px);
                transform: translate(0, 4000px); }
      .main-menu nav a:hover:after {
        width: 100%; }
  .main-menu .morph-shape {
    height: 100vh;
    width: 300px;
    position: absolute;
    top: 0;
    right: 0;
    z-index: 1;
    -webkit-transform: translate(320px, 0) rotate(180deg);
        -ms-transform: translate(320px, 0) rotate(180deg);
            transform: translate(320px, 0) rotate(180deg);
    -webkit-transition: -webkit-transform 0.4s ease;
    transition: -webkit-transform 0.4s ease;
    transition: transform 0.4s ease;
    transition: transform 0.4s ease, -webkit-transform 0.4s ease; }
  .main-menu .menu_close {
    position: absolute;
    right: 20px;
    top: 30px;
    width: 20px;
    height: 20px;
    cursor: pointer; }
    .main-menu .menu_close:before, .main-menu .menu_close:after {
      content: "";
      display: block;
      height: 2px;
      position: absolute;
      background: #b3e5fc;
      width: 100%;
      top: 9px; }
    .main-menu .menu_close:before {
      -webkit-transform: rotate(45deg);
          -ms-transform: rotate(45deg);
              transform: rotate(45deg); }
    .main-menu .menu_close:after {
      -webkit-transform: rotate(-45deg);
          -ms-transform: rotate(-45deg);
              transform: rotate(-45deg); }
  .main-menu.opened .morph-shape {
    -webkit-transform: translate(0, 0) rotate(180deg);
        -ms-transform: translate(0, 0) rotate(180deg);
            transform: translate(0, 0) rotate(180deg); }
  .main-menu.opened nav {
    -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
            transform: translate(0, 0); }

nav.opened {
  -webkit-transition: -webkit-transform 0.4s ease;
  transition: -webkit-transform 0.4s ease;
  transition: transform 0.4s ease;
  transition: transform 0.4s ease, -webkit-transform 0.4s ease; }
  nav.opened a:nth-child(1) {
    -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
            transform: translate(0, 0);
    -webkit-transition: -webkit-transform 0.7s ease-out;
    transition: -webkit-transform 0.7s ease-out;
    transition: transform 0.7s ease-out;
    transition: transform 0.7s ease-out, -webkit-transform 0.7s ease-out; }
  nav.opened a:nth-child(3) {
    -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
            transform: translate(0, 0);
    -webkit-transition: -webkit-transform 0.7s ease-out;
    transition: -webkit-transform 0.7s ease-out;
    transition: transform 0.7s ease-out;
    transition: transform 0.7s ease-out, -webkit-transform 0.7s ease-out; }
  nav.opened a:nth-child(4) {
    -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
            transform: translate(0, 0);
    -webkit-transition: -webkit-transform 0.7s ease-out;
    transition: -webkit-transform 0.7s ease-out;
    transition: transform 0.7s ease-out;
    transition: transform 0.7s ease-out, -webkit-transform 0.7s ease-out; }
  nav.opened a:nth-child(5) {
    -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
            transform: translate(0, 0);
    -webkit-transition: -webkit-transform 0.7s ease-out;
    transition: -webkit-transform 0.7s ease-out;
    transition: transform 0.7s ease-out;
    transition: transform 0.7s ease-out, -webkit-transform 0.7s ease-out; }
  nav.opened a:nth-child(6) {
    -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
            transform: translate(0, 0);
    -webkit-transition: -webkit-transform 0.7s ease-out;
    transition: -webkit-transform 0.7s ease-out;
    transition: transform 0.7s ease-out;
    transition: transform 0.7s ease-out, -webkit-transform 0.7s ease-out; }

.main-menu__contacts {
  position: absolute;
  top: 110px;
  right: -45px;
  max-width: 320px;
  width: 100%;
  text-align: left;
  z-index: 2;
  -webkit-transform: translate(0, 3000px);
      -ms-transform: translate(0, 3000px);
          transform: translate(0, 3000px); }
  .main-menu__contacts.opened {
    -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
            transform: translate(0, 0);
    -webkit-transition: -webkit-transform 0.7s ease-out;
    transition: -webkit-transform 0.7s ease-out;
    transition: transform 0.7s ease-out;
    transition: transform 0.7s ease-out, -webkit-transform 0.7s ease-out; }
  .main-menu__contacts .contacts__text {
    font-size: 18px;
    line-height: 28px;
    color: #fff;
    font-family: "Roboto Slab", serif;
    font-weight: 300;
    margin-bottom: 25px; }

.contacts__container {
  padding-top: 10px; }
  .contacts__container .social__item {
    display: inline-block;
    width: 45px;
    height: 45px;
    text-align: center;
    vertical-align: middle;
    border-radius: 50%;
    border: 2px solid #004d85;
    margin-right: 20px;
    -webkit-transition: border-color 0.2s linear;
    transition: border-color 0.2s linear; }
    .contacts__container .social__item svg {
      fill: #004d85;
      width: 25px;
      height: 25px;
      margin-top: 8px;
      -webkit-transition: fill 0.2s linear;
      transition: fill 0.2s linear; }
    .contacts__container .social__item:hover {
      border-color: #fff; }
      .contacts__container .social__item:hover svg {
        fill: #fff; }

@media (max-width: 700px) {
  .header__subtitle {
    font-size: 50px; }
  .header__title {
    font-size: 32px; }
  .header__description {
    line-height: 44px; } }

@media (max-width: 500px) {
  .header__subtitle {
    margin-bottom: 0;
    white-space: normal; }
  .header__title,
  .header__description {
    font-size: 24px;
    line-height: 36px; }
  .header__description {
    margin-bottom: 25px; }
  .main-menu.opened nav {
    width: 100%;
    max-width: none; }
  .main-menu.opened .morph-shape {
    width: 100%; }
  .main-menu nav br {
    display: none; }
  .main-menu nav a {
    display: block;
    text-align: left; }
  .main-menu__contacts {
    right: auto;
    left: 30px; } }

/************************************************************************
// VIDEO POP UP
************************************************************************/
.videopopup {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 53, 91, 0.7);
  z-index: 50;
  overflow: hidden;
  -webkit-transform: scale(0);
      -ms-transform: scale(0);
          transform: scale(0);
  -webkit-transition: -webkit-transform 0.4s ease-out;
  transition: -webkit-transform 0.4s ease-out;
  transition: transform 0.4s ease-out;
  transition: transform 0.4s ease-out, -webkit-transform 0.4s ease-out; }
  .videopopup .videopopup__container {
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
    max-width: 854px;
    width: 100%;
    max-height: 480px;
    height: 100%; }
    .videopopup .videopopup__container iframe {
      width: 100%;
      height: 100%; }
  .videopopup.visible {
    -webkit-transform: scale(1);
        -ms-transform: scale(1);
            transform: scale(1);
    -webkit-transition: -webkit-transform 0.4s ease-out;
    transition: -webkit-transform 0.4s ease-out;
    transition: transform 0.4s ease-out;
    transition: transform 0.4s ease-out, -webkit-transform 0.4s ease-out; }
	</style>
	<script>
	 dataLayer = [];
	</script>
	
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W7SLS8V');</script>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P8DCV5Q');</script>
<!-- End Google Tag Manager -->

	</head>

	<body>

	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W7SLS8V"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P8DCV5Q"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<svg style="position: absolute; width: 0; height: 0;" width="0" height="0" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
<defs>
<symbol id="icon-form_icon1" viewBox="0 0 32 32">
<title>form_icon1</title>
<path class="path1" d="M25.998 22.002h-1.998c-2.209 0-4.003-1.794-4.003-4.003v-1.286c0.884-1.049 1.517-2.295 1.913-3.608 0.040-0.224 0.257-0.33 0.396-0.481 0.765-0.765 0.917-2.058 0.343-2.974-0.079-0.139-0.218-0.264-0.211-0.435 0-1.174 0.007-2.348 0-3.522-0.033-1.418-0.435-2.889-1.431-3.937-0.798-0.844-1.899-1.352-3.034-1.57-1.431-0.27-2.935-0.257-4.353 0.099-1.233 0.31-2.387 1.029-3.1 2.097-0.633 0.93-0.91 2.058-0.956 3.166-0.020 1.194-0.007 2.387-0.007 3.588 0.026 0.237-0.178 0.402-0.27 0.6-0.541 0.983-0.303 2.322 0.567 3.034 0.218 0.152 0.264 0.429 0.343 0.666 0.376 1.181 1.009 2.275 1.8 3.225v1.326c0 2.209-1.794 4.003-4.003 4.003h-1.992c0 0-3.627 1.002-6.002 6.002v1.998c0 1.108 0.897 1.998 1.998 1.998h28.003c1.108 0 1.998-0.897 1.998-1.998v-1.998c-2.374-4.993-6.002-5.988-6.002-5.988z"></path>
</symbol>
<symbol id="icon-form_icon2" viewBox="0 0 32 32">
<title>form_icon2</title>
<path class="path1" d="M27.573 22.272c-1.931-1.653-3.893-2.656-5.803-1.003l-1.141 0.997c-0.832 0.725-2.384 4.107-8.384-2.789-5.995-6.891-2.427-7.963-1.589-8.683l1.147-0.997c1.899-1.653 1.184-3.739-0.187-5.883l-0.827-1.301c-1.376-2.139-2.875-3.547-4.779-1.893l-1.035 0.901c-0.843 0.613-3.195 2.608-3.765 6.395-0.683 4.544 1.483 9.744 6.448 15.456 4.96 5.712 9.813 8.581 14.411 8.528 3.819-0.043 6.128-2.091 6.853-2.837l1.035-0.901c1.899-1.653 0.72-3.333-1.216-4.987l-1.168-1.003z"></path>
</symbol>
<symbol id="icon-form_icon3" viewBox="0 0 43 32">
<title>form_icon3</title>
<path class="path1" d="M42.667 26.662c0 0.932-0.264 1.803-0.686 2.568l-13.472-15.072 13.331-11.66c0.519 0.827 0.827 1.785 0.827 2.832v21.333zM21.333 16.893l18.546-16.224c-0.765-0.413-1.618-0.668-2.541-0.668h-32c-0.923 0-1.785 0.255-2.541 0.668l18.537 16.224zM26.504 15.908l-4.291 3.755c-0.255 0.22-0.563 0.325-0.879 0.325s-0.624-0.106-0.879-0.325l-4.291-3.755-13.639 15.266c0.818 0.51 1.776 0.818 2.814 0.818h32c1.038 0 1.996-0.308 2.814-0.818l-13.648-15.266zM0.827 2.497c-0.51 0.818-0.827 1.785-0.827 2.832v21.333c0 0.932 0.264 1.803 0.686 2.568l13.472-15.081-13.331-11.652z"></path>
</symbol>
<symbol id="icon-obtain1" viewBox="0 0 32 32">
<title>obtain1</title>
<path class="path1" d="M29.606 4.346h-3.136v-1.741c0-0.736-0.621-1.357-1.357-1.357h-1.933c-0.736 0-1.357 0.621-1.357 1.357v1.741h-11.494v-1.741c0-0.736-0.621-1.357-1.357-1.357h-1.933c-0.736 0-1.357 0.621-1.357 1.357v1.741h-3.283c-1.312 0-2.4 1.082-2.4 2.4v21.626c0 1.312 1.082 2.4 2.4 2.4h27.2c1.312 0 2.4-1.082 2.4-2.4v-21.626c0-1.318-1.082-2.4-2.394-2.4zM23.258 2.72h1.779v4.256h-1.779v-4.256zM7.123 2.72h1.779v4.256h-1.779v-4.256zM2.438 5.779h3.29v1.274c0 0.736 0.621 1.357 1.357 1.357h1.933c0.736 0 1.357-0.621 1.357-1.357v-1.274h11.456v1.274c0 0.736 0.621 1.357 1.357 1.357h1.933c0.736 0 1.357-0.621 1.357-1.357v-1.274h3.174c0.544 0 0.966 0.429 0.966 0.966v3.872h-29.146v-3.872c0-0.544 0.429-0.966 0.966-0.966zM29.606 29.344h-27.168c-0.544 0-0.966-0.429-0.966-0.966v-16.333h29.101v16.326c0 0.544-0.429 0.973-0.966 0.973zM7.782 15.955c0 0.384-0.307 0.698-0.698 0.698h-2.317c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.378-0.038 0.691 0.307 0.691 0.698zM14.477 15.955c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.346-0.038 0.698 0.307 0.698 0.698zM21.133 15.955c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.384-0.038 0.698 0.307 0.698 0.698zM27.821 15.955c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.39-0.038 0.698 0.307 0.698 0.698zM7.782 20.71c0 0.384-0.307 0.698-0.698 0.698h-2.317c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.378 0.006 0.691 0.314 0.691 0.698zM14.477 20.71c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.346 0.006 0.698 0.314 0.698 0.698zM21.133 20.71c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.384 0.006 0.698 0.314 0.698 0.698zM27.821 20.71c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.39 0.006 0.698 0.314 0.698 0.698zM7.782 25.51c0 0.384-0.307 0.698-0.698 0.698h-2.317c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.378-0.038 0.691 0.275 0.691 0.698zM14.477 25.51c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.346-0.038 0.698 0.275 0.698 0.698zM21.133 25.51c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.384-0.038 0.698 0.275 0.698 0.698zM27.821 25.51c0 0.384-0.307 0.698-0.698 0.698h-2.323c-0.384 0-0.698-0.307-0.698-0.698 0-0.384 0.307-0.698 0.698-0.698h2.323c0.39-0.038 0.698 0.275 0.698 0.698z"></path>
</symbol>
<symbol id="icon-obtain2" viewBox="0 0 32 32">
<title>obtain2</title>
<path class="path1" d="M13.037 2.054l-2.522 0.755 2.522 0.749z"></path>
<path class="path2" d="M13.037 2.829v-0.774l-2.522 0.755 1.299 0.384z"></path>
<path class="path3" d="M31.962 26.253c-0.083-0.192-0.269-0.32-0.48-0.32h-8.339c-0.013-0.006-0.019-0.019-0.032-0.026l-1.786-1.466c-0.096-0.077-0.211-0.122-0.333-0.122h-6.912v-0.717h12.742c0.192 0 0.365-0.102 0.461-0.269 0.090-0.166 0.083-0.371-0.019-0.531-0.051-0.083-5.498-8.486-13.184-17.766v-0.736c0-0.013 0-0.026 0-0.038v-2.906c0-0.166-0.077-0.32-0.211-0.416-0.058-0.045-0.128-0.070-0.198-0.090-0.006 0-0.013 0-0.019-0.006-0.013 0-0.019-0.006-0.032-0.006-0.006 0-0.019 0-0.026-0.006-0.006 0-0.019 0-0.026 0s-0.013 0-0.019 0c-0.006 0-0.013 0-0.019 0s-0.013 0-0.019 0c-0.019 0-0.032 0-0.051 0.006 0 0-0.006 0-0.006 0-0.026 0.006-0.051 0.013-0.077 0.019l-4.851 1.446c-0.224 0.064-0.371 0.269-0.371 0.499s0.154 0.435 0.371 0.499l4.493 1.344v1.376l-8.838 16.813c-0.083 0.16-0.077 0.358 0.013 0.512 0.096 0.154 0.262 0.25 0.448 0.25h8.397v0.717h-6.995c-0.147 0-0.288 0.064-0.39 0.173l-1.082 1.21c-0.058 0.070-0.102 0.147-0.122 0.23h-3.93c-0.186 0-0.365 0.102-0.454 0.269s-0.090 0.365 0.006 0.525l2.4 3.955c0.096 0.154 0.262 0.25 0.448 0.25h24.621c0.141 0 0.269-0.058 0.371-0.154l3.936-3.955c0.147-0.147 0.192-0.371 0.115-0.563zM14.79 22.56c0.371-0.614 0.666-1.306 0.89-2.074 0.019 0 0.038 0 0.051 0h8.717c0.602 0.877 1.082 1.581 1.408 2.074h-11.066zM23.725 19.443h-7.795c0.39-2.003 0.365-4.448-0.070-7.309-0.256-1.683-0.621-3.226-0.947-4.429 3.712 4.595 6.784 8.832 8.813 11.738zM14.080 8.653c0.909 3.59 1.894 9.427 0 13.037v-13.037zM30.221 26.976l-2.893 2.906h-24.115l-1.766-2.912zM13.037 8.262v14.298h-7.533l7.533-14.298zM20.806 25.357l0.698 0.57h-15.738l0.512-0.57h14.528z"></path>
</symbol>
<symbol id="icon-obtain3" viewBox="0 0 28 32">
<title>obtain3</title>
<path class="path1" d="M25.389 3.667c0.026 0.090 0.051 0.179 0.070 0.275-0.019-0.096-0.045-0.186-0.070-0.275z"></path>
<path class="path2" d="M22.022 11.174v0l0.090-0.134h-0.006z"></path>
<path class="path3" d="M25.005 2.829c0.019 0.032 0.038 0.064 0.058 0.090-0.019-0.026-0.038-0.058-0.058-0.090z"></path>
<path class="path4" d="M24.806 2.547c0.013 0.019 0.032 0.038 0.045 0.058-0.013-0.019-0.032-0.038-0.045-0.058z"></path>
<path class="path5" d="M24.896 2.675c0.019 0.032 0.045 0.058 0.064 0.090-0.019-0.032-0.038-0.064-0.064-0.090z"></path>
<path class="path6" d="M24.256 1.971c-0.032-0.026-0.070-0.058-0.102-0.083 0.032 0.026 0.058 0.045 0.090 0.070 0 0.006 0.006 0.006 0.013 0.013z"></path>
<path class="path7" d="M24.077 1.83c-0.026-0.019-0.058-0.045-0.083-0.064 0.013 0.006 0.026 0.019 0.038 0.032s0.026 0.019 0.045 0.032z"></path>
<path class="path8" d="M23.162 1.344c-0.026-0.006-0.045-0.019-0.070-0.026 0.019 0.006 0.038 0.013 0.058 0.019 0.006 0 0.006 0.006 0.013 0.006z"></path>
<path class="path9" d="M24.941 6.746v-0c0.403-0.602 0.614-1.293 0.614-2.010 0-0.032 0-0.064 0-0.096 0 0.032 0 0.064 0 0.096-0.006 0.717-0.218 1.414-0.614 2.010z"></path>
<path class="path10" d="M24.307 2.016c0.051 0.045 0.096 0.083 0.141 0.128 0 0 0 0 0 0s0 0 0 0c-0.051-0.051-0.102-0.096-0.154-0.141 0 0 0 0 0 0 0.006 0.006 0.013 0.013 0.013 0.013z"></path>
<path class="path11" d="M24.691 2.406c0.019 0.019 0.032 0.038 0.045 0.058-0.013-0.019-0.026-0.038-0.045-0.058z"></path>
<path class="path12" d="M21.946 1.133v0c0 0 0 0 0 0s0 0 0 0z"></path>
<path class="path13" d="M23.469 1.472c0.019 0.013 0.045 0.019 0.064 0.032-0.019-0.013-0.038-0.026-0.064-0.032z"></path>
<path class="path14" d="M23.29 1.389c0.019 0.006 0.038 0.019 0.058 0.026-0.019-0.006-0.038-0.013-0.058-0.026z"></path>
<path class="path15" d="M23.648 1.562c0.026 0.013 0.045 0.026 0.070 0.038-0.026-0.013-0.045-0.026-0.070-0.038z"></path>
<path class="path16" d="M24.576 2.272c0.013 0.019 0.032 0.032 0.045 0.051-0.019-0.019-0.032-0.032-0.045-0.051z"></path>
<path class="path17" d="M23.821 1.658c0.026 0.013 0.051 0.032 0.077 0.045-0.026-0.013-0.051-0.026-0.077-0.045z"></path>
<path class="path18" d="M25.101 3.002c0.013 0.026 0.032 0.058 0.045 0.083-0.013-0.026-0.026-0.058-0.045-0.083z"></path>
<path class="path19" d="M21.946 6.534c-0.902 0-1.638-0.736-1.638-1.638s0.736-1.638 1.638-1.638v0c-0.902 0-1.638 0.736-1.638 1.638 0 0.909 0.736 1.638 1.638 1.638 0.058 0 0.115 0 0.166-0.006-0.058 0.006-0.109 0.006-0.166 0.006z"></path>
<path class="path20" d="M21.946 5.408c0.282 0 0.506-0.23 0.506-0.506 0-0.282-0.23-0.506-0.506-0.506-0.282 0-0.506 0.23-0.506 0.506s0.224 0.506 0.506 0.506z"></path>
<path class="path21" d="M21.568 12.506c0.109 0.154 0.282 0.243 0.461 0.243 0 0 0.006 0 0.006 0 0.006 0 0.013 0 0.026 0-0.006 0-0.013 0-0.026 0 0 0-0.006 0-0.006 0-0.179 0-0.352-0.096-0.461-0.243v0z"></path>
<path class="path22" d="M22.931 1.267c-0.019-0.006-0.032-0.006-0.051-0.013 0 0 0 0 0 0 0.019 0.006 0.038 0.013 0.051 0.013z"></path>
<path class="path23" d="M27.168 6.797v-0.026l-0.48 0.282z"></path>
<path class="path24" d="M22.106 11.040h0.006l1.562-2.374z"></path>
<path class="path25" d="M23.987 1.766c0 0 0 0 0 0 0.013 0.006 0.026 0.019 0.038 0.032-0.013-0.013-0.026-0.019-0.038-0.032z"></path>
<path class="path26" d="M22.547 1.184c-0.026-0.006-0.058-0.013-0.083-0.013-0.019 0-0.038-0.006-0.064-0.006 0 0-0.006 0-0.006 0 0.013 0 0.019 0 0.032 0.006 0.045 0 0.083 0.006 0.122 0.013z"></path>
<path class="path27" d="M22.342 1.152c-0.019 0-0.032-0.006-0.051-0.006-0.032 0-0.058-0.006-0.090-0.006-0.006 0-0.006 0-0.013 0 0.006 0 0.013 0 0.026 0 0.045 0.006 0.090 0.006 0.128 0.013z"></path>
<path class="path28" d="M22.125 1.133c-0.058 0-0.115-0.006-0.173-0.006 0 0 0 0 0 0 0.070 0 0.134 0 0.198 0.006-0.013 0-0.019 0-0.026 0z"></path>
<path class="path29" d="M22.97 1.28c-0.006 0-0.019-0.006-0.026-0.006s-0.006 0-0.013 0c-0.019-0.006-0.032-0.006-0.051-0.013 0.032 0 0.064 0.013 0.090 0.019z"></path>
<path class="path30" d="M22.778 1.229c0.006 0 0.006 0 0.013 0.006 0 0 0 0 0 0-0.006 0-0.019-0.006-0.026-0.006-0.045-0.013-0.090-0.019-0.134-0.032-0.013 0-0.026-0.006-0.038-0.006 0 0-0.006 0-0.006 0 0.019 0 0.032 0.006 0.051 0.006 0.045 0.006 0.090 0.019 0.141 0.032z"></path>
<path class="path31" d="M20.307 4.896c0 0.902 0.736 1.638 1.638 1.638 0.058 0 0.115 0 0.166-0.006 0.826-0.083 1.472-0.781 1.472-1.632 0-0.902-0.736-1.638-1.638-1.638v0c-0.902 0-1.638 0.736-1.638 1.638zM21.946 4.39c0.282 0 0.506 0.23 0.506 0.506 0 0.282-0.23 0.506-0.506 0.506-0.282 0-0.506-0.23-0.506-0.506s0.224-0.506 0.506-0.506z"></path>
<path class="path32" d="M28.013 5.299c-0.173-0.102-0.39-0.102-0.57 0l-0.89 0.525c0.083-0.352 0.128-0.717 0.128-1.088 0-2.611-2.125-4.736-4.736-4.736s-4.736 2.125-4.736 4.736c0 0.934 0.275 1.843 0.794 2.624 0 0 0.006 0.006 0.006 0.006l1.709 2.458-1.037 0.608-8.768-5.133c0 0 0 0 0 0s0 0 0 0c-0.013-0.006-0.019-0.013-0.032-0.013-0.013-0.006-0.026-0.013-0.032-0.019-0.013-0.006-0.026-0.006-0.032-0.013-0.013-0.006-0.026-0.006-0.038-0.013-0.013 0-0.026-0.006-0.038-0.006s-0.026-0.006-0.038-0.006c-0.013 0-0.026 0-0.038 0s-0.026 0-0.038 0c-0.013 0-0.026 0-0.038 0s-0.026 0-0.032 0c-0.013 0-0.026 0.006-0.038 0.006s-0.026 0.006-0.032 0.006c-0.013 0.006-0.026 0.006-0.038 0.013s-0.019 0.006-0.032 0.013c-0.013 0.006-0.026 0.013-0.038 0.019s-0.019 0.006-0.032 0.013c0 0 0 0 0 0s0 0 0 0l-9.062 5.299c-0.173 0.102-0.282 0.288-0.282 0.486v20.346c0 0.205 0.109 0.39 0.282 0.493 0.090 0.051 0.186 0.077 0.282 0.077s0.198-0.026 0.288-0.077l8.768-5.133 8.768 5.133c0 0 0 0 0 0s0 0 0 0c0.013 0.006 0.026 0.013 0.038 0.019s0.019 0.013 0.032 0.013c0.013 0.006 0.032 0.013 0.045 0.013 0.006 0 0.019 0.006 0.026 0.006 0.019 0.006 0.032 0.006 0.051 0.013 0.006 0 0.013 0.006 0.026 0.006 0.026 0 0.051 0.006 0.070 0.006 0.026 0 0.051 0 0.070-0.006 0.006 0 0.013 0 0.026-0.006 0.019 0 0.032-0.006 0.051-0.013 0.006 0 0.019-0.006 0.026-0.006 0.013-0.006 0.032-0.006 0.045-0.013s0.019-0.006 0.032-0.013c0.013-0.006 0.026-0.013 0.038-0.019 0 0 0 0 0 0s0 0 0 0l9.056-5.299c0.173-0.102 0.282-0.288 0.282-0.486v-20.352c0-0.198-0.109-0.39-0.288-0.486zM9.056 18.669v7.142l-7.923 4.64v-8.365l7.923-4.64v1.222zM9.056 8.083v8.051l-7.923 4.64v-9.363l7.923-4.64v1.312zM18.112 30.451l-7.923-4.64v-8.365l7.923 4.64v8.365zM18.112 12.41v8.365l-7.923-4.64v-9.363l7.923 4.64v0.998zM19.635 7.731l-0.698-1.005c-0.39-0.589-0.602-1.28-0.602-1.99 0-1.99 1.619-3.61 3.603-3.61v0c0 0 0 0 0 0 0.058 0 0.115 0 0.173 0.006 0.006 0 0.019 0 0.026 0 0.013 0 0.032 0 0.045 0.006 0.006 0 0.006 0 0.013 0 0.032 0 0.058 0.006 0.090 0.006 0.019 0 0.032 0.006 0.051 0.006s0.032 0.006 0.051 0.006c0 0 0.006 0 0.006 0 0.019 0 0.038 0.006 0.064 0.006 0.026 0.006 0.058 0.006 0.083 0.013 0.013 0 0.026 0.006 0.032 0.006 0 0 0.006 0 0.006 0 0.013 0 0.026 0.006 0.038 0.006 0.045 0.006 0.090 0.019 0.134 0.032 0.006 0 0.019 0.006 0.026 0.006 0 0 0 0 0 0 0.032 0.006 0.064 0.013 0.090 0.026 0.019 0.006 0.032 0.006 0.051 0.013 0.006 0 0.006 0 0.013 0s0.019 0.006 0.026 0.006c0.038 0.013 0.083 0.026 0.122 0.038 0.026 0.006 0.045 0.013 0.070 0.026 0.045 0.013 0.083 0.032 0.128 0.051 0.019 0.006 0.038 0.019 0.058 0.026 0.038 0.019 0.083 0.038 0.122 0.051 0.019 0.013 0.045 0.019 0.064 0.032 0.038 0.019 0.077 0.038 0.115 0.058 0.026 0.013 0.045 0.026 0.070 0.038 0.032 0.019 0.070 0.038 0.102 0.058 0.026 0.013 0.051 0.032 0.077 0.045 0.032 0.019 0.064 0.038 0.090 0.058 0 0 0 0 0 0 0.032 0.019 0.058 0.038 0.083 0.064 0.026 0.019 0.051 0.038 0.077 0.058 0.032 0.026 0.070 0.058 0.102 0.083 0.013 0.013 0.032 0.026 0.045 0.038 0.051 0.045 0.102 0.090 0.154 0.141 0.045 0.038 0.083 0.083 0.122 0.122 0.013 0.013 0.032 0.032 0.045 0.051 0.026 0.026 0.051 0.058 0.077 0.083 0.013 0.019 0.032 0.038 0.045 0.058 0.026 0.026 0.045 0.058 0.064 0.083 0.013 0.019 0.032 0.038 0.045 0.058 0.006 0.006 0.006 0.013 0.013 0.019 0.013 0.019 0.026 0.032 0.032 0.051 0.019 0.032 0.045 0.058 0.064 0.090 0.013 0.019 0.026 0.045 0.045 0.064 0.019 0.032 0.038 0.064 0.058 0.090 0.013 0.026 0.032 0.051 0.045 0.077s0.032 0.058 0.045 0.083c0.026 0.058 0.058 0.109 0.083 0.166 0 0 0 0.006 0 0.006 0.019 0.045 0.038 0.090 0.058 0.128 0 0.006 0 0.006 0.006 0.013 0.032 0.090 0.064 0.173 0.096 0.262 0.026 0.090 0.051 0.179 0.070 0.275 0 0.013 0.006 0.026 0.006 0.038 0.006 0.038 0.013 0.070 0.019 0.109 0.006 0.019 0.006 0.038 0.013 0.058 0.006 0.032 0.013 0.064 0.013 0.102 0 0.019 0.006 0.045 0.006 0.064 0.006 0.032 0.006 0.064 0.013 0.096 0 0.026 0.006 0.045 0.006 0.070 0 0.032 0.006 0.064 0.006 0.096 0 0.026 0 0.045 0 0.070 0 0.032 0 0.064 0 0.096 0 0.717-0.211 1.414-0.614 2.010l-0.019 0.032-2.899 4.39-2.362-3.418zM27.168 18.682v7.13l-7.923 4.64v-8.365l7.923-4.64v1.235zM27.168 6.797v9.338l-7.923 4.64v-9.363l1.114-0.653 0.646 0.928 0.563 0.813c0.109 0.154 0.282 0.243 0.467 0.243 0 0 0.006 0 0.006 0 0.006 0 0.013 0 0.026 0 0.179-0.013 0.346-0.102 0.442-0.25l3.206-4.864 1.453-0.851v0.019z"></path>
</symbol>
<symbol id="icon-obtain4" viewBox="0 0 30 32">
<title>obtain4</title>
<path class="path1" d="M26.464 23.059l-5.056-1.261c-0.019-0.026-0.038-0.058-0.064-0.083l-1.504-1.504v-1.44c0.128-0.128 0.25-0.262 0.365-0.397 1.408-1.626 2.253-3.808 2.381-6.157 0.006-0.096 0.013-0.186 0.013-0.282 0.013-0.314 0.032-0.602 0.058-0.902l0.499-4.986c0.154-1.542-0.352-3.085-1.395-4.23-1.037-1.158-2.522-1.818-4.070-1.818h-5.587c-1.549 0-3.034 0.659-4.077 1.805s-1.549 2.694-1.395 4.237l0.499 4.986c0.032 0.294 0.045 0.595 0.058 0.909 0.006 0.096 0.006 0.186 0.013 0.282 0.128 2.349 0.973 4.531 2.381 6.157 0.122 0.141 0.243 0.269 0.365 0.397v1.44l-1.504 1.504c-0.026 0.026-0.045 0.051-0.064 0.077l-5.056 1.267c-2.144 0.538-3.322 1.85-3.322 3.706v3.616c0 0.896 0.73 1.626 1.626 1.626h26.541c0.896 0 1.626-0.73 1.626-1.626v-3.616c-0.006-1.856-1.184-3.168-3.328-3.706v0zM14.342 28.685h1.107c0.038 0 0.077 0 0.115-0.006l0.39 2.285h-2.112l0.39-2.285c0.032 0 0.070 0.006 0.109 0.006v0zM16.032 27.059c0 0.326-0.262 0.589-0.589 0.589h-1.107c-0.326 0-0.589-0.262-0.589-0.589v-0.826l1.146-0.762 1.139 0.762v0.826zM10.989 19.654c1.171 0.838 2.515 1.286 3.904 1.286s2.733-0.448 3.904-1.286v0.557l-3.904 3.904-3.904-3.904v-0.557zM16.845 25.523c-0.006 0-0.006 0 0 0l-1.139-0.755 3.61-3.61 1.018 1.018c-0.346 0.621-1.146 2.067-2.285 4.115 0 0.006-0.006 0.013-0.019 0.013-0.013 0.006-0.026-0.006-0.026-0.006l-1.158-0.774zM8.794 2.502c0.845-0.934 2.048-1.466 3.302-1.466h5.587c1.254 0 2.464 0.538 3.302 1.466 0.845 0.934 1.254 2.182 1.133 3.437l-0.314 3.155c-0.698-1.67-1.472-2.739-1.517-2.803-0.166-0.224-0.48-0.282-0.71-0.122-1.363 0.928-7.43 1.050-9.651 1.018-0.147 0-0.294 0.058-0.397 0.173-0.634 0.71-1.133 1.504-1.504 2.208l-0.365-3.629c-0.122-1.254 0.288-2.502 1.133-3.437v0zM8.237 12.154c-0.006-0.090-0.013-0.186-0.013-0.275 0-0.064-0.006-0.128-0.013-0.192 0.109-0.32 0.762-2.067 1.933-3.462 1.434 0.013 7.29 0 9.549-0.954 0.442 0.704 1.382 2.342 1.875 4.403-0.006 0.070-0.006 0.134-0.013 0.198-0.006 0.090-0.006 0.186-0.013 0.275-0.237 4.352-3.155 7.757-6.65 7.757s-6.413-3.405-6.656-7.75zM10.47 21.158l3.61 3.61-1.133 0.755c0 0 0 0 0 0l-1.158 0.774c-0.006 0-0.013 0.006-0.026 0.006s-0.019-0.013-0.019-0.013c-1.139-2.042-1.939-3.494-2.285-4.115l1.011-1.018zM1.037 30.374v-3.616c0-1.37 0.851-2.278 2.541-2.701l5.043-1.261c0.301 0.538 0.922 1.658 2.214 3.987 0.147 0.262 0.39 0.448 0.678 0.518 0.083 0.019 0.166 0.032 0.25 0.032 0.211 0 0.416-0.064 0.595-0.179l0.352-0.237v0.134c0 0.474 0.205 0.902 0.531 1.197l-0.461 2.707h-6.771v-2.797c0-0.288-0.23-0.518-0.518-0.518s-0.518 0.23-0.518 0.518v2.797h-3.347c-0.326 0.006-0.589-0.256-0.589-0.582zM28.749 30.374c0 0.326-0.262 0.589-0.589 0.589h-3.347v-2.797c0-0.288-0.23-0.518-0.518-0.518s-0.518 0.23-0.518 0.518v2.797h-6.778l-0.461-2.707c0.326-0.294 0.531-0.723 0.531-1.197v-0.134l0.352 0.237c0.179 0.122 0.384 0.179 0.595 0.179 0.083 0 0.166-0.013 0.25-0.032 0.288-0.070 0.538-0.256 0.678-0.518 1.293-2.33 1.914-3.443 2.214-3.981l5.043 1.261c1.683 0.422 2.541 1.331 2.541 2.701v3.603h0.006z"></path>
</symbol>
<symbol id="icon-obtain5" viewBox="0 0 32 32">
<title>obtain5</title>
<path class="path1" d="M31.696 17.76l-4.104-12.64c-0.24-0.736-0.672-1.336-1.312-1.792s-1.344-0.688-2.104-0.688h-16.32c-0.768 0-1.464 0.232-2.104 0.688s-1.080 1.056-1.312 1.792l-4.112 12.64c-0.224 0.696-0.336 1.216-0.336 1.568v6.672c0 0.92 0.328 1.704 0.984 2.36s1.44 0.984 2.36 0.984h25.36c0.92 0 1.704-0.328 2.36-0.984s0.976-1.44 0.976-2.36v-6.672c0-0.352-0.112-0.872-0.336-1.568zM6.12 4.976c0.064-0.2 0.184-0.36 0.368-0.488s0.384-0.192 0.592-0.192h17.864c0.216 0 0.408 0.064 0.592 0.192s0.304 0.296 0.368 0.488l3.984 12.448h-27.592l3.824-12.448zM30.632 26.952c0 0.2-0.072 0.368-0.216 0.512s-0.312 0.216-0.512 0.216h-27.776c-0.2 0-0.368-0.072-0.512-0.216s-0.216-0.312-0.216-0.512v-7.312c0-0.2 0.072-0.368 0.216-0.512s0.312-0.216 0.512-0.216h27.776c0.2 0 0.368 0.072 0.512 0.216s0.216 0.312 0.216 0.512v7.312z"></path>
<path class="path2" d="M20.904 21.784c-0.416 0-0.768 0.144-1.064 0.44s-0.448 0.648-0.448 1.064c0 0.416 0.144 0.768 0.448 1.064 0.296 0.296 0.648 0.44 1.064 0.44s0.768-0.144 1.064-0.44c0.296-0.296 0.448-0.648 0.448-1.064s-0.152-0.768-0.448-1.064c-0.296-0.288-0.648-0.44-1.064-0.44z"></path>
<path class="path3" d="M25.744 21.784c-0.416 0-0.768 0.152-1.064 0.44-0.296 0.296-0.44 0.648-0.44 1.064s0.144 0.768 0.44 1.064c0.296 0.296 0.648 0.44 1.064 0.44s0.768-0.144 1.064-0.44c0.296-0.296 0.448-0.648 0.448-1.064s-0.152-0.768-0.448-1.064c-0.296-0.288-0.656-0.44-1.064-0.44z"></path>
</symbol>
<symbol id="icon-wheel" viewBox="0 0 32 32">
<title>wheel</title>
<path class="path1" d="M31.155 15.155c-0.269 0-0.506 0.128-0.659 0.32h-3.322c-0.122-2.694-1.203-5.139-2.906-7.008l2.355-2.355c0.25 0.032 0.506-0.051 0.698-0.243 0.326-0.326 0.326-0.864 0-1.19s-0.864-0.326-1.19 0c-0.192 0.192-0.269 0.448-0.243 0.698l-2.355 2.355c-1.869-1.702-4.314-2.784-7.008-2.906v-3.322c0.198-0.154 0.32-0.39 0.32-0.659 0-0.467-0.378-0.845-0.845-0.845s-0.845 0.378-0.845 0.845c0 0.269 0.128 0.506 0.32 0.659v3.322c-2.694 0.122-5.139 1.203-7.008 2.906l-2.349-2.349c0.032-0.25-0.051-0.506-0.243-0.698-0.326-0.326-0.864-0.326-1.19 0s-0.326 0.864 0 1.19c0.192 0.192 0.448 0.269 0.698 0.243l2.355 2.355c-1.702 1.869-2.784 4.314-2.906 7.008h-3.328c-0.154-0.198-0.39-0.32-0.659-0.32-0.467-0.006-0.845 0.371-0.845 0.838s0.378 0.845 0.845 0.845c0.269 0 0.506-0.128 0.659-0.32h3.322c0.122 2.694 1.203 5.139 2.906 7.008l-2.355 2.355c-0.25-0.032-0.506 0.051-0.698 0.243-0.326 0.326-0.326 0.864 0 1.19s0.864 0.326 1.19 0c0.192-0.192 0.269-0.448 0.243-0.698l2.355-2.355c1.869 1.702 4.314 2.784 7.008 2.906v3.322c-0.198 0.154-0.32 0.39-0.32 0.659 0 0.467 0.378 0.845 0.845 0.845s0.845-0.378 0.845-0.845c0-0.269-0.128-0.506-0.32-0.659v-3.322c2.694-0.122 5.139-1.203 7.008-2.906l2.355 2.355c-0.032 0.25 0.051 0.506 0.243 0.698 0.326 0.326 0.864 0.326 1.19 0s0.326-0.864 0-1.19c-0.192-0.192-0.448-0.269-0.698-0.243l-2.355-2.355c1.702-1.869 2.784-4.314 2.906-7.008h3.322c0.154 0.198 0.39 0.32 0.659 0.32 0.467 0 0.845-0.378 0.845-0.845s-0.378-0.845-0.845-0.845zM26.131 15.482h-1.632c-0.090-1.523-0.582-2.976-1.44-4.243-0.16-0.237-0.486-0.301-0.723-0.141s-0.301 0.486-0.141 0.723c0.736 1.094 1.171 2.349 1.261 3.661h-4.314c-0.083-0.493-0.275-0.947-0.557-1.331l4.941-4.941c1.517 1.677 2.483 3.866 2.605 6.272zM18.144 16c0 1.184-0.96 2.144-2.144 2.144s-2.144-0.96-2.144-2.144c0-1.184 0.96-2.144 2.144-2.144 0 0 0 0 0 0s0 0 0 0c1.184 0 2.144 0.96 2.144 2.144zM15.482 12.858c-0.493 0.083-0.947 0.275-1.331 0.557l-3.053-3.053c1.19-1.037 2.714-1.702 4.384-1.818v4.314zM13.408 14.144c-0.282 0.39-0.474 0.845-0.557 1.331h-4.314c0.115-1.67 0.781-3.194 1.818-4.384l3.053 3.053zM12.858 16.518c0.083 0.493 0.275 0.947 0.557 1.331l-3.053 3.053c-1.037-1.19-1.702-2.714-1.818-4.384h4.314zM14.144 18.592c0.39 0.282 0.845 0.474 1.331 0.557v4.314c-1.67-0.115-3.194-0.781-4.384-1.818l3.053-3.053zM16.518 19.142c0.493-0.083 0.947-0.275 1.331-0.557l3.053 3.053c-1.19 1.037-2.714 1.702-4.384 1.818v-4.314zM18.592 17.856c0.282-0.39 0.474-0.845 0.557-1.331h4.314c-0.115 1.67-0.781 3.194-1.818 4.384l-3.053-3.053zM22.79 8.474l-4.934 4.934c-0.39-0.282-0.845-0.474-1.331-0.557v-4.314c1.299 0.090 2.547 0.512 3.635 1.242 0.237 0.16 0.563 0.096 0.723-0.141s0.096-0.563-0.141-0.723c-1.261-0.845-2.707-1.325-4.211-1.421v-1.626c2.394 0.122 4.582 1.088 6.259 2.605zM15.482 5.869v1.626c-1.958 0.122-3.738 0.902-5.12 2.125l-1.152-1.152c1.677-1.51 3.866-2.477 6.272-2.598zM8.474 9.21l1.152 1.152c-1.222 1.382-2.010 3.162-2.125 5.12h-1.632c0.122-2.406 1.088-4.595 2.605-6.272zM5.869 16.518h1.626c0.122 1.958 0.902 3.738 2.125 5.12l-1.152 1.152c-1.51-1.677-2.477-3.866-2.598-6.272zM9.21 23.526l1.152-1.152c1.382 1.222 3.162 2.010 5.12 2.125v1.626c-2.406-0.115-4.595-1.082-6.272-2.598zM16.518 26.131v-1.626c1.958-0.122 3.738-0.902 5.12-2.125l1.152 1.152c-1.677 1.51-3.866 2.477-6.272 2.598zM23.526 22.79l-1.152-1.152c1.222-1.382 2.010-3.162 2.125-5.12h1.626c-0.115 2.406-1.082 4.595-2.598 6.272z"></path>
</symbol>
<symbol id="icon-numbers_icon1" viewBox="0 0 32 32">
<title>numbers_icon1</title>
<path class="path1" d="M26.765 28.646c0.621 0 1.133-0.506 1.133-1.133 0-0.621-0.506-1.133-1.133-1.133-0.621 0-1.133 0.506-1.133 1.133s0.506 1.133 1.133 1.133zM26.765 26.95c0.314 0 0.57 0.256 0.57 0.57s-0.256 0.57-0.57 0.57c-0.314 0-0.57-0.256-0.57-0.57s0.256-0.57 0.57-0.57z"></path>
<path class="path2" d="M23.366 28.646c0.621 0 1.133-0.506 1.133-1.133 0-0.621-0.506-1.133-1.133-1.133-0.621 0-1.133 0.506-1.133 1.133s0.506 1.133 1.133 1.133zM23.366 26.95c0.314 0 0.57 0.256 0.57 0.57s-0.256 0.57-0.57 0.57c-0.314 0-0.57-0.256-0.57-0.57s0.256-0.57 0.57-0.57z"></path>
<path class="path3" d="M15.296 21.382h15.296c0 0 0 0 0 0 0.154 0 0.282-0.128 0.282-0.282 0-0.070-0.026-0.134-0.070-0.186l-15.283-20.8c-0.070-0.096-0.198-0.141-0.314-0.102s-0.192 0.147-0.192 0.269v20.819c0 0.154 0.122 0.282 0.282 0.282zM15.571 1.139l14.458 19.68h-12.736c0 0 0 0 0 0v-8.928c0-0.154-0.128-0.282-0.282-0.282s-0.282 0.128-0.282 0.282v8.922c0 0 0 0 0 0h-1.158v-19.674z"></path>
<path class="path4" d="M2.349 21.382h11.526c0.154 0 0.282-0.128 0.282-0.282v-15.693c0-0.122-0.077-0.23-0.192-0.269s-0.243 0-0.314 0.102l-11.526 15.693c-0.064 0.083-0.070 0.198-0.026 0.294 0.045 0.090 0.147 0.154 0.25 0.154zM13.594 6.272v14.547h-1.491c0 0 0 0 0 0v-5.101c0-0.154-0.128-0.282-0.282-0.282s-0.282 0.128-0.282 0.282v5.101c0 0 0 0 0 0h-8.634l10.688-14.547z"></path>
<path class="path5" d="M31.437 22.374h-30.874c-0.154 0-0.282 0.128-0.282 0.282 0 2.125 0.41 3.808 1.254 5.165 0.774 1.242 1.901 2.163 3.437 2.822 2.918 1.254 7.046 1.357 11.027 1.357s8.109-0.102 11.027-1.357c1.536-0.659 2.662-1.581 3.437-2.822 0.845-1.35 1.254-3.040 1.254-5.165 0-0.154-0.128-0.282-0.282-0.282zM31.059 24.262h-24.173c-0.154 0-0.282 0.128-0.282 0.282s0.128 0.282 0.282 0.282h24.090c-0.454 2.579-1.818 4.288-4.173 5.299-2.816 1.21-6.88 1.312-10.803 1.312s-7.987-0.102-10.803-1.312c-2.893-1.242-4.282-3.533-4.346-7.187h1.549c0.205 3.046 1.581 5.030 4.205 6.074 2.464 0.979 5.862 1.062 9.139 1.062 0.154 0 0.282-0.128 0.282-0.282s-0.128-0.282-0.282-0.282c-3.219 0-6.56-0.077-8.934-1.018-2.426-0.96-3.654-2.733-3.853-5.549h28.192c-0.006 0.454-0.038 0.896-0.090 1.318z"></path>
<path class="path6" d="M11.821 14.061c0.154 0 0.282-0.128 0.282-0.282v-2.688c0-0.154-0.128-0.282-0.282-0.282s-0.282 0.128-0.282 0.282v2.688c0 0.16 0.128 0.282 0.282 0.282z"></path>
<path class="path7" d="M17.018 10.24c0.154 0 0.282-0.128 0.282-0.282v-2.688c0-0.154-0.128-0.282-0.282-0.282s-0.282 0.128-0.282 0.282v2.688c0 0.154 0.122 0.282 0.282 0.282z"></path>
</symbol>
<symbol id="icon-numbers_icon2" viewBox="0 0 32 32">
<title>numbers_icon2</title>
<path class="path1" d="M16 9.037c-3.84 0-6.957 3.123-6.957 6.963s3.123 6.963 6.957 6.963c3.84 0 6.963-3.123 6.963-6.963-0.006-3.84-3.123-6.963-6.963-6.963zM20.666 12.32l-1.901 1.901c-0.256-0.397-0.589-0.73-0.986-0.986l1.901-1.901c0.365 0.288 0.698 0.621 0.986 0.986zM21.946 16c0 0.237-0.013 0.467-0.038 0.698h-2.688c0.051-0.224 0.077-0.461 0.077-0.698s-0.026-0.474-0.077-0.698h2.688c0.019 0.23 0.038 0.461 0.038 0.698zM16 18.278c-1.254 0-2.278-1.024-2.278-2.278s1.024-2.278 2.278-2.278c1.254 0 2.278 1.024 2.278 2.278s-1.024 2.278-2.278 2.278zM10.054 16c0-0.237 0.013-0.467 0.038-0.698h2.688c-0.051 0.224-0.077 0.461-0.077 0.698s0.026 0.474 0.077 0.698h-2.688c-0.026-0.23-0.038-0.461-0.038-0.698zM10.31 14.285c0.115-0.384 0.269-0.755 0.454-1.101l1.101 1.101h-1.555zM11.866 17.715l-1.101 1.101c-0.186-0.346-0.339-0.717-0.454-1.101h1.555zM21.69 17.715c-0.115 0.384-0.269 0.755-0.454 1.101l-1.101-1.101h1.555zM20.134 14.285l1.101-1.101c0.186 0.346 0.339 0.717 0.454 1.101h-1.555zM17.715 11.866v-1.555c0.384 0.115 0.755 0.269 1.101 0.454l-1.101 1.101zM16.698 10.099v2.688c-0.224-0.051-0.461-0.077-0.698-0.077s-0.474 0.026-0.698 0.077v-2.688c0.23-0.026 0.461-0.038 0.698-0.038 0.237-0.006 0.467 0.006 0.698 0.038zM14.285 11.866l-1.101-1.101c0.346-0.186 0.717-0.339 1.101-0.454v1.555zM12.32 11.334l1.901 1.901c-0.397 0.256-0.73 0.589-0.986 0.986l-1.901-1.901c0.288-0.365 0.621-0.698 0.986-0.986zM11.334 19.68l1.901-1.901c0.256 0.397 0.589 0.73 0.986 0.986l-1.901 1.901c-0.365-0.288-0.698-0.621-0.986-0.986zM14.285 20.134v1.555c-0.384-0.115-0.755-0.269-1.101-0.454l1.101-1.101zM15.302 21.901v-2.688c0.224 0.051 0.461 0.077 0.698 0.077s0.474-0.026 0.698-0.077v2.688c-0.23 0.026-0.461 0.038-0.698 0.038-0.237 0.006-0.467-0.013-0.698-0.038zM17.715 20.134l1.101 1.101c-0.346 0.186-0.717 0.339-1.101 0.454v-1.555zM19.68 20.666l-1.901-1.901c0.397-0.256 0.73-0.589 0.986-0.986l1.901 1.901c-0.288 0.365-0.621 0.698-0.986 0.986z"></path>
<path class="path2" d="M31.456 14.746c-0.346-0.32-0.819-0.486-1.286-0.454l-3.994 0.282c-0.25-1.766-0.941-3.386-1.971-4.749l3.021-2.624c0.032-0.026 0.058-0.051 0.096-0.083 0.666-0.666 0.666-1.754 0-2.426-0.333-0.333-0.8-0.518-1.274-0.499s-0.922 0.23-1.235 0.589l-2.63 3.027c-1.357-1.024-2.982-1.722-4.749-1.971l0.282-3.987c0.006-0.038 0.006-0.083 0.006-0.128-0.006-0.954-0.774-1.722-1.722-1.722-0.474 0-0.934 0.198-1.254 0.544s-0.486 0.819-0.454 1.286l0.282 3.994c-1.766 0.25-3.386 0.941-4.749 1.971l-2.63-3.027c-0.314-0.358-0.762-0.576-1.235-0.589-0.474-0.019-0.941 0.166-1.274 0.499s-0.518 0.8-0.499 1.274c0.019 0.474 0.23 0.922 0.589 1.235l3.027 2.63c-1.024 1.357-1.722 2.982-1.971 4.749l-3.994-0.282c-0.474-0.032-0.941 0.134-1.293 0.454-0.346 0.326-0.544 0.787-0.544 1.261s0.198 0.928 0.544 1.254c0.32 0.294 0.742 0.461 1.171 0.461 0.038 0 0.077 0 0.122-0.006l3.994-0.282c0.25 1.766 0.941 3.386 1.971 4.749l-3.027 2.63c-0.358 0.314-0.576 0.762-0.589 1.235-0.019 0.474 0.166 0.941 0.499 1.274 0.32 0.32 0.762 0.499 1.216 0.499 0.019 0 0.038 0 0.058 0 0.474-0.019 0.922-0.23 1.235-0.589l2.63-3.027c1.357 1.024 2.982 1.722 4.749 1.971l-0.282 3.994c-0.032 0.474 0.134 0.941 0.454 1.293 0.326 0.346 0.781 0.544 1.254 0.544s0.928-0.198 1.254-0.544c0.326-0.346 0.493-0.819 0.454-1.293l-0.282-3.994c1.766-0.25 3.386-0.941 4.749-1.971l2.624 3.021c0.026 0.032 0.051 0.064 0.083 0.090 0.32 0.32 0.749 0.499 1.21 0.499 0 0 0 0 0.006 0 0.461 0 0.89-0.179 1.216-0.506 0.333-0.333 0.518-0.8 0.499-1.274s-0.23-0.922-0.589-1.235l-3.027-2.63c1.024-1.357 1.722-2.982 1.971-4.749l3.987 0.282c0.038 0.006 0.083 0.006 0.128 0.006 0.947 0 1.715-0.768 1.715-1.715 0-0.461-0.198-0.922-0.544-1.242zM25.574 5.44c0.128-0.147 0.307-0.23 0.506-0.243 0.198-0.006 0.384 0.064 0.518 0.205 0.275 0.275 0.275 0.717 0 0.986-0.013 0.013-0.019 0.019-0.038 0.032l-3.014 2.618c-0.192-0.205-0.384-0.403-0.589-0.589l2.618-3.008zM15.302 1.766c-0.013-0.198 0.051-0.384 0.186-0.525s0.314-0.224 0.512-0.224c0.384 0 0.698 0.314 0.698 0.698 0 0.013 0 0.032 0 0.051l-0.282 3.974c-0.141-0.006-0.275-0.006-0.416-0.006s-0.282 0.006-0.416 0.006l-0.282-3.974zM5.44 6.426c-0.147-0.128-0.237-0.307-0.243-0.506s0.064-0.378 0.205-0.518c0.141-0.141 0.32-0.211 0.518-0.205s0.378 0.090 0.506 0.243l2.618 3.014c-0.205 0.192-0.403 0.384-0.589 0.589l-3.014-2.618zM1.766 16.698c-0.198 0.013-0.384-0.051-0.525-0.186s-0.224-0.314-0.224-0.512c0-0.198 0.077-0.378 0.224-0.512 0.141-0.134 0.333-0.198 0.525-0.186l3.974 0.282c-0.006 0.141-0.006 0.275-0.006 0.416s0.006 0.282 0.006 0.416l-3.974 0.282zM6.426 26.56c-0.128 0.147-0.307 0.237-0.506 0.243s-0.378-0.064-0.518-0.205c-0.141-0.141-0.211-0.326-0.205-0.518 0.006-0.198 0.090-0.378 0.243-0.506l3.014-2.618c0.192 0.205 0.384 0.403 0.589 0.589l-2.618 3.014zM16.698 30.234c0.013 0.198-0.051 0.384-0.186 0.525s-0.314 0.224-0.512 0.224c-0.198 0-0.378-0.077-0.512-0.224-0.134-0.141-0.198-0.333-0.186-0.525l0.282-3.974c0.141 0.006 0.275 0.006 0.416 0.006s0.282-0.006 0.416-0.006l0.282 3.974zM26.56 25.574c0.147 0.128 0.23 0.307 0.243 0.506 0.006 0.198-0.064 0.384-0.205 0.518-0.134 0.134-0.307 0.205-0.493 0.205 0 0 0 0 0 0-0.186 0-0.365-0.070-0.493-0.205-0.013-0.013-0.019-0.019-0.032-0.038l-2.618-3.014c0.205-0.192 0.403-0.384 0.589-0.589l3.008 2.618zM16 25.254c-5.101 0-9.254-4.154-9.254-9.254s4.154-9.254 9.254-9.254 9.254 4.154 9.254 9.254-4.154 9.254-9.254 9.254zM30.285 16.698c-0.013 0-0.032 0-0.051 0l-3.974-0.282c0.006-0.141 0.006-0.275 0.006-0.416s-0.006-0.282-0.006-0.416l3.974-0.282c0.192-0.013 0.384 0.051 0.525 0.186s0.224 0.314 0.224 0.512c0 0.384-0.314 0.698-0.698 0.698z"></path>
</symbol>
<symbol id="icon-numbers_icon3" viewBox="0 0 46 32">
<title>numbers_icon3</title>
<path class="path1" d="M46.132 8.065c0.407-3.006-0.277-5.17-2.025-6.437-3.829-2.784-12.402-0.897-26.21 5.762-6.77 3.265-11.024 6.16-13.41 8.018-2.155 1.683-4.68 3.977-4.476 5.42 0.194 1.378 2.109 1.609 3.533 1.665 0.86 0.037 1.702-0.009 2.22-0.037l0.324 2.22c0.083 0.546 0.37 1.017 0.814 1.341 0.351 0.25 0.758 0.388 1.184 0.388 0.12 0 0.231-0.009 0.351-0.028 1.526-0.259 4.476-0.684 7.279-0.583 4.994 3.672 12.319 5.54 21.799 5.54 1.323 0 2.432-0.037 3.329-0.055 0.555-0.018 0.999-0.028 1.323-0.028 1.323 0 1.85-0.573 2.053-1.064 0.684-1.591-1.406-4.227-2.849-5.79l-0.499-7.195c0.99-1.091 4.754-5.392 5.262-9.138zM6.002 19.468c-0.37 0.111-0.601 0.472-0.546 0.851l0.093 0.601c-0.425 0.028-1.017 0.046-1.637 0.037-1.655-0.028-2.201-0.287-2.349-0.388 0.139-0.416 1.064-1.794 4.143-4.153 2.303-1.766 6.409-4.523 12.865-7.639 16.277-7.852 22.381-7.547 24.638-5.91 1.267 0.925 1.729 2.553 1.397 4.985-0.425 3.163-3.755 7.066-4.791 8.203-0.12-0.028-0.268-0.055-0.425-0.083 0.12-0.472 0.194-0.971 0.213-1.489 0.056-1.434-0.305-2.775-1.017-3.773-0.786-1.11-1.979-1.748-3.348-1.803-2.858-0.111-5.225 2.183-5.336 5.133-0.018 0.462 0.018 0.916 0.111 1.35-7.306 0.176-15.371 1.545-24.009 4.079zM38.067 14.428c-0.102 2.673-1.711 3.616-3.052 3.616-0.046 0-0.093 0-0.139 0-0.953-0.037-1.831-0.462-2.488-1.202-0.657-0.749-0.999-1.72-0.962-2.738 0.083-2.053 1.665-3.662 3.607-3.662 0.046 0 0.093 0 0.139 0 1.517 0.065 2.997 1.313 2.895 3.986zM16 24.259v0c-3.006-0.139-6.197 0.314-7.824 0.592-0.129 0.018-0.259-0.009-0.37-0.083-0.102-0.074-0.176-0.194-0.194-0.324l-0.536-3.69c8.481-2.432 16.379-3.727 23.501-3.838 0.185 0.333 0.407 0.647 0.666 0.943 0.934 1.054 2.201 1.665 3.579 1.72 0.065 0 0.129 0 0.203 0 0 0 0 0 0 0 1.341 0 2.488-0.527 3.329-1.526 0.157-0.194 0.305-0.398 0.435-0.62 0.213 0.037 0.398 0.074 0.555 0.102l0.259 3.709c-0.897-0.046-2.876-0.139-5.79-0.139-0.083 0-0.157 0-0.24 0-3.82 0.009-9.951 0.176-17.508 0.925-0.425 0.046-0.731 0.416-0.694 0.842 0.037 0.398 0.37 0.694 0.768 0.694 0.028 0 0.055 0 0.074 0 12.846-1.276 21.734-0.869 23.491-0.768l0.093 1.286c-2.395 0.37-5.993 0.555-10.719 0.555-6.557 0-12.994-0.37-13.059-0.379-0.009 0-0.018 0-0.018 0zM18.784 25.942c2.516 0.111 6.381 0.24 10.294 0.24 3.681 0 6.696-0.111 9.027-0.342l1.776 3.921c-0.703 0.018-1.498 0.037-2.377 0.037-7.834-0.009-14.113-1.304-18.719-3.857zM42.793 29.577c-0.009 0.018-0.157 0.129-0.638 0.129-0.176 0-0.37 0-0.601 0.009l-1.84-4.060c0.213-0.028 0.425-0.065 0.629-0.093 2.007 2.183 2.599 3.681 2.451 4.014z"></path>
<path class="path2" d="M34.858 15.88c-1.387-0.49-0.943-2.839-0.934-2.867 0.083-0.416-0.185-0.823-0.601-0.906s-0.823 0.185-0.906 0.601c-0.009 0.037-0.176 0.888-0.065 1.85 0.074 0.601 0.24 1.128 0.499 1.554 0.351 0.583 0.851 0.999 1.498 1.221 0.083 0.028 0.166 0.046 0.259 0.046 0.314 0 0.62-0.194 0.731-0.518 0.139-0.398-0.074-0.832-0.481-0.98z"></path>
</symbol>
<symbol id="icon-numbers_icon4" viewBox="0 0 29 32">
<title>numbers_icon4</title>
<path class="path1" d="M28.442 31.008h-3.379v-1.018c0-2.336-0.621-4.134-1.85-5.35-0.109-0.109-0.224-0.211-0.333-0.307-0.006-0.006-0.013-0.006-0.013-0.013-1.491-1.235-3.181-1.242-3.258-1.242h-1.344c0 0 0 0 0 0h-1.146v-0.781c0-0.026 0-0.051-0.006-0.077 1.594-0.659 2.605-1.76 3.219-2.714 0.646-1.005 0.96-1.99 1.088-2.534 0.39-0.077 0.73-0.262 0.992-0.557 0.704-0.781 0.666-2.010 0.614-2.502-0.038-0.378-0.205-0.691-0.493-0.909-0.013-0.006-0.019-0.019-0.032-0.026 0.275-0.781 0.39-1.517 0.352-2.202-0.013-0.256-0.051-0.461-0.083-0.627 0.070-0.077 0.122-0.179 0.134-0.288l0.704-6.278c0.109-0.205 0.064-0.461-0.122-0.614-0.058-0.051-1.459-1.21-3.488-1.165-1.088-1.088-3.136-1.811-5.536-1.811s-4.448 0.717-5.536 1.811c-1.67-0.006-2.906 0.8-3.277 1.075-0.064 0.019-0.122 0.051-0.166 0.096-0.122 0.109-0.186 0.269-0.166 0.429l0.73 6.464c0.013 0.109 0.058 0.205 0.128 0.275-0.038 0.166-0.077 0.378-0.090 0.634-0.038 0.685 0.083 1.427 0.352 2.202-0.013 0.006-0.026 0.019-0.032 0.026-0.282 0.218-0.454 0.538-0.493 0.909-0.051 0.493-0.090 1.722 0.614 2.502 0.269 0.294 0.608 0.48 1.005 0.557 0.134 0.544 0.442 1.53 1.088 2.534 0.614 0.954 1.619 2.054 3.219 2.714-0.006 0.026-0.006 0.051-0.006 0.077v0.774h-2.49c-0.077 0-1.766 0.006-3.258 1.242-0.006 0.006-0.013 0.006-0.019 0.013-0.115 0.096-0.224 0.192-0.333 0.301-1.229 1.216-1.85 3.021-1.85 5.35v1.018h-3.386c-0.275 0-0.499 0.224-0.499 0.499s0.224 0.499 0.499 0.499h27.942c0.275 0 0.499-0.224 0.499-0.499 0-0.269-0.224-0.486-0.499-0.486zM24.070 29.984v1.018h-8.256l6.733-5.632c1.005 1.030 1.523 2.579 1.523 4.614zM6.989 9.414l-0.614-5.427c1.274 0.25 4.198 0.73 8.064 0.73 2.778 0 5.504-0.25 8.115-0.742l-0.614 5.427c-2.509 0.493-5.082 0.742-7.654 0.742-3.699 0-6.394-0.531-7.296-0.73zM21.67 15.757c-0.045 0.051-0.102 0.096-0.154 0.134v-2.227c0.070 0.006 0.147 0.019 0.211 0.038 0 0 0.006 0 0.006 0 0.070 0.019 0.134 0.051 0.186 0.090s0.096 0.096 0.115 0.224c0.051 0.582-0.013 1.344-0.365 1.741zM21.51 12.659v-2.144c0-0.006 0-0.013 0-0.026 0.096-0.019 0.198-0.038 0.294-0.058 0.077 0.371 0.128 1.126-0.256 2.227-0.006 0.006-0.019 0.006-0.038 0zM21.722 3.11c-0.307 0.051-0.614 0.102-0.922 0.147-0.026-0.141-0.064-0.282-0.115-0.416 0.39 0.051 0.742 0.154 1.037 0.269zM10.56 1.85c1.037-0.55 2.426-0.858 3.91-0.858s2.874 0.307 3.91 0.858c0.813 0.435 1.318 0.986 1.434 1.542-1.754 0.218-3.552 0.326-5.37 0.326-2.086 0-3.904-0.147-5.318-0.32 0.102-0.557 0.614-1.114 1.434-1.549zM8.134 3.27c-0.333-0.051-0.634-0.096-0.896-0.141 0.288-0.115 0.627-0.218 1.005-0.275-0.045 0.134-0.083 0.275-0.109 0.416zM7.117 10.458c0.096 0.019 0.198 0.038 0.314 0.064v2.138c-0.019 0-0.038 0-0.051 0.006-0.384-1.082-0.339-1.83-0.262-2.208zM6.899 14.022c0.013-0.134 0.064-0.186 0.109-0.224 0.051-0.038 0.115-0.070 0.186-0.090 0 0 0.006 0 0.006 0 0.070-0.019 0.147-0.032 0.224-0.045v2.234c-0.064-0.038-0.115-0.083-0.166-0.141-0.352-0.397-0.416-1.158-0.358-1.734zM9.472 19.002c-0.781-1.197-1.011-2.374-1.050-2.579v-5.728c1.376 0.218 3.392 0.442 5.869 0.442 2.086 0 4.173-0.16 6.221-0.48v5.766c-0.032 0.205-0.262 1.357-1.030 2.547-1.114 1.728-2.79 2.682-4.973 2.829h-0.090c-2.17-0.141-3.834-1.082-4.947-2.797zM12.32 24.070c0.275 0 0.499-0.224 0.499-0.499v-1.024c0.474 0.122 0.992 0.205 1.555 0.243 0.013 0 0.019 0 0.032 0h0.128c0.013 0 0.019 0 0.032 0 0.563-0.038 1.082-0.122 1.555-0.243v1.024c0 0.275 0.224 0.499 0.499 0.499h0.685l-2.944 4.243-2.752-4.243h0.71zM10.419 24.070l3.507 5.402c0.090 0.141 0.243 0.224 0.41 0.224 0 0 0.006 0 0.006 0 0.16 0 0.314-0.077 0.41-0.211l3.763-5.414h1.094c0 0 0 0 0 0 0.032 0 1.075 0.006 2.138 0.666l-7.232 6.048-7.328-6.054c1.037-0.646 2.054-0.659 2.144-0.659h1.088zM4.87 29.984c0-2.349 0.691-3.763 1.53-4.614l6.822 5.638h-8.352v-1.024z"></path>
<path class="path2" d="M11.539 12.928c0 0.329-0.266 0.595-0.595 0.595s-0.595-0.266-0.595-0.595c0-0.329 0.266-0.595 0.595-0.595s0.595 0.266 0.595 0.595z"></path>
<path class="path3" d="M18.592 12.928c0 0.329-0.266 0.595-0.595 0.595s-0.595-0.266-0.595-0.595c0-0.329 0.266-0.595 0.595-0.595s0.595 0.266 0.595 0.595z"></path>
<path class="path4" d="M14.342 16.422h0.256c0.659 0 1.19-0.538 1.19-1.19 0-0.275-0.224-0.499-0.499-0.499s-0.499 0.224-0.499 0.499c0 0.109-0.090 0.198-0.198 0.198h-0.256c-0.109 0-0.198-0.090-0.198-0.198 0-0.275-0.224-0.499-0.499-0.499s-0.499 0.224-0.499 0.499c0.006 0.659 0.544 1.19 1.203 1.19z"></path>
<path class="path5" d="M16.307 7.546c0-0.275-0.224-0.499-0.499-0.499s-0.499 0.224-0.499 0.499c0 0.237-0.109 0.448-0.275 0.595v-1.12c0.275 0 0.493-0.224 0.493-0.499s-0.224-0.499-0.499-0.499h-0.192c0.038-0.058 0.058-0.122 0.058-0.198 0-0.198-0.16-0.358-0.358-0.358s-0.358 0.16-0.358 0.358c0 0.070 0.019 0.141 0.058 0.198h-0.192c-0.275 0-0.499 0.224-0.499 0.499s0.224 0.493 0.493 0.499v1.12c-0.166-0.141-0.275-0.352-0.275-0.595 0-0.275-0.224-0.499-0.499-0.499s-0.499 0.224-0.499 0.499c0 0.973 0.794 1.766 1.766 1.766 0.986 0 1.779-0.794 1.779-1.766z"></path>
<path class="path6" d="M17.555 18.413c0.173-0.211 0.141-0.525-0.077-0.698-0.211-0.173-0.525-0.141-0.698 0.077-0.006 0.006-0.602 0.659-2.189 0.659-1.645 0-2.445-0.685-2.483-0.717-0.205-0.179-0.518-0.166-0.698 0.032-0.186 0.205-0.173 0.518 0.032 0.704 0.045 0.038 1.094 0.973 3.155 0.973 2.067 0 2.867-0.922 2.957-1.030z"></path>
</symbol>
<symbol id="icon-callback" viewBox="0 0 32 32">
<title>callback</title>
<path class="path1" d="M31.283 25.286l-4.941-4.941c-0.986-0.979-2.618-0.947-3.635 0.070l-2.49 2.49c-0.16-0.090-0.32-0.179-0.493-0.275-1.574-0.87-3.725-2.067-5.99-4.333-2.272-2.272-3.469-4.429-4.339-5.997-0.090-0.166-0.179-0.326-0.269-0.48l1.67-1.67 0.819-0.826c1.018-1.018 1.050-2.65 0.064-3.635l-4.941-4.941c-0.973-0.979-2.605-0.947-3.622 0.070l-1.395 1.402 0.038 0.038c-0.467 0.595-0.858 1.28-1.146 2.022-0.269 0.71-0.435 1.382-0.512 2.054-0.653 5.414 1.818 10.355 8.525 17.062 9.274 9.274 16.749 8.576 17.069 8.538 0.704-0.083 1.376-0.25 2.061-0.518 0.736-0.288 1.421-0.678 2.016-1.146l0.032 0.026 1.408-1.382c1.024-1.011 1.050-2.643 0.070-3.629zM30.118 27.827l-0.621 0.621-0.25 0.237c-0.39 0.378-1.062 0.909-2.048 1.293-0.557 0.218-1.114 0.352-1.67 0.422-0.070 0.006-7.098 0.608-15.802-8.090-7.398-7.405-8.582-11.667-8.090-15.795 0.064-0.544 0.198-1.101 0.416-1.67 0.39-0.992 0.915-1.664 1.293-2.054l0.864-0.87c0.416-0.416 1.062-0.448 1.44-0.070l4.941 4.941c0.378 0.378 0.346 1.024-0.064 1.44l-3.334 3.334 0.307 0.518c0.173 0.288 0.352 0.614 0.544 0.966 0.915 1.658 2.176 3.923 4.602 6.342 2.419 2.419 4.685 3.674 6.336 4.589 0.358 0.198 0.685 0.378 0.973 0.55l0.518 0.307 3.334-3.334c0.416-0.416 1.062-0.448 1.446-0.070l4.941 4.941c0.371 0.39 0.339 1.037-0.077 1.453z"></path>
</symbol>
<symbol id="icon-inst" viewBox="0 0 32 32">
<title>inst</title>
<path class="path1" d="M4.107 0h23.785c2.259 0 4.107 1.672 4.107 4.107v23.786c0 2.435-1.848 4.107-4.107 4.107h-23.785c-2.26 0-4.107-1.672-4.107-4.107v-23.786c0-2.435 1.848-4.107 4.107-4.107v0zM23.311 3.555c-0.792 0-1.44 0.648-1.44 1.441v3.448c0 0.792 0.648 1.441 1.44 1.441h3.617c0.792 0 1.441-0.648 1.441-1.441v-3.448c0-0.793-0.648-1.441-1.441-1.441h-3.617zM28.383 13.533h-2.816c0.266 0.87 0.411 1.792 0.411 2.745 0 5.323-4.454 9.637-9.947 9.637s-9.946-4.315-9.946-9.637c0-0.954 0.144-1.875 0.411-2.745h-2.939v13.517c0 0.7 0.572 1.272 1.272 1.272h22.284c0.7 0 1.272-0.572 1.272-1.272v-13.517h-0.001zM16.030 9.703c-3.549 0-6.427 2.788-6.427 6.227s2.878 6.227 6.427 6.227c3.549 0 6.427-2.788 6.427-6.227s-2.878-6.227-6.427-6.227z"></path>
</symbol>
<symbol id="icon-youtube" viewBox="0 0 32 32">
<title>youtube</title>
<path class="path1" d="M25.223 23.407h-1.756l0.008-1.020c0-0.453 0.372-0.824 0.827-0.824h0.112c0.456 0 0.829 0.371 0.829 0.824l-0.020 1.020zM18.636 21.221c-0.445 0-0.81 0.299-0.81 0.666v4.961c0 0.366 0.364 0.665 0.81 0.665 0.447 0 0.812-0.299 0.812-0.665v-4.961c0-0.367-0.365-0.666-0.812-0.666zM29.333 18.446v9.438c0 2.264-1.963 4.116-4.363 4.116h-17.941c-2.4 0-4.363-1.852-4.363-4.116v-9.438c0-2.264 1.963-4.117 4.363-4.117h17.941c2.4 0 4.363 1.853 4.363 4.117zM8.226 28.908l-0.001-9.942 2.224 0.001v-1.473l-5.929-0.009v1.448l1.851 0.005v9.969h1.855zM14.893 20.448h-1.854v5.309c0 0.768 0.047 1.152-0.003 1.287-0.151 0.412-0.828 0.849-1.093 0.044-0.045-0.141-0.005-0.566-0.006-1.295l-0.007-5.345h-1.844l0.006 5.262c0.001 0.806-0.018 1.408 0.006 1.681 0.045 0.483 0.029 1.045 0.477 1.366 0.834 0.601 2.433-0.090 2.833-0.949l-0.004 1.096 1.489 0.002-0-8.459zM20.825 26.527l-0.004-4.418c-0.001-1.684-1.261-2.692-2.971-1.33l0.007-3.285-1.852 0.003-0.009 11.339 1.523-0.022 0.139-0.706c1.946 1.785 3.169 0.562 3.167-1.58zM26.628 25.941l-1.39 0.007c-0.001 0.055-0.003 0.119-0.004 0.188v0.776c0 0.415-0.343 0.753-0.76 0.753h-0.272c-0.417 0-0.761-0.338-0.761-0.753v-2.041h3.184v-1.198c0-0.876-0.022-1.751-0.095-2.252-0.228-1.584-2.451-1.835-3.574-1.024-0.352 0.253-0.622 0.592-0.778 1.047-0.158 0.455-0.236 1.078-0.236 1.868v2.635c0.001 4.38 5.321 3.761 4.686-0.006zM19.496 11.638c0.096 0.233 0.244 0.421 0.446 0.563 0.199 0.14 0.454 0.21 0.759 0.21 0.267 0 0.504-0.072 0.71-0.221s0.379-0.37 0.52-0.665l-0.035 0.727h2.067v-8.79h-1.627v6.841c0 0.37-0.305 0.674-0.678 0.674-0.371 0-0.677-0.303-0.677-0.674v-6.841h-1.698v5.929c0 0.755 0.014 1.259 0.036 1.514 0.023 0.254 0.081 0.497 0.177 0.732zM13.233 6.674c0-0.844 0.070-1.503 0.21-1.978 0.141-0.473 0.394-0.854 0.76-1.141 0.365-0.288 0.833-0.433 1.401-0.433 0.478 0 0.888 0.094 1.23 0.278 0.344 0.185 0.609 0.425 0.793 0.721 0.187 0.297 0.315 0.603 0.383 0.915 0.069 0.317 0.103 0.795 0.103 1.439v2.223c0 0.815-0.033 1.415-0.096 1.796-0.063 0.382-0.198 0.736-0.407 1.068-0.206 0.329-0.473 0.574-0.795 0.731-0.326 0.158-0.7 0.236-1.121 0.236-0.471 0-0.868-0.065-1.195-0.201-0.328-0.135-0.582-0.339-0.762-0.609-0.182-0.271-0.311-0.601-0.388-0.986s-0.115-0.963-0.115-1.733l0.001-2.327zM14.852 10.165c0 0.498 0.37 0.904 0.822 0.904s0.821-0.406 0.821-0.904v-4.679c0-0.497-0.369-0.903-0.821-0.903s-0.822 0.406-0.822 0.903v4.679zM9.131 12.528h1.95l0.002-6.741 2.304-5.775h-2.133l-1.225 4.289-1.242-4.301h-2.111l2.451 5.79 0.003 6.738z"></path>
</symbol>
<symbol id="icon-fb" viewBox="0 0 32 32">
<title>fb</title>
<path class="path1" d="M23.999 0.007l-4.15-0.007c-4.662 0-7.675 3.091-7.675 7.875v3.631h-4.172c-0.361 0-0.652 0.292-0.652 0.653v5.261c0 0.361 0.292 0.652 0.652 0.652h4.172v13.275c0 0.361 0.292 0.653 0.652 0.653h5.444c0.361 0 0.652-0.292 0.652-0.652v-13.275h4.878c0.361 0 0.652-0.292 0.652-0.652l0.002-5.261c0-0.173-0.069-0.339-0.191-0.461s-0.289-0.191-0.462-0.191h-4.88v-3.078c0-1.479 0.353-2.23 2.28-2.23l2.795-0.001c0.36 0 0.652-0.292 0.652-0.652v-4.885c0-0.36-0.292-0.652-0.651-0.652z"></path>
</symbol>
<symbol id="footer_logo" viewBox="0 0 1024 1024">
<title>footer_logo</title>
<path class="path1" d="M811.662 372.907c16.64-35.556 29.156-79.644 35.129-134.542-15.502-1.707-30.151-2.56-44.373-2.844-0.569 48.782-8.676 91.876-24.32 129.422 11.236 1.849 22.329 4.409 33.564 7.964z"></path>
<path class="path2" d="M660.196 248.604c0.569 3.271 0.853 6.542 1.28 9.671 3.698 26.738 3.556 53.049-1.138 78.933-2.418 13.084-5.973 25.316-10.24 36.978 24.32-8.533 51.342-14.080 81.209-14.080 9.244 0 18.347 0.569 27.591 1.564 14.791-33.707 23.893-75.378 24.462-126.578 0-3.129 0.142-6.4 0-9.529 0-3.129-0.142-6.4-0.284-9.671-0.427-14.080-1.28-28.587-2.987-44.089-52.907 5.973-95.858 17.92-130.844 33.849 3.698 11.093 6.827 22.044 9.102 32.996 0.853 3.413 1.28 6.684 1.849 9.956z"></path>
<path class="path3" d="M374.187 206.364c-34.987-15.787-77.796-27.733-130.844-33.422-1.707 15.502-2.56 30.009-2.844 44.089 46.649 0.569 88.32 7.964 124.729 22.329 2.276-10.951 5.262-22.044 8.96-32.996z"></path>
<path class="path4" d="M245.618 364.8c3.271-0.569 6.542-1.138 9.813-1.564 3.129-0.427 6.258-1.138 9.387-1.422 9.387-0.996 18.773-1.707 28.018-1.707 29.724 0 56.747 5.404 81.067 13.938-4.124-11.662-7.68-23.751-10.098-36.551-4.836-25.742-4.978-52.053-1.422-78.791-32.853-13.653-72.96-22.187-121.884-22.756-1.707 0-3.271-0.142-4.978-0.142-1.422 0-2.987 0.142-4.409 0.142-3.129 0-6.4 0.142-9.671 0.284-14.080 0.427-28.729 1.28-44.231 2.987 6.116 54.329 18.489 98.133 35.129 133.547 11.093-3.271 22.187-5.831 33.28-7.964z"></path>
<path class="path5" d="M211.769 652.231c-16.498 35.413-28.729 79.218-34.702 133.404 15.502 1.707 30.151 2.56 44.373 2.844 0.569-48.213 8.391-91.022 23.751-128.142-11.093-1.991-22.187-4.551-33.422-8.107z"></path>
<path class="path6" d="M363.662 775.396c-0.569-3.271-0.853-6.4-1.422-9.671-3.698-26.738-3.556-53.049 1.138-78.933 2.418-12.516 5.831-24.32 9.671-35.556-24.178 8.533-51.058 13.796-80.64 13.796-9.387 0-18.773-0.569-28.16-1.707-14.507 33.564-23.467 74.809-24.036 125.298 0 3.129-0.142 6.258 0 9.529 0 3.129 0.142 6.4 0.284 9.671 0.427 14.080 1.28 28.587 2.987 44.089 52.907-5.973 95.858-17.92 130.844-33.849-3.698-11.093-6.684-22.044-9.102-32.996-0.569-3.129-0.853-6.4-1.564-9.671z"></path>
<path class="path7" d="M649.813 817.636c34.844 15.787 77.796 27.733 130.844 33.422 1.707-15.502 2.418-30.009 2.844-44.089-46.649-0.569-88.32-7.964-124.729-22.329-2.418 10.951-5.404 22.044-8.96 32.996z"></path>
<path class="path8" d="M778.951 660.338c-3.271 0.569-6.542 1.138-9.813 1.564-3.129 0.427-6.258 1.138-9.387 1.564-9.529 1.138-19.058 1.707-28.444 1.707-29.582 0-56.32-5.262-80.498-13.796 3.84 11.236 7.253 22.756 9.671 35.129 4.836 25.742 4.978 52.053 1.422 78.791 32.853 13.653 72.96 22.187 121.884 22.756 1.707 0 3.271 0.142 4.978 0.142 1.422 0 2.987-0.142 4.409-0.142 3.129 0 6.4-0.142 9.671-0.284 14.080-0.427 28.729-1.28 44.231-2.987-5.973-53.76-18.204-97.28-34.56-132.409-11.378 3.271-22.613 5.831-33.564 7.964z"></path>
<path class="path9" d="M599.751 223.289c1.28 2.987 2.276 6.116 3.413 9.244 3.698 9.956 6.684 20.053 9.102 30.151 0.853 3.413 1.849 6.827 2.418 10.24 0.711 3.556 1.138 6.969 1.707 10.524 1.28 8.533 2.276 17.067 2.702 25.742 1.138 30.862-3.84 59.307-11.804 84.907 5.831-3.413 11.947-6.542 18.204-9.529 14.791-33.28 23.324-72.676 18.204-117.902-0.427-3.271-0.996-6.827-1.564-10.098s-0.853-6.542-1.564-9.813c-1.991-10.382-5.12-21.049-8.533-32-0.996-3.129-1.849-6.116-2.987-9.244-1.138-2.987-2.418-6.116-3.556-9.102-13.653-34.418-34.987-71.253-66.56-110.791-12.231 9.813-23.040 19.627-33.422 29.298 31.147 31.858 55.040 64.996 70.542 99.129 1.28 2.987 2.418 6.116 3.698 9.244z"></path>
<path class="path10" d="M416.996 393.956c4.409 2.56 8.676 5.12 12.942 7.822 4.978 3.271 9.671 6.542 14.222 9.956 0.996 0.711 2.133 1.422 3.129 2.133 12.089-7.964 25.742-13.938 40.391-16.924 1.564-0.284 3.129-0.569 4.836-0.853s3.413-0.569 5.12-0.853c1.564-0.142 3.129-0.427 4.693-0.569 1.707-0.142 3.556-0.142 5.262-0.284 1.564 0 2.987-0.284 4.551-0.284 0 0 0 0 0 0s0.142 0 0.142 0c1.564 0 2.987 0.142 4.551 0.284 1.707 0.142 3.556 0.142 5.262 0.284 1.564 0.142 3.129 0.284 4.693 0.569 1.707 0.142 3.413 0.427 5.12 0.711 1.564 0.284 3.271 0.569 4.836 0.853 14.649 3.129 28.16 8.96 40.249 16.924 0.996-0.711 2.133-1.564 3.129-2.276 13.653-32.711 22.756-71.68 18.773-114.916-0.427-3.84-0.853-7.68-1.422-11.52-0.569-3.556-1.138-6.969-1.849-10.524-1.991-9.956-4.409-20.053-7.964-30.293-1.138-3.129-2.418-6.258-3.698-9.387-1.138-3.129-2.276-6.116-3.698-9.244-13.796-31.573-35.698-64.427-68.409-97.849-2.133-2.133-4.409-4.409-6.684-6.542s-4.693-4.409-6.969-6.684c-10.24-9.671-21.191-19.484-33.422-29.156-31.573 39.538-52.764 76.516-66.418 111.076-1.138 2.987-2.56 6.116-3.556 9.102-1.138 3.129-1.991 6.116-2.987 9.244-3.556 10.951-6.542 21.76-8.533 32-0.711 3.271-0.996 6.542-1.564 9.813-0.569 3.413-1.138 6.827-1.564 10.24-4.978 45.084 3.556 84.196 18.347 117.333 6.4 3.413 12.516 6.542 18.489 9.813z"></path>
<path class="path11" d="M424.249 800.711c-1.28-2.987-2.276-6.116-3.413-9.244-3.698-9.956-6.684-20.053-9.102-30.151-0.853-3.413-1.707-6.827-2.418-10.24-0.711-3.556-1.138-6.969-1.707-10.524-1.28-8.533-2.276-17.209-2.702-25.884-1.138-30.151 3.556-58.169 11.378-83.342-5.831 3.413-12.089 6.542-18.489 9.529-14.364 32.853-22.613 71.822-17.493 116.338 0.427 3.271 0.996 6.827 1.564 10.098s0.853 6.542 1.564 9.956c1.991 10.382 4.978 21.049 8.533 32 0.996 3.129 1.849 6.116 2.987 9.244 1.138 2.987 2.418 5.973 3.556 9.102 13.653 34.418 34.987 71.253 66.56 110.791 12.231-9.813 23.040-19.627 33.422-29.298-31.147-31.858-55.040-64.996-70.542-99.129-1.422-2.987-2.418-6.116-3.698-9.244z"></path>
<path class="path12" d="M607.431 631.467c-4.409-2.56-8.676-5.12-12.8-7.822-4.836-3.129-9.529-6.4-13.938-9.671-1.564-1.138-3.271-2.276-4.836-3.556-11.804 7.68-25.173 13.369-39.396 16.498-1.564 0.284-3.129 0.569-4.836 0.853s-3.413 0.569-5.12 0.853c-1.564 0.142-3.129 0.427-4.693 0.427-1.707 0.142-3.556 0.284-5.262 0.284-1.564 0-2.987 0.284-4.551 0.284 0 0 0 0 0 0s-0.142 0-0.142 0c-1.564 0-2.987-0.142-4.551-0.284-1.707-0.142-3.556-0.142-5.262-0.284-1.564-0.142-3.129-0.284-4.693-0.569-1.707-0.142-3.413-0.427-5.12-0.711-1.564-0.284-3.271-0.427-4.836-0.853-14.222-3.129-27.449-8.676-39.253-16.356-1.564 1.138-3.271 2.418-4.978 3.556-13.227 32.284-21.902 70.684-17.92 112.924 0.427 3.84 0.853 7.68 1.422 11.52 0.569 3.556 1.138 6.969 1.849 10.524 1.991 9.956 4.409 20.053 7.964 30.293 0.996 3.129 2.418 6.258 3.698 9.387s2.276 6.116 3.698 9.244c13.796 31.573 35.698 64.427 68.409 97.849 2.133 2.133 4.409 4.409 6.684 6.542s4.693 4.409 6.969 6.684c10.24 9.671 21.191 19.484 33.422 29.156 31.573-39.538 52.764-76.516 66.418-111.076 1.138-2.987 2.56-6.116 3.556-9.102 1.138-3.129 1.991-6.258 2.987-9.244 3.556-10.951 6.542-21.76 8.533-32 0.711-3.271 0.996-6.542 1.564-9.813 0.569-3.413 1.138-6.827 1.564-10.24 4.978-44.373-3.271-82.916-17.636-115.769-6.684-2.844-12.942-6.116-18.916-9.529z"></path>
<path class="path13" d="M220.018 425.813c3.129-1.28 6.4-2.56 9.529-3.698s6.258-2.418 9.387-3.413c10.098-3.413 20.338-6.258 30.72-8.249 3.556-0.711 7.111-1.28 10.667-1.849s7.111-0.996 10.667-1.422c8.818-0.996 17.636-1.564 26.596-1.564v0c27.591 0 53.049 4.836 76.231 12.089 4.836 1.564 9.671 3.129 14.222 4.836 5.404 1.991 10.809 4.124 15.929 6.4 1.138 0.569 2.276 0.996 3.556 1.564 0.853-0.853 1.707-1.707 2.56-2.56 0.427-0.427 0.853-0.853 1.28-1.28-0.853-0.711-1.849-1.28-2.702-1.991-4.267-2.987-8.818-5.973-13.369-8.818-5.404-3.271-10.951-6.542-16.64-9.529-4.693-2.418-9.529-4.978-14.507-7.111-26.596-11.804-57.031-19.627-91.307-19.627-6.116 0-12.516 0.427-18.773 0.853-3.413 0.284-6.827 0.711-10.24 1.138s-6.684 0.853-10.098 1.422c-10.524 1.707-21.333 4.124-32.427 7.396-2.987 0.853-6.258 2.133-9.244 3.129-3.129 0.996-6.258 1.991-9.529 3.271-36.124 13.511-75.093 35.413-117.049 68.978 9.671 12.089 19.342 23.040 28.871 33.138 33.849-33.138 69.12-57.6 105.671-73.102z"></path>
<path class="path14" d="M211.2 631.609c3.129 0.996 6.258 2.276 9.387 3.129 11.093 3.413 21.902 5.689 32.427 7.538 3.413 0.569 6.684 0.996 10.098 1.422s6.827 0.853 10.24 1.138c6.542 0.569 12.942 0.996 19.2 0.996 33.991 0 64.284-7.822 90.738-19.484 4.978-2.133 9.671-4.551 14.222-6.969 5.689-2.987 11.093-5.973 16.213-9.244 4.693-2.844 9.387-5.831 13.653-8.96 1.564-1.138 2.987-2.276 4.551-3.271-0.711-0.711-1.564-1.28-2.276-1.991-0.569-0.569-1.138-1.138-1.707-1.707-0.711-0.711-1.28-1.564-1.991-2.276-1.564-1.707-3.271-3.413-4.693-5.262-0.569-0.711-1.138-1.564-1.849-2.418-1.422-1.849-2.844-3.698-4.267-5.547-0.569-0.853-1.138-1.707-1.707-2.56-7.538-11.52-13.084-24.462-16.356-38.4-0.569-2.133-0.853-4.551-1.28-6.684-0.142-0.853-0.284-1.849-0.427-2.844-0.284-2.418-0.569-4.693-0.853-7.111-0.142-0.853-0.142-1.849-0.142-2.844-0.142-1.849-0.284-3.698-0.284-5.547 0-0.284 0-0.711 0-0.996 0-0.142 0-0.284 0-0.569 0.142-1.991 0.284-3.84 0.427-5.831 0-0.853 0-1.707 0.142-2.56 0.142-2.418 0.427-4.978 0.853-7.396 0.142-0.853 0.284-1.707 0.427-2.56 0.427-2.276 0.711-4.693 1.28-6.969 3.129-14.080 8.818-27.164 16.498-38.827 0.427-0.569 0.711-1.138 0.996-1.707-1.138-0.427-2.133-0.996-3.271-1.422-27.307-11.378-59.164-19.769-94.009-19.769-3.84 0-7.68 0.142-11.662 0.284s-7.964 0.569-11.947 0.996c-3.698 0.427-7.396 0.853-11.093 1.564-10.24 1.564-20.622 3.982-31.147 7.253-3.271 0.996-6.684 2.133-9.956 3.413-3.271 1.138-6.542 2.418-9.813 3.698-33.564 13.511-68.551 36.124-104.249 71.111-2.133 2.133-4.409 4.409-6.542 6.542-2.276 2.418-4.693 4.693-6.969 7.253-9.529 10.24-19.2 21.191-28.871 33.28 41.671 33.28 80.356 55.040 116.338 68.551 3.413 1.564 6.542 2.56 9.671 3.556z"></path>
<path class="path15" d="M609.991 610.276c5.262 3.271 10.667 6.258 16.356 9.244 4.693 2.418 9.387 4.836 14.222 6.969 26.453 11.662 56.604 19.342 90.596 19.342 6.4 0 12.942-0.427 19.484-0.996 3.413-0.284 6.827-0.853 10.24-1.138 3.271-0.427 6.542-0.853 9.956-1.422 10.524-1.849 21.333-4.124 32.569-7.538 3.129-0.853 6.258-2.133 9.244-3.129 3.129-0.996 6.258-1.991 9.529-3.271 35.982-13.511 74.809-35.413 116.48-68.836-9.671-12.089-19.342-23.040-28.871-33.138-33.707 33.138-68.836 57.458-105.102 72.96-3.129 1.28-6.258 2.56-9.529 3.698-3.129 1.138-6.258 2.418-9.387 3.556-10.24 3.556-20.48 6.258-30.72 8.391-3.413 0.711-6.827 1.28-10.382 1.849s-7.111 1.138-10.809 1.422c-9.102 0.996-18.204 1.707-27.449 1.707-27.164 0-52.48-4.693-75.378-11.804-4.836-1.422-9.529-2.987-14.080-4.693-5.262-1.991-10.524-3.982-15.644-6.116-1.991-0.853-3.84-1.707-5.689-2.56-0.569 0.569-1.138 1.138-1.707 1.707-0.711 0.711-1.422 1.28-2.133 1.991 1.422 0.996 2.844 2.133 4.409 3.271 4.409 2.702 9.102 5.547 13.796 8.533z"></path>
<path class="path16" d="M812.089 393.529c-3.129-0.996-6.258-2.276-9.387-3.129-11.093-3.271-21.902-5.547-32.427-7.396-3.413-0.569-6.827-1.138-10.24-1.422-3.413-0.427-6.827-0.853-10.24-1.138-6.258-0.569-12.516-0.853-18.489-0.853-34.276 0-64.711 7.822-91.307 19.769-4.978 2.276-9.813 4.693-14.649 7.253-5.831 2.987-11.378 6.258-16.64 9.529-4.551 2.844-8.96 5.689-13.227 8.676-0.996 0.711-1.991 1.422-2.844 2.133 0.427 0.427 0.996 0.853 1.422 1.28 0.853 0.853 1.707 1.707 2.56 2.56 0.427 0.427 0.853 0.853 1.138 1.28 1.849 1.991 3.698 3.982 5.404 6.116 0.427 0.427 0.711 0.996 0.996 1.422 1.707 2.133 3.413 4.267 4.978 6.542 0.427 0.569 0.711 0.996 0.996 1.564 7.538 11.52 13.227 24.462 16.356 38.4 0.427 2.276 0.853 4.551 1.28 6.684 0.142 0.853 0.284 1.849 0.427 2.844 0.284 2.418 0.569 4.693 0.853 7.111 0.142 0.853 0.142 1.849 0.142 2.702 0.142 1.849 0.284 3.698 0.284 5.547 0 0.284 0 0.711 0 0.996 0 0.142 0 0.284 0 0.569 0 1.991-0.142 3.84-0.284 5.689 0 0.853 0 1.707-0.142 2.56-0.142 2.418-0.427 4.978-0.853 7.396-0.142 0.853-0.284 1.707-0.427 2.56-0.427 2.276-0.711 4.693-1.28 6.969-3.129 14.080-8.818 27.164-16.498 38.827-0.569 0.853-1.138 1.707-1.707 2.56 1.707 0.711 3.556 1.422 5.262 2.276 27.022 11.093 58.311 19.2 92.729 19.2 4.267 0 8.391-0.142 12.8-0.427 3.84-0.284 7.68-0.569 11.662-0.996 3.698-0.427 7.253-0.996 10.951-1.564 10.24-1.707 20.622-3.982 31.147-7.253 3.271-0.996 6.542-2.276 9.813-3.413s6.542-2.418 9.813-3.698c33.28-13.511 68.124-36.124 103.396-70.684 2.133-2.133 4.409-4.409 6.542-6.542 2.276-2.418 4.693-4.693 6.969-7.253 9.529-10.24 19.2-21.191 28.871-33.28-41.813-33.422-80.782-55.324-116.907-68.693-2.844-1.28-6.116-2.276-9.244-3.271z"></path>
<path class="path17" d="M413.724 513.849c0 2.702 0.569 5.404 0.853 7.964 0 0.569 0.142 1.138 0.142 1.707 0.284 2.702 0.284 5.404 0.853 7.964 0.142 0.569 0.427 1.138 0.569 1.849 2.133 9.529 5.831 18.489 10.524 26.738 1.707 2.987 3.271 5.973 5.262 8.676 0.427 0.569 0.853 0.996 1.28 1.564 1.564 2.133 3.271 4.124 4.978 6.116 0.284 0.284 0.569 0.569 0.711 0.853s0.284 0.427 0.569 0.711c2.133 2.276 4.267 4.409 6.542 6.542 0.142 0.142 0.427 0.427 0.569 0.569 0.284 0.284 0.569 0.427 0.853 0.711 1.991 1.707 3.982 3.556 6.116 5.12 0.569 0.427 0.996 0.996 1.564 1.28 2.702 1.991 5.689 3.556 8.676 5.262 6.684 3.698 13.796 6.684 21.333 8.818 3.129 0.853 6.4 1.849 9.671 2.418 0.996 0.142 2.133 0.142 3.271 0.284 2.276 0.284 4.409 0.569 6.684 0.711 0.142 0 0.142 0 0.142 0 0.996 0.142 1.991 0.284 2.987 0.284 1.28 0 2.418 0.427 3.698 0.427 1.138 0 2.276-0.284 3.413-0.427 0.996 0 1.991-0.284 2.987-0.284 0.142 0 0.142 0 0.284 0 2.276-0.142 4.551-0.427 6.684-0.711 0.996-0.142 2.133-0.142 3.271-0.284 3.271-0.569 6.542-1.422 9.671-2.276 7.68-2.133 14.791-5.12 21.476-8.818 2.987-1.707 5.973-3.271 8.818-5.262 0.569-0.427 0.996-0.853 1.564-1.28 2.133-1.564 4.267-3.413 6.258-5.12 0.284-0.284 0.569-0.427 0.853-0.711 0.284-0.142 0.427-0.284 0.569-0.569 2.276-1.991 4.551-4.267 6.542-6.542 0.142-0.142 0.427-0.427 0.569-0.569 0.284-0.284 0.569-0.711 0.853-0.996 1.707-1.991 3.413-3.84 4.978-5.973 0.427-0.569 0.853-0.996 1.28-1.564 1.991-2.702 3.698-5.689 5.262-8.676 4.836-8.391 8.676-17.493 10.809-27.307 0.142-0.569 0.284-1.138 0.427-1.564 0.569-2.702 0.427-5.404 0.853-8.107 0-0.427 0.142-0.996 0.142-1.564 0.284-2.702 0.853-5.404 0.853-8.249 0-0.427 0.142-0.853 0.142-1.422 0 0 0-0.142 0-0.142s0 0 0 0c0-0.569-0.142-1.138-0.142-1.707 0-2.702-0.569-5.404-0.853-7.964 0-0.569-0.142-1.138-0.142-1.707-0.284-2.702-0.284-5.404-0.853-7.964-0.142-0.569-0.427-1.138-0.569-1.849-2.133-9.529-5.973-18.489-10.524-26.738-1.707-2.987-3.271-5.973-5.262-8.676-0.142-0.142-0.427-0.427-0.569-0.569-1.849-2.418-3.84-4.693-5.831-6.969-0.142-0.142-0.427-0.427-0.569-0.711-0.142-0.142-0.142-0.142-0.284-0.284-2.133-2.418-4.409-4.693-6.827-6.827-0.142-0.142-0.284-0.142-0.284-0.284-0.284-0.142-0.427-0.427-0.711-0.569-2.276-1.991-4.551-3.982-6.969-5.689-0.284-0.142-0.569-0.427-0.853-0.711-2.702-1.991-5.689-3.556-8.533-5.262-6.969-3.982-14.364-7.111-22.187-9.387-3.129-0.853-6.4-1.849-9.671-2.418-0.996-0.142-2.133-0.142-3.271-0.284-2.276-0.284-4.409-0.569-6.684-0.711-0.142 0-0.142 0-0.284 0-0.996-0.142-1.991-0.284-2.987-0.284-1.28 0-2.418-0.427-3.698-0.427-1.138 0-2.276 0.284-3.413 0.284-0.996 0-1.991 0.284-2.987 0.284-0.142 0-0.142 0-0.284 0-2.276 0.142-4.551 0.427-6.684 0.711-0.996 0.142-2.133 0.142-3.129 0.284-3.271 0.569-6.542 1.422-9.671 2.276-7.964 2.276-15.502 5.404-22.471 9.387-2.987 1.707-5.831 3.271-8.533 5.262-0.284 0.142-0.427 0.427-0.711 0.569-2.418 1.849-4.693 3.84-6.969 5.831-0.142 0.142-0.427 0.284-0.711 0.569-0.142 0.142-0.142 0.142-0.284 0.284-2.418 2.133-4.693 4.409-6.827 6.827-0.142 0.142-0.142 0.142-0.284 0.284-0.142 0.284-0.427 0.569-0.569 0.711-1.991 2.276-3.982 4.551-5.689 6.969-0.142 0.284-0.427 0.427-0.569 0.711-1.991 2.702-3.556 5.689-5.262 8.676-4.836 8.533-8.676 17.493-10.809 27.307-0.142 0.569-0.284 1.138-0.427 1.564-0.569 2.702-0.569 5.404-0.853 8.107 0 0.569-0.142 0.996-0.142 1.564-0.284 2.702-0.853 5.404-0.853 8.249 0 0.427-0.142 0.853-0.142 1.422 0 0 0 0.142 0 0.142s0 0 0 0c0.284 0.427 0.427 0.996 0.427 1.564z"></path>
</symbol>
<symbol id="iconlogo" viewBox="0 0 3098 1024">
<title>iconlogo</title>
<path fill="#004d85" class="path1 fill-color2" d="M924.903 99.097v825.806h-825.806v-825.806h825.806zM990.968 33.032h-957.935v957.935h957.935v-957.935z"></path>
<path fill="#004d85" class="path2 fill-color2" d="M498.787 204.8h-109.006v564.852h317.11v-89.187h-204.8v-475.665z"></path>
<path fill="#004d85" class="path3 fill-color2" d="M1073.548 33.032h957.935v957.935h-957.935v-957.935z"></path>
<path fill="#00355b" class="path4 fill-color1" d="M1615.277 670.555c-9.91 13.213-29.729 19.819-52.852 19.819-29.729 0-52.852-6.606-66.065-23.123s-23.123-39.639-23.123-75.974h-105.703v3.303c0 59.458 16.516 102.4 56.155 135.432 39.639 29.729 85.884 46.245 138.735 46.245s95.794-13.213 128.826-42.942c33.032-26.426 49.548-66.065 49.548-115.613 0-46.245-13.213-85.884-39.639-112.31s-66.065-49.548-122.219-69.368c-33.032-13.213-56.155-29.729-72.671-39.639-13.213-13.213-19.819-26.426-19.819-46.245s6.606-36.335 19.819-49.548c13.213-13.213 29.729-19.819 52.852-19.819s42.942 6.606 56.155 23.123c13.213 16.516 19.819 36.335 19.819 62.761h105.703v-3.303c0-49.548-16.516-89.187-49.548-122.219s-75.974-49.548-132.129-49.548c-52.852 0-95.794 13.213-128.826 42.942s-49.548 66.065-49.548 112.31c0 46.245 13.213 82.581 42.942 109.006s72.671 49.548 132.129 69.368c29.729 13.213 49.548 26.426 59.458 36.335 9.91 13.213 16.516 29.729 16.516 52.852 0 29.729-6.606 46.245-16.516 56.155z"></path>
<path fill="#004d85" class="path5 fill-color2" d="M3005.935 99.097v825.806h-825.806v-825.806h825.806zM3072 33.032h-957.935v957.935h957.935v-957.935z"></path>
<path fill="#004d85" class="path6 fill-color2" d="M2784.619 475.665h-184.981v75.974h75.974v102.4c-6.606 13.213-16.516 19.819-26.426 26.426s-26.426 9.91-49.548 9.91c-29.729 0-52.852-9.91-66.065-29.729-16.516-19.819-23.123-49.548-23.123-89.187v-171.768c0-39.639 6.606-66.065 23.123-85.884s36.335-29.729 62.761-29.729c26.426 0 46.245 6.606 59.458 23.123 13.213 13.213 19.819 36.335 19.819 69.368h102.4v-3.303c0-56.155-16.516-99.097-49.548-132.129-29.729-29.729-79.277-46.245-142.039-46.245-59.458 0-105.703 16.516-138.735 52.852-36.335 36.335-52.852 85.884-52.852 151.948v171.768c0 66.065 19.819 115.613 56.155 151.948s85.884 52.852 145.342 52.852c46.245 0 85.884-9.91 115.613-26.426 29.729-19.819 52.852-39.639 69.368-62.761v-211.406z"></path>
</symbol>
</defs>
</svg>
	<header class="hero-header">
		<div class="video-wrapper">
			<video id="hero_header"  loop muted preload="auto">
				<source src="ow_head.mp4" type="video/mp4">
				<source src="ow_head.webm" type="video/webm">
			</video>
			<div class="overlay">

			</div>
		</div>
		<div class="text-wrapper">
			<h1 class="header__title">
				Яхтенная школа
			</h1><br>
			<h3 class="header__subtitle">
				HUNGRY MONKEY
			</h3><br>
			<span class="header__description">
				Готовы к новым впечатлениям?!
			</span><br>
			<button class="header__button" id="header_button">
				Полный вперёд
			</button>
		</div>
		<div class="main-menu">
			<nav>
				<!-- <a href="whatyouget_scr">Что вы получите</a><br> -->
				<a href="program_scr">Программа</a>
				<a href="whymonkey_scr">Почему Hungry Monkey </a>
				<!-- <a href="feedbacks_scr">Видеоотзывы</a> -->
				<a href="form_scr">Присоединиться</a>
			</nav>
			<div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
				<svg fill="#00355b" xmlns="https://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
					<path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
				</svg>
			</div>
			<div class="main-menu__contacts">
				<span class="contacts__text">
					г. Киев, Коперника 12Д
				</span><br>
				<span class="contacts__text">
					(067) 506-90-00
				</span>
				<div class="contacts__container">
					<a href="https://www.youtube.com/channel/UClqJwHqbQAyFlIjqhwt0Jyg" class="social__item" target="_blank">
						<svg class="icon icon-youtube"><use xlink:href="#icon-youtube"></use></svg>
					</a>
					<a href="https://www.facebook.com/hmlsproject" class="social__item" target="_blank">
						<svg class="icon icon-fb"><use xlink:href="#icon-fb"></use></svg>
					</a>
					<a href="https://www.instagram.com/hungry.monkey/" class="social__item" target="_blank">
						<svg class="icon icon-inst"><use xlink:href="#icon-inst"></use></svg>
					</a>
				</div>
			</div>
		</div>
		<div class="buttons__container">
			<button class="callback__button">
				<i class="callback__icon">
					<svg class="icon icon-form_icon1"><use xlink:href="#icon-form_icon2"></use></svg>
				</i>
			</button>
			<button class="menu__buton">
				<svg viewBox="0 0 800 600">
			    <path d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200" id="top"></path>
			    <path d="M300,320 L540,320" id="middle"></path>
			    <path d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190" id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path>
			  </svg>
			</button>
		</div>
	</header>
	<section class="doyoutry">
		<div class="pp">
			<img src="pp.jpg" alt="">
		</div>
		<h2 class="doyoutry__header">
			Вы пробовали?
		</h2>
		<div class="doyoutry__container">
			<div class="container__item">
				<div class="item__wrapper">
					<span class="item__header">
						Прокатиться
					</span><br>
					<span class="item__text">
						на водных лыжах <br>позади яхты?
					</span>
				</div>
			</div>
			<div class="container__item">
				<div class="item__wrapper">
					<span class="item__header">
						Прыгнуть
					</span><br>
					<span class="item__text">
						в чистую <br>лазурную воду?
					</span>
				</div>
			</div>
			<div class="container__item">
				<div class="item__wrapper">
					<span class="item__header">
						Словить
					</span><br>
					<span class="item__text">
						огромную рыбину <br>в Тихом океане?
					</span>
				</div>
			</div>
		</div>
	</section>
	<section class="videoscreen">
		<span class="videoscreen__question">
			Все еще нет?
		</span><br>
		<p class="videoscreen__text">
			Хватит размениваться на&nbsp;скучные увлечения и&nbsp;однообразный&nbsp;отдых<br>
			Забудьте о рутине, переверните&nbsp;свой&nbsp;мир
		</p><br>
		<h2 class="videoscreen__title">
			Откройте для&nbsp;себя яхтинг!
		</h2><br>
		<button class="videoscreen__button" id="video-section_button">
			<span></span>
		</button>

  		<div class="site-wrapper">
        <div class="videopopup__container">
					<video src="/HM_Video.mp4" width="600px" height="auto" preload="auto" controlsList="nodownload" controls></video>
          <!--<iframe  src="https://www.youtube.com/embed/zYUEm2itVng"  allowfullscreen></iframe>-->
        </div>
      </div>

	</section>
	<section class="teach clear-fix">
		<div class="teach__col">
			<div class="col__wrapper">
				<p class="col__text">
					<i class="text__icon">
						<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					</i>
					Мы научим Вас, как противостоять<br> бушующей стихии
				</p>
				<p class="col__text">
					<i class="text__icon">
						<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					</i>
					Как покорить ветер и волны<br>
					и стать настоящим морским волком
				</p>
				<p class="col__text">
					<i class="text__icon">
						<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					</i>
					Мы научим Вас управлять мечтой
				</p>
			</div>
		</div>
		<div class="teach__col">
			<div class="col__wrapper">
				<h2 class="col__text">
					<i class="text__icon">
						<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					</i>
					Яхтинг – это яркие эмоции <br>
					и впечатления
				</h2>
				<p class="col__text">
					<i class="text__icon">
						<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					</i>
					Это мурашки по коже и настоящая<br> свобода
				</p>
				<p class="col__text">
					<i class="text__icon">
						<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					</i>
					Это то, что вытащит вас из четырех стен<br> и заставит жить по-настоящему
				</p>
			</div>
		</div>
		<button class="teach__button" id="teach_button">
			полный вперёд
		</button>
	</section>
	<section class="choice clear-fix">
		<div class="choice__col">
			<img src="img/choice1.jpg" alt="" class="col__picture">
			<div class="col__overlay">
				<span class="overlay__header">
					Покорите море и&nbsp;ветер
				</span>
				<h2 class="overlay__text">
					Получив права шкипера, Вы сможете арендовать яхту в любой точке планеты и увидите множество новых восхитительных мест
				</h2>
			</div>
		</div>
		<div class="choice__col">
			<img src="img/choice2.jpg" alt="" class="col__picture">
			<div class="col__overlay">
				<span class="overlay__header">
					Выберите свой рай
				</span>
				<p class="overlay__text">
					Права шкипера позволят в качестве капитана участвовать и побеждать в лучших яхтенных регатах
				</p>
			</div>
		</div>
		<div class="choice__col">
			<img src="img/choice3.jpg" alt="" class="col__picture">
			<div class="col__overlay">
				<span class="overlay__header">
					Станьте тем, <br>кем всегда хотели быть
				</span>
				<p class="overlay__text">
					Вы сможете путешествовать по всему миру, изучая языки, традиции и национальную кухню
				</p>
			</div>
		</div>
	</section>
	<section class="doubts" id="program_scr">
		<img src="img/doubts_right.jpg" alt="" class="doubts__overlay">
		<div class="site-wrapper">
			<span class="doubts__subtitle">
				Наш курс – 10&nbsp;идеально продуманных теоретических занятий.
			</span>
			<p class="doubts__description">
				Вы изучите всё самое важное, от устройства яхты до&nbsp;навигации и работы с&nbsp;парусами,<br>
				и сможете уверенно встать за&nbsp;штурвал.
			</p>
			<h2 class="doubts__title">
				Программа курса <span></span>
			</h2><br>
			<div class="doubts__container">
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Вводное занятие и основы теории паруса
				</h3>
				<ul class="container__list">
					<li>
						Вводное занятие: о морской практике и сертификатах.
					</li>
					<li>
						Устройство парусной яхты (кокпит, палуба, каюты).
					</li>
					<li>
						Основные элементы устройства парусных судов, основы гидродинамики парусного судна.
					</li>
					<li>
						Парусное вооружение яхт (кэт, шлюп, тендер, кеч, иол, шхуна).
					</li>
					<li>
						Основы теории паруса.
					</li>
					<li>
						Управление парусным судном: Курсы к ветру, вымпельный ветер.
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Основы хождения под парусом, элементы управления яхтой под двигателем
				</h3>
				<ul class="container__list">
					<li>
						Центр парусности, центр бокового сопротивления, остойчивость яхты, сопротивление воды движению яхты, волновое сопротивление.
					</li>
					<li>
						Настройка парусов на различных курсах к ветру.
					</li>
					<li>
						Повороты оверштаг и фордевинд.
					</li>
					<li>
						Основные морские узлы.
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Управление яхтой под двигателем
				</h3>
				<ul class="container__list">
					<li>
						Управление яхтой под двигателем: эффект винта, изменение направления движения.
					</li>
					<li>
						Отход и швартовка в различных условиях (наиболее характерные случаи).
					</li>
					<li>
						Якорная стоянка: Основные разновидности якорей их особенности. Грунты – что как держит.
					</li>
					<li>
						Способы постановки на якорь (завод дополнительных швартовых на берег). Тол-буй, проблемы при подъеме якоря.
					</li>
					<li>
						Подход–отход от бочки.
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Безопасность. Основы чартера. Яхтенный этикет
				</h3>
				<ul class="container__list">
					<li>
						Безопасность на борту. Аварийно-спасательные средства и устройства.
					</li>
					<li>
						Организация команды на борту, несение вахт, бортовой журнал.
					</li>
					<li>
						Организация чартера, чеклист, сдача-прием яхты, яхтенный этикет.
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Морская радиосвязь
				</h3>
				<ul class="container__list">
					<li>
						Радиосвязь.
					</li>
					<li>
						Морской английский.
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Правила судовождения
				</h3>
				<ul class="container__list">
					<li>
						Международные правила предупреждения столкновений судов в море (МППСС72).
					</li>
					<li>
						Огни и знаки судов.
					</li>
					<li>
						В общем о местных правилах.
					</li>
					<li>
						Правила пересечения границ.
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Гидрометеорология.
				</h3>
				<ul class="container__list">
					<li>
						Гидрометеорология и океанография.
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Навигация.
				</h3>
				<ul class="container__list">
					<li>
						Навигация (начало, теоретическая часть).
					</li>
					<li>
						Навигация (Практические занятия с морскими картами)
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Лоция.
				</h3>
				<ul class="container__list">
					<li>
						Основные определения.
					</li>
					<li>
						Латеральная и кардинальная системы, другие знаки, створы.
					</li>
					<li>
						Маяки.
					</li>
					<li>
						«Пайлот бук», «Огни и знаки», «Извещения мореплавателям».
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Навигационные программы и системы.
				</h3>
				<ul class="container__list">
					<li>
						Современные технические и программные средства навигации. Навигационные приборы и программы.
					</li>
				</ul>
				<h3 class="container__title">
					<svg class="icon icon-wheel"><use xlink:href="#icon-wheel"></use></svg>
					Заключительное занятие. Тесты
				</h3>
			</div>
			<div class="button__wrapper">
				<button class="doubts__button" id="doubts_button">
					полный вперёд
				</button>
			</div>
		</div>
	</section>
	<section class="icons-sections" id="whatyouget_scr">
		<div class="site-wrapper">
			<h2 class="icons-sections__title">
				После прохождения полного курса&nbsp;теории <br>и морской практики вы&nbsp;получите:
			</h2>
			<div class="icons-sections__container">
				<div class="container__item">
					<div class="item_wrapper">
						<figure class="item__header">
							<div class="header__circle"></div>
							<i class="header__icon"></i>
						</figure>
						<span class="item__text">
							Международные<br> права шкипера  IYT<br> «International Bareboat<br> Skipper»

						</span>
					</div>
				</div>
				<div class="container__item">
					<div class="item_wrapper">
						<figure class="item__header">
							<div class="header__circle"></div>
							<i class="header__icon"></i>
						</figure>
						<span class="item__text">
							Сертификат<br> радиооператора VHF <br> Radio Operator
						</span>
					</div>
				</div>
				<div class="container__item">
					<div class="item_wrapper">
						<figure class="item__header">
							<div class="header__circle"></div>
							<i class="header__icon"></i>
						</figure>
						<span class="item__text">
							International Sailor <br> Logbook для записей о <br>пройденных милях
						</span>
					</div>
				</div>
				<div class="container__item">
					<div class="item_wrapper">
						<figure class="item__header">
							<div class="header__circle"></div>
							<i class="header__icon"></i>
						</figure>
						<span class="item__text">
							SSD-диск с обучающими<br> материалами<br> и литературой
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="whatyouget">
		<div class="site-wrapper">
			<div class="whatyouget__row clear-fix">
				<div class="whatyouget__col">
					<div class="col__wrapper wrapper_left">
						<h2 class="col__header">
							Учитесь <br>где угодно:
						</h2>
						<p class="col__text">
							обучайтесь дистанционно<br> с любой точки планеты
						</p>
						<i class="col__icon">
							<svg class="icon icon-obtain1"><use xlink:href="#icon-obtain3"></use></svg>
						</i>
					</div>
				</div>
				<div class="whatyouget__col">
					<div class="col__wrapper wrapper_right">
						<span class="col__header">
							Быстрый <br>курс:

						</span>
						<p class="col__text">
							освойте всю теорию <br>за 2 месяца
						</p>
						<i class="col__icon">
							<svg class="icon icon-obtain1"><use xlink:href="#icon-obtain1"></use></svg>
						</i>
					</div>
				</div>
			</div>
			<div class="whatyouget__row clear-fix">
				<div class="whatyouget__col">
					<div class="col__wrapper wrapper_left">
						<span class="col__header">
							Всё <br>рядом:
						</span>
						<p class="col__text">
							после теоретического курса<br> отправляйтесь на практику<br> со своим инструктором
						</p>
						<i class="col__icon">
							<svg class="icon icon-obtain1"><use xlink:href="#icon-obtain2"></use></svg>
						</i>
					</div>
				</div>
				<div class="whatyouget__col">
					<div class="col__wrapper wrapper_right">
						<span class="col__header">
							Качественные <br>знания:
						</span>
						<p class="col__text">
							учитесь с лучшими <br>инструкторами Украины
						</p>
						<i class="col__icon">
							<svg class="icon icon-obtain1"><use xlink:href="#icon-obtain4"></use></svg>
						</i>
					</div>
				</div>
			</div>
			<div class="whatyouget__row clear-fix">
				<div class="whatyouget__col">
					<div class="col__wrapper wrapper_midle">
						<span class="col__header">
							Всегда под рукой:
						</span>
						<p class="col__text">
							улучшайте свои знания в любой момент<br> благодаря SSD-диску с обучающими<br> материалами и литературой
						</p>
						<i class="col__icon">
							<svg class="icon icon-obtain1"><use xlink:href="#icon-obtain5"></use></svg>
						</i>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="adventure">
		<h2 class="adventure__title">
			Выберите приключение
		</h2>
        <div class="loop owl-carousel owl-theme owl-center owl-loaded">
            <div class="item">
                <img src="img/_owl1.jpg" alt="" class="item__img">
                <h4 class="item__title">
                    Америка<br>
                    <span class="item_subtitle">
                        Проголосовало
                        <?php

                        	include 'id.php';

                        ?>
                        <span data-id="id1" class="overlay__like"><?=$id1?></span>
                    </span>
                </h4>
                <div class="item__overlay" id="databutton1">
                    <button class="overlay__button">

                    </button><br>
                    <span class="overlay__text">
                        Выбрать
                    </span><br>
                </div>
            </div>
            <div class="item">
                <img src="img/_owl2.jpg" alt="" class="item__img">
                <h4 class="item__title">
                    Средиземное море<br>
                    <span class="item_subtitle">
                        Проголосовало

                        <span data-id="id2" class="overlay__like"><?=$id2?></span>
                    </span>
                </h4>
                <div class="item__overlay" id="databutton2">
                    <button class="overlay__button">

                    </button><br>
                    <span class="overlay__text">
                        Выбрать
                    </span>
                </div>
            </div>
            <div class="item active center">
                <img src="img/_owl3.jpg" alt="" class="item__img">
                <h4 class="item__title">
                    Карибы<br>
                    <span class="item_subtitle">
                        Проголосовало
                        <span data-id="id3" class="overlay__like"><?=$id3?></span>
                    </span>
                </h4>
                <div class="item__overlay" id="databutton3">
                    <button class="overlay__button">

                    </button><br>
                    <span class="overlay__text">
                        Выбрать
                    </span>
                </div>
            </div>
            <div class="item">
                <img src="img/_owl4.jpg" alt="" class="item__img">
                <h4 class="item__title">
                    Индийский океан<br>
                    <span class="item_subtitle">
                        Проголосовало
                        <span data-id="id4" class="overlay__like"><?=$id4?></span>
                    </span>
                </h4>
                <div class="item__overlay" id="databutton4">
                    <button class="overlay__button">

                    </button><br>
                    <span class="overlay__text">
                        Выбрать
                    </span>
                </div>
            </div>
            <div class="item">
                <img src="img/_owl5.jpg" alt="" class="item__img">
                <h4 class="item__title">Австралия<br>
                    <span class="item_subtitle">
                        Проголосовало

                        <span data-id="id5" class="overlay__like"><?=$id5?></span>
                    </span>
                </h4>
                <div class="item__overlay" id="databutton5">
                    <button class="overlay__button">

                    </button><br>
                    <span class="overlay__text">
                        Выбрать
                    </span>
                </div>
            </div>
            <div class="item">
                <img src="img/_owl6.jpg" alt="" class="item__img">
                <h4 class="item__title">
                    Хорватия<br>
                    <span class="item_subtitle">
                        Проголосовало

                        <span data-id="id6" class="overlay__like"><?=$id6?></span>
                    </span>
                </h4>
                <div class="item__overlay" id="databutton6">
                    <button class="overlay__button">

                    </button><br>
                    <span class="overlay__text">
                        Выбрать
                    </span>
                </div>
            </div>
            <div class="item">
                <img src="img/_owl7.jpg" alt="" class="item__img">
                <h4 class="item__title">
                    Греция<br>
                    <span class="item_subtitle">
                        Проголосовало

                        <span data-id="id7" class="overlay__like"><?=$id7?></span>
                    </span>
                </h4>
                <div class="item__overlay" id="databutton7">
                    <button class="overlay__button">

                    </button><br>
                    <span class="overlay__text">
                        Выбрать
                    </span>
                </div>
            </div>
        </div>
    </section>
    <section class="whymonkey" id="whymonkey_scr">
    	<h2 class="whymonkey__title">
    		Почему Hungry Monkey
    	</h2>
    	<div class="whymonkey__container clear-fix">
    		<div class="container__item">
    			<div class="item__wrapper">
	    			<span class="item__header">
	    				Концентрат нужных знаний
	    			</span>
	    			<p class="item__text">
	    				Мы постоянно совершенствуем и оттачиваем нашу теоретическую программу. Обучаясь с нами, вы получаете тщательно продуманную и сжатую выдержку знаний, необходимых для управления яхтой. Ни грамма лишней информации!
	    			</p>
	    			<img src="img/whymonkey1.jpg" alt="" class="item__picture">
	    		</div>
    		</div>
    		<div class="container__item">
    			<div class="item__wrapper">
	    			<img src="img/whymonkey2.jpg" alt="" class="item__picture">
	    			<span class="item__header">
	    				Демократичная цена
	    			</span>
	    			<p class="item__text">
	    				Мы не просто яхтенная школа – мы сообщество людей, которые живут и “дышат” яхтингом. Мы не продаем
						курсы — мы делимся с людьми своими открытиями и восторгом. Именно поэтому стоимость обучения в Hungry Monkey так лояльна.
	    			</p>
	    		</div>
    		</div>
    		<div class="container__item">
    			<div class="item__wrapper">
	    			<span class="item__header">
	    				Интерактивные занятия
	    			</span>
	    			<p class="item__text">
	    				Мы тщательно продумываем сценарий каждого занятия и стараемся сделать его как можно более интересным. На наших лекциях не засыпают! Наши занятия проходят в удобное для вас время – в будние дни с 18:00 до 21:00. Ваши выходные остаются выходными!
	    			</p>
	    			<img src="img/whymonkey3.jpg" alt="" class="item__picture">
	    		</div>
    		</div>
    		<div class="container__item">
    			<div class="item__wrapper">
	    			<img src="img/whymonkey4.jpg" alt="" class="item__picture">
	    			<span class="item__header">
	    				Все рядом
	    			</span>
	    			<p class="item__text">
	    				Мы – первая школа в Украине, в которой можно пройти яхтенную практику в Киеве! Никаких расходов на перелет и чартер яхты, никакого языкового барьера и лишней траты времени – станьте настоящим яхтсменом, не выезжая из Киева!
	    			</p>
	    		</div>
    		</div>
    	</div>
    </section>
    <section class="insctructors" id="insctructors_scr">
    	<div class="site-wrapper">
    		<h2 class="insctructors__title">
    			Наши инструкторы
    		</h2>
    		<div class="owl-instructors">
			    <div class="item left_pic">
			    	<div class="item__body clear-fix">
			    		<img src="" alt="" class="body__picture">
			    		<p class="body__text">

			    		</p>
			    	</div>
			    	<div class="item__foter">
			    		<span class="footer__title">

			    		</span>
			    		<p class="footer__subtitle">

			    		</p>
			    	</div>
			    </div>
			    <div class="item right_pic">
			    	<div class="item__body clear-fix">
			    		<img src="" alt="" class="body__picture">
			    		<p class="body__text">

			    		</p>
			    	</div>
			    	<div class="item__foter">
			    		<span class="footer__title">

			    		</span>
			    		<p class="footer__subtitle">

			    		</p>
			    	</div>
			    </div>
			    <div class="item left_pic">
			    	<div class="item__body clear-fix">
			    		<img src="" alt="" class="body__picture">
			    		<p class="body__text">

			    		</p>
			    	</div>
			    	<div class="item__foter">
			    		<span class="footer__title">

			    		</span>
			    		<p class="footer__subtitle">

			    		</p>
			    	</div>
			    	<!-- <div class="item__body clear-fix">
			    		<img src="" alt="" class="body__picture">
			    		<p class="body__text">

			    		</p>
			    	</div>
			    	<div class="item__foter">
			    		<span class="footer__title">

			    		</span>
			    		<p class="footer__subtitle">

			    		</p>
			    	</div> -->
			    </div>
			    <div class="item right_pic">
			    	<!-- <div class="item__body clear-fix">
			    		<img src="" alt="" class="body__picture">
			    		<p class="body__text">

			    		</p>
			    	</div>
			    	<div class="item__foter">
			    		<span class="footer__title">

			    		</span>
			    		<p class="footer__subtitle">

			    		</p>
			    	</div> -->
			    	<div class="item__body clear-fix">
			    		<img src="" alt="" class="body__picture">
			    		<p class="body__text">

			    		</p>
			    	</div>
			    	<div class="item__foter">
			    		<span class="footer__title">

			    		</span>
			    		<p class="footer__subtitle">

			    		</p>
			    	</div>
			    </div>
			    <div class="item left_pic">
			    	<div class="item__body clear-fix">
			    		<img src="" alt="" class="body__picture">
			    		<p class="body__text">

			    		</p>
			    	</div>
			    	<div class="item__foter">
			    		<span class="footer__title">

			    		</span>
			    		<p class="footer__subtitle">

			    		</p>
			    	</div>

			    </div>
			    <div class="item right_pic">
            <div class="item__body clear-fix">
              <img src="" alt="" class="body__picture">
              <p class="body__text">

              </p>
            </div>
            <div class="item__foter">
              <span class="footer__title">

              </span>
              <p class="footer__subtitle">

              </p>
            </div>
          </div>
			</div>
    	</div>
    </section>

    <section class="price">
    	<div class="site-wrapper">
    		<p class="price__description">
    			Считаете, что яхтинг – развлечение для миллионеров? <br>Думаете, что научиться управлять яхтой может только избранный?
    		</p>
    		<span class="price__title">Отбросьте сомнения! </span><br>
    		<h2 class="price__description">Научиться управлять яхтой не дороже, чем купить iPhone.</h2>
    		<p class="price__description">
    		Да, признаём – это сложнее, чем водить автомобиль. <br>Но есть один миф о яхтинге, который мы с радостью подтвердим  —
    		</p>
    		<span class="price__title">
    			Однажды взойдя на борт, Вы&nbsp;не&nbsp;останетесь равнодушным!
    		</span><br>
    		<button class="price__button" id="price_button">
    			Узнать цену
    		</button>
    	</div>
    </section>
    <section class="numbers">
    	<h2 class="numbers__title">
    		Hungry Monkey в&nbsp;цифрах
    	</h2>
    	<div class="numbers__container">
                <div class="container__item">
                    <div class="item__wrapper-first">
                        <span class="item__header">Обучаем <br>яхтингу c</span><br>
                        <span class="item__numer">2010</span><br>
                        <span class="item__foter">года</span>
                    </div>
                    <div class="item__wrapper-second">
                        <span class="item__header">Обучаем <br> яхтингу c</span><br>
                        <span class="item__numer">2010</span><br>
                        <span class="item__foter">года</span>
                    </div>
                </div>
                <div class="container__item">
                    <div class="item__wrapper-first">
                        <span class="item__header">Стаж <br>инструкторов</span><br>
                        <span class="item__numer">14</span><br>
                        <span class="item__foter">лет</span>
                    </div>
                    <div class="item__wrapper-second">
                        <span class="item__header">Стаж <br>инструкторов</span><br>
                        <span class="item__numer">14</span><br>
                        <span class="item__foter">лет</span>
                    </div>
                </div>
                <div class="container__item">
                    <div class="item__wrapper-second">
                        <span class="item__header">Нами <br>обучено</span><br>
                        <span class="item__numer">153</span><br>
                        <span class="item__foter">шкипера</span>
                    </div>
                    <div class="item__wrapper-first">
                        <span class="item__header">Нами <br>обучено</span><br>
                        <span class="item__numer">153</span><br>
                        <span class="item__foter">шкипера</span>
                    </div>
                </div>
                <div class="container__item">
                    <div class="item__wrapper-second">
                        <span class="item__header">С Hungry Monkey <br>работают</span><br>
                        <span class="item__numer">12</span><br>
                        <span class="item__foter">инструкторов</span>
                    </div>
                    <div class="item__wrapper-first">
                        <span class="item__header">С Hungry Monkey <br>работают</span><br>
                        <span class="item__numer">12</span><br>
                        <span class="item__foter">инструкторов</span>
                    </div>
                </div>
            </div>
    </section>
    <section class="issa">
    	<div class="site-wrapper">
    		<h2 class="issa__title">
    			Система обучения International Yacht Training (IYT)
    		</h2>
    		<div class="issa__item">
    			<span class="item__title">
    				Международная система обучения
    			</span>
    			<p class="item__text">
    				 Сертификаты IYT признаны всеми ведущими чартерными компаниями. Кроме того, среди международных систем только IYT допускает обучение шкиперов на русском языке.
    			</p>
    			<i class="item__icon">
    				<svg width="223" height="166" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 58.5 500 377.8" enable-background="new 0 58.5 500 377.8" xml:space="preserve">
					 <defs>
					 	<pattern   patternContentUnits="objectBoundingBox" id="img1" width="100%" height="100%">
					 		<image xlink:href="img/for_mask.jpg" width="1" height="1" />
					 	</pattern>
					 </defs>
				<path d="M492.1,315.3h-28.5H316.2c-0.4,0-0.8,0-1.1,0.1c-0.1,0-0.2,0-0.2,0c-0.4,0.1-0.7,0.1-1.1,0.3c0,0-0.1,0-0.1,0
					c-0.3,0.1-0.6,0.2-1,0.4c0,0-0.1,0-0.1,0.1c-0.3,0.2-0.6,0.4-0.9,0.6c-0.1,0-0.1,0.1-0.2,0.1c-0.3,0.2-0.5,0.4-0.8,0.7
					c0,0,0,0-0.1,0.1c-0.3,0.3-0.5,0.5-0.7,0.8c0,0.1-0.1,0.1-0.1,0.2c-0.2,0.3-0.4,0.6-0.6,0.9c0,0,0,0.1,0,0.1c-0.2,0.3-0.3,0.7-0.4,1
					c0,0,0,0,0,0.1c-5.2,15.5-17.7,26.7-33.7,31v-49.4h59.2c3.4,0,6.4-2.2,7.5-5.4l4.4-13.5c11.8-36.6,11.8-75.3,0-111.9l-4.4-13.5
					c-1-3.2-4.1-5.4-7.5-5.4H273V133h16.4c4.3,0,7.9-3.5,7.9-7.9c0-4.3-3.5-7.9-7.9-7.9H273V115l48.6-18.5c3.1-1.2,5.1-4.2,5.1-7.5
					c-0.1-3.3-2.2-6.2-5.4-7.3l-53.6-17.6c-2.4-0.8-5-0.4-7.1,1.1c-2,1.5-3.2,3.8-3.2,6.4v37.8c0,0.1,0,0.2,0,0.4v7.6h-14.8
					c-4.3,0-7.9,3.5-7.9,7.9c0,4.3,3.5,7.9,7.9,7.9h14.8v19.4h-57.1c-2.5,0-4.9,1.2-6.4,3.2c-1.5,2-1.9,4.6-1.1,7l4.4,13.5
					c2.1,6.5,3.8,13,5,19.6c0,0.3,0.1,0.6,0.2,0.9c5.1,27.2,3.4,55.1-5.2,81.8l-4.4,13.5c-0.8,2.4-0.4,5,1.1,7c1.5,2,3.8,3.2,6.4,3.2
					h59.2v51.3h-82.8V111.8V85c0-4.3-3.5-7.9-7.9-7.9c-4.3,0-7.9,3.5-7.9,7.9v24.7l-92.8,159c-1.4,2.4-1.4,5.4,0,7.9
					c1.4,2.4,4,3.9,6.8,3.9h86v73.1h-21.2c-10.7,0-20.1-7.2-22.8-17.6l-7.4-42.9c-0.6-3.8-3.9-6.5-7.7-6.5H85.3h-64H7.9
					c-4.3,0-7.9,3.5-7.9,7.9c0,4.3,3.5,7.9,7.9,7.9h7.6l7.9,26.3c0,0,0,0,0,0.1L54,430.7c1,3.3,4.1,5.6,7.5,5.6h335.1
					c2.7,0,5.2-1.4,6.6-3.6L468,331h24.2c4.3,0,7.9-3.5,7.9-7.9S496.5,315.3,492.1,315.3z M273,82.4l22.3,7.3L273,98.2V82.4z M212,283.3
					c8.5-26.3,10.9-53.7,7.2-80.6H311c4.3,0,7.9-3.5,7.9-7.9c0-4.3-3.5-7.9-7.9-7.9h-94.7c-1.2-5.2-2.6-10.4-4.3-15.6l-1-3.2h117.6
					l2.6,8.1c10.8,33.4,10.8,68.8,0,102.2l-2.6,8.1H211L212,283.3z M88.6,264.8l72.3-124v124H88.6z M392.3,420.6h-325l-25.9-86.5h38.1
					c4.3,0,7.9-3.5,7.9-7.9s-3.5-7.9-7.9-7.9H36.7l-4.9-16.2h53.4h9.8l6.3,36.7c0,0.2,0.1,0.4,0.1,0.6c4.4,17.5,20,29.8,38.1,29.8h120.8
					c27.5,0,50.9-14.8,61.2-38.3h127.8L392.3,420.6z  " fill="url(#img1)"/>
				</svg>
    			</i>
    		</div>
    		<div class="issa__item">
    			<span class="item__title">
    				Высший уровень требований к обучению
    			</span>
    			<p class="item__text">
    				 Количество студентов на одной яхте – не более 5 человек. Наши инструкторы уделяют достаточно времени каждому студенту.
    			</p>
    			<i class="item__icon">
    				<svg  version="1.1"  xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 130.2 500 239.6"  xml:space="preserve" width="220" height="106">
				 <defs>
				 	<pattern   patternContentUnits="objectBoundingBox" id="img2" width="100%" height="100%">
				 		<image xlink:href="img/for_mask.jpg" width="1" height="1" />
				 	</pattern>
				 </defs>
				<path  d="M488.5,221.7c-7.4-7.5-17.4-11.6-27.9-11.6h-20.1c-5.5-15.8-14.1-30.3-25.3-42.2c-3.8-4-7.8-7.7-12.1-11.1
				c-0.1-0.1-0.2-0.1-0.3-0.2c-21.3-17-48.2-26.5-76.2-26.5c-0.8,0-1.6,0-2.3,0c-17,0.3-32.8,2.9-48,8c-0.8,0.1-1.7,0.4-2.5,0.8
				c-0.1,0.1-0.3,0.2-0.4,0.2c-12.5,4.4-24.5,10.6-36.5,18.5c-18.5-18-41.2-26.8-69.2-26.8c-0.2,0-0.4,0-0.6,0
				c-25.4,0.1-49.5,8.4-69.3,22.7c-0.3,0.2-0.6,0.4-0.8,0.6c-18.8,13.8-33.6,33-41.8,55.8c-0.1,0.3-0.2,0.7-0.3,1H39.4
				C17.7,211,0,228.7,0,250.4c0,21.7,17.7,39.4,39.4,39.4H55c8.4,23.5,23.7,43.6,44.4,58.1c20.5,14.3,44.5,21.9,69.3,21.9
				c0.7,0,1.3,0,2,0c37.1-0.6,68.7-10.9,96.8-31.5c1.2-0.9,2.4-1.8,3.6-2.7c17.2,18.2,43.3,30.5,65.9,30.5c39,0,77.1-23.9,96.9-60.9
				c2.8-5.2,5.1-10.6,7.1-16.3h19.6c21.7,0,39.4-17.6,39.4-39.3C500,239.1,495.9,229.2,488.5,221.7z M101.4,170.7l16.9,16.9
				c1.5,1.5,3.6,2.3,5.6,2.3c2,0,4-0.8,5.6-2.3c3.1-3.1,3.1-8.1,0-11.1l-15-15c13.5-8.1,28.7-13.1,44.8-14.5v23.5
				c0,4.4,3.5,7.9,7.9,7.9c4.4,0,7.9-3.5,7.9-7.9v-23.6c15.7,1.1,29.3,5.6,41.1,13.9l-15.6,15.6c-3.1,3.1-3.1,8.1,0,11.1
				c1.5,1.5,3.6,2.3,5.6,2.3c2,0,4-0.8,5.6-2.3l16.4-16.4c0.3,0.3,0.5,0.5,0.8,0.8l0.2,0.2c0.1,0.1,0.2,0.2,0.3,0.3
				c5.7,6.1,15.4,20.5,24.3,34.4L232.2,221c-3.6,2.4-4.6,7.3-2.2,10.9c1.5,2.3,4,3.5,6.6,3.5c1.5,0,3-0.4,4.4-1.3l21.1-14
				c3.1,4.9,5.8,9.3,7.8,12.7c-10.9,7.7-20,16.2-28.1,23.8l-0.1,0.1c-2.1,2-4.2,4-6.3,5.9c-2.3-3.9-5.4-9.1-9.1-15.2
				c-2-3.3-3.9-6.6-5.8-9.7c-16.7-28.1-29.9-50.3-53.2-50.3c-18.1,0-34.1,9.3-41.7,23.7H71.7C78.3,195.1,88.6,181.3,101.4,170.7z
				 M339.8,210.2c-20.5,0.1-39.1,4.8-56.6,14.3c-0.8-1.3-1.7-2.8-2.6-4.3c10-7.1,23.1-14.1,45.4-14.1c7.4,0,14.3,1.5,20.1,4.1H339.8z
				 M169.5,291c-3.5,0-7-0.4-10.4-1.2c25.1-0.9,43.3-11.8,60.3-23.2c1.5,2.5,2.8,4.7,3.9,6.5C209.7,283.6,194.8,290.6,169.5,291z
				 M155.4,211h-10c5.1-4.6,12.6-7.9,21.9-7.9c3.3,0,6.4,1,9.4,2.7C169.5,210.5,163.4,211,155.4,211z M39.4,274.1
				c-13,0-23.6-10.6-23.6-23.6c0-13,10.6-23.6,23.6-23.6h0.9v27.7c0,4.4,3.5,7.9,7.9,7.9c4.4,0,7.9-3.5,7.9-7.9v-27.7h35.1v27.7
				c0,4.4,3.5,7.9,7.9,7.9c4.4,0,7.9-3.5,7.9-7.9v-27.7h29.4h5.7v27.7c0,4.4,3.5,7.9,7.9,7.9c4.4,0,7.9-3.5,7.9-7.9v-27.8
				c9.9-0.2,19.4-1.6,30.5-9.9c5.8,7.4,11.8,17.4,18.6,28.9c1.4,2.4,2.9,4.8,4.4,7.3c-16.7,11.2-33,21.1-55.7,21.1h-20.7H51.7
				L39.4,274.1L39.4,274.1z M170.5,354c-0.6,0-1.1,0-1.7,0c-21.6,0-42.5-6.6-60.3-19.1c-16.4-11.5-29-27-36.5-45.1H128
				c0.8,0.8,1.6,1.5,2.4,2.3L111.5,311c-3.1,3.1-3.1,8.1,0,11.1c1.5,1.5,3.6,2.3,5.6,2.3c2,0,4-0.8,5.6-2.3l21.2-21.2
				c5.6,2.7,11.7,4.5,17.9,5.3v24.9c0,4.4,3.5,7.9,7.9,7.9c4.4,0,7.9-3.5,7.9-7.9v-24.8c9-0.6,16.8-2.1,23.6-4.1l15.4,23.8
				c1.5,2.3,4,3.6,6.6,3.6c1.5,0,3-0.4,4.3-1.3c3.7-2.4,4.7-7.2,2.3-10.9l-13.7-21.1c7.8-3.9,14.2-8.6,20.1-13.5
				c1.7-1.4,3.3-2.8,4.9-4.2l16.2,21c1.6,2,3.9,3.1,6.3,3.1c1.7,0,3.4-0.5,4.8-1.6c3.4-2.7,4.1-7.6,1.4-11.1l-17-22.1
				c8.1-7.6,17.2-16.2,27.9-23.4l17,22.2c1.6,2,3.9,3.1,6.3,3.1c1.7,0,3.4-0.5,4.8-1.6c3.5-2.6,4.1-7.6,1.5-11.1l-15.8-20.6
				c13.1-6.5,26.8-9.9,42-10.4v28.5c0,4.4,3.5,7.9,7.9,7.9c4.4,0,7.9-3.5,7.9-7.9v-28.6H363c7.4,11.5,12.8,27.5,6.5,46.3
				c-0.1,0.3-0.2,0.6-0.4,1H340c-13.3,0.1-23.8,3.7-35.1,12c-6.2,4.5-12.4,10.3-19.9,17.3c-7.7,7.2-16.4,15.4-26.8,23.1
				C232.5,344.4,204.6,353.5,170.5,354z M347.1,294.4c-5.9,1-11.8-0.6-17.1-4.3c3.1-0.7,6.3-1.1,10-1.1h18.6
				C355.2,291.8,351.3,293.7,347.1,294.4z M420,297.8c-17.1,31.9-49.7,52.5-83,52.5c-18,0-39.5-10.2-53.7-24.8c4.5-4,8.6-7.9,12.5-11.5
				c7.1-6.7,13-12.1,18.5-16.1c0.1-0.1,0.2-0.2,0.4-0.3c5.2,5.2,11.1,8.9,17.3,11V327c0,4.4,3.5,7.9,7.9,7.9s7.9-3.5,7.9-7.9v-16.8
				c0.6-0.1,1.3-0.2,2-0.3c8.1-1.3,15.5-5.2,21.5-11.1l11.1,11.1c1.5,1.5,3.6,2.3,5.6,2.3c2,0,4-0.8,5.6-2.3c3.1-3.1,3.1-8.1,0-11.1
				l-12.8-12.8c1.4-2.7,2.7-5.7,3.8-8.8c2.1-6.2,3.2-12.5,3.5-18.8h23.5c4.4,0,7.9-3.5,7.9-7.9c0-4.4-3.5-7.9-7.9-7.9h-24.7
				c-2.3-11.5-7.7-22.7-16-33.1c-9.9-12.3-26.2-19.4-44.7-19.4c-26.2,0-42.5,8.6-53.8,16.4c-7.9-12.6-17.4-27.1-24.9-37
				c8.7-5.6,17.3-10.2,26.1-13.7l12.6,25.1c1.4,2.8,4.2,4.4,7.1,4.4c1.2,0,2.4-0.3,3.5-0.8c3.9-1.9,5.5-6.7,3.5-10.6l-11.6-23.1
				c11.4-3.1,23.2-4.6,36-4.9c1.5,0,3,0,4.5,0v27.8c0,4.4,3.5,7.9,7.9,7.9c4.4,0,7.9-3.5,7.9-7.9v-26.3c14.9,2.5,29,8.2,41.4,16.5
				l-15,15.8c-3,3.2-2.9,8.1,0.3,11.1c1.5,1.4,3.5,2.2,5.4,2.2c2.1,0,4.2-0.8,5.7-2.5l16-16.9c1.7,1.6,3.4,3.2,5,5
				C433.3,210.2,440.1,260.3,420,297.8z M460.6,273.2h-15.4c2.5-12.9,2.9-26.3,1-39.6c-0.4-2.6-0.8-5.2-1.3-7.7h12.6v28.6
				c0,4.4,3.5,7.9,7.9,7.9s7.9-3.5,7.9-7.9v-24.9c1.5,0.9,2.9,2,4.2,3.3c4.5,4.5,6.9,10.4,6.9,16.8
				C484.2,262.6,473.6,273.2,460.6,273.2z" fill="url(#img2)"/>
			</svg>
    			</i>
    		</div>
    		<div class="issa__item">
    			<span class="item__title">
    				Индивидуальная ответственность инструктора
    			</span>
    			<p class="item__text">
    				Инструктор, выдавший лицензию, несет персональную ответственность за каждого студента. Фамилия инструктора и название школы будут указаны в вашем сертификате шкипера.
    			</p>
    			<i class="item__icon">
    				<svg width="217" height="166" version="1.1"  xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 59 500 381.9" enable-background="new 0 59 500 381.9" xml:space="preserve">
							 <pattern   patternContentUnits="objectBoundingBox" id="img3" width="100%" height="100%">
					 		<image xlink:href="img/for_mask.jpg" width="1" height="1" />
					 	</pattern>
					<g fill="url(#img3)">
						<path d="M424.7,228.6c-0.9-4.3-5.1-7.1-9.4-6.3l-174.9,35.1L249,209c0.5-2.6-0.4-5.3-2.3-7.1c-1.9-1.9-4.6-2.6-7.2-2.1l-20.1,4
							l-11.9-21.8c-1.7-3.1-5.2-4.7-8.6-4l-19,3.8l-2.4-12c-0.9-4.3-5.1-7.1-9.4-6.3c-4.3,0.9-7.1,5.1-6.3,9.4l2.4,12l-23.1,4.6l-2.4-12
							c-0.9-4.3-5.1-7.1-9.4-6.3c-4.3,0.9-7.1,5.1-6.3,9.4l2.4,12l-23.1,4.6l-2.4-12c-0.9-4.3-5.1-7.1-9.4-6.3c-4.3,0.9-7.1,5.1-6.3,9.4
							l2.4,12l-15,3c-2.1,0.4-3.9,1.6-5.1,3.4c-1.2,1.8-1.6,3.9-1.2,6l4.2,21l-6.1,1.2c-2.3,0.5-4.2,1.9-5.4,3.9c-1.1,2-1.4,4.4-0.6,6.6
							l16,45.3l-8.2,1.6c-4.3,0.9-7.1,5.1-6.3,9.4c0.8,3.8,4.1,6.4,7.8,6.4c0.5,0,1.1-0.1,1.6-0.2L418.4,238
							C422.7,237.2,425.5,233,424.7,228.6z M196.3,194.8l6.7,12.3L85.4,230.7l-2.6-13.1L196.3,194.8z M223.5,260.8L89.6,287.7l-13.7-38.8
							l4.9-1c0,0,0,0,0,0c0,0,0,0,0,0l150.4-30.2L223.5,260.8z"/>
						<circle cx="208.7" cy="236.8" r="8.3"/>
						<path d="M301.8,162.3c6.3-1.3,12.7,1.8,15.6,7.5c1.4,2.8,4.2,4.3,7.1,4.3c0.8,0,1.6-0.1,2.4-0.4c3.8-0.3,7-3.4,7.3-7.3
							c0.5-6.4,5.3-11.7,11.5-12.9c4.3-0.9,7.1-5.1,6.3-9.4c-0.9-4.3-5.1-7.1-9.4-6.3c-8.5,1.7-15.7,7-20.1,14.2c-6.7-4.9-15.4-7-24-5.3
							c-4.3,0.9-7.1,5.1-6.3,9.4C293.3,160.4,297.5,163.2,301.8,162.3z"/>
						<path d="M226.5,147c6.3-1.3,12.7,1.8,15.6,7.5c1.4,2.8,4.2,4.3,7.1,4.3c0.8,0,1.6-0.1,2.4-0.4c3.8-0.3,7-3.4,7.3-7.3
							c0.5-6.4,5.3-11.7,11.5-12.9c4.3-0.9,7.1-5.1,6.3-9.4c-0.9-4.3-5.1-7.1-9.4-6.3c-8.5,1.7-15.7,7-20.1,14.2c-6.8-4.9-15.4-7-24-5.3
							c-4.3,0.9-7.1,5.1-6.3,9.4C217.9,145.1,222.1,147.9,226.5,147z"/>
						<path d="M498.8,241.9c-1.1-1.8-2.9-3.1-5-3.6l-40.6-9.7L420.5,65.5c-0.4-2.1-1.6-3.9-3.4-5.1c-1.8-1.2-3.9-1.6-6-1.2L6.4,140.4
							c-2.1,0.4-3.9,1.6-5.1,3.4c-1.2,1.8-1.6,3.9-1.2,6L38,338.3c0.6,3.1,3,5.5,6,6.2c0,0,0,0,0,0l401.4,96.3c0.6,0.1,1.2,0.2,1.9,0.2
							c1.5,0,2.9-0.4,4.2-1.2c1.8-1.1,3.1-2.9,3.6-5l44.8-186.9C500.3,245.9,499.9,243.7,498.8,241.9z M17.4,154.5l389-78.1l34.7,172.7
							l-389,78.1L17.4,154.5z M441.3,423.3L83,337.4l30-6l94.5,22.7c0.6,0.2,1.3,0.2,1.9,0.2c3.6,0,6.9-2.5,7.8-6.1
							c1-4.3-1.6-8.6-5.9-9.6l-60.9-14.6l30.9-6.2l100.8,24.2c0.6,0.2,1.3,0.2,1.9,0.2c3.6,0,6.9-2.5,7.8-6.1c1-4.3-1.6-8.6-5.9-9.6
							l-67.2-16.1l99.2-19.9l74.4,17.8c0.6,0.2,1.3,0.2,1.9,0.2c3.6,0,6.9-2.5,7.8-6.1c1-4.3-1.6-8.6-5.9-9.6l-40.7-9.8l31-6.2l70.1,16.8
							c0.6,0.2,1.3,0.2,1.9,0.2c3.6,0,6.9-2.5,7.8-6.1c1-4.3-1.6-8.6-5.9-9.6l-36.5-8.8l28.5-5.7c4.3-0.9,7.1-5.1,6.3-9.4l-1.6-8.1
							l25.6,6.1L441.3,423.3z"/>
						<path d="M368.7,376.2l-124.2-29.8c-4.3-1-8.6,1.6-9.6,5.9c-1,4.3,1.6,8.6,5.9,9.6L365,391.8c0.6,0.2,1.3,0.2,1.9,0.2
							c3.6,0,6.9-2.5,7.8-6.1C375.7,381.6,373,377.2,368.7,376.2z"/>
						<path d="M439.6,363.1L321,334.7c-4.3-1-8.6,1.6-9.6,5.9c-1,4.3,1.6,8.6,5.9,9.6l118.6,28.5c0.6,0.2,1.3,0.2,1.9,0.2
							c3.6,0,6.9-2.5,7.8-6.1C446.6,368.5,443.9,364.2,439.6,363.1z"/>
						<path d="M446.5,334.7l-141-33.8c-4.3-1-8.6,1.6-9.6,5.9c-1,4.3,1.6,8.6,5.9,9.6l141,33.8c0.6,0.2,1.3,0.2,1.9,0.2
							c3.6,0,6.9-2.5,7.8-6.1C453.4,340,450.8,335.7,446.5,334.7z"/>
						<path d="M453.3,306.2l-16.9-4c-4.3-1-8.6,1.6-9.6,5.9c-1,4.3,1.6,8.6,5.9,9.6l16.9,4c0.6,0.2,1.3,0.2,1.9,0.2
							c3.6,0,6.9-2.5,7.8-6.1C460.2,311.6,457.6,307.3,453.3,306.2z"/>
					</g>
					</svg>
    			</i>
    		</div>
    		<div class="issa__item">
    			<span class="item__title">
    				Лицензии, признанные во всем мире
    			</span>
    			<p class="item__text">
    				MCA (Агентство по судоходству и береговой охране Великобритании) и USCG (Агентство Береговой охраны США). Эти государственные организации обеспечивают безопасность судоходства и высоко оценивают результаты обучения школ IYT.
    			</p>
    			<i class="item__icon">
    				<svg width="128" height="181" version="1.1"  xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="71.5 0 353.7 500" enable-background="new 71.5 0 353.7 500" xml:space="preserve">
						 <pattern   patternContentUnits="objectBoundingBox" id="img4" width="100%" height="100%">
				 		<image xlink:href="img/for_mask.jpg" width="1" height="1" />
				 	</pattern>
					<g fill="url(#img4)">
						<path d="M278.6,60.8c0-15.8-12.9-28.7-28.7-28.7c-15.8,0-28.7,12.9-28.7,28.7c0,15.8,12.9,28.7,28.7,28.7
							C265.8,89.4,278.6,76.6,278.6,60.8z M239.2,60.8c0-5.9,4.8-10.8,10.8-10.8c5.9,0,10.8,4.8,10.8,10.8c0,5.9-4.8,10.8-10.8,10.8
							C244,71.6,239.2,66.7,239.2,60.8z"/>
						<path d="M425.2,400.8l-1.8-95.9c-0.1-3.4-2.1-6.5-5.2-7.9c-3.1-1.4-6.7-1-9.4,1.1l-73.3,59c-2.8,2.3-4,6-3,9.5c1,3.5,4,6,7.6,6.4
							l16.7,1.9l-4.6,6.1c-10.1,13.5-25.5,21.3-42.6,21.4c-16.5,0-30.6-12.1-33.5-28.7c-2-11.6-3-23.4-3-35.2v-137h28.6
							c12.8,0,23.2-10.4,23.2-23.2c0-12.8-10.4-23.2-23.2-23.2h-28.6v-38c22-9.1,37.5-30.9,37.5-56.2C310.8,27.3,283.5,0,250,0
							s-60.8,27.3-60.8,60.8c0,25.3,15.5,47,37.5,56.2v38h-28.6c-12.8,0-23.2,10.4-23.2,23.2c0,12.8,10.4,23.2,23.2,23.2h28.6v137
							c0,11.8-1,23.6-3,35.2c-2.9,16.6-17,28.7-33.6,28.7c-16.9-0.1-32.4-7.9-42.5-21.4l-4.6-6.1l16.7-1.9c3.6-0.4,6.6-2.9,7.6-6.4
							c1-3.5-0.2-7.2-3-9.5l-73.3-59c-2.7-2.1-6.3-2.6-9.4-1.1c-3.1,1.4-5.1,4.5-5.2,7.9l-1.8,96.5c-0.1,3.6,2.1,7,5.4,8.4
							c3.4,1.4,7.2,0.7,9.8-1.9l12.7-12.7l11.5,22.9c13,26.1,39.3,42.8,68.4,43.5c7.3,0.2,14.4,1.7,21.2,4.4c6.8,2.7,12.9,6.6,18.3,11.5
							l21.9,20c1.7,1.6,3.9,2.3,6,2.3c2.2,0,4.3-0.8,6-2.3l21.9-20c10.8-9.9,24.8-15.5,39.5-15.9c14.3-0.4,28.2-4.6,40.2-12.3
							c12-7.7,21.8-18.5,28.2-31.2l11.5-22.9l12.7,12.7c1.7,1.7,4,2.6,6.4,2.6c4.9-0.1,8.8-4,8.8-8.9
							C425.2,401.4,425.2,401.1,425.2,400.8z M207.1,60.8c0-23.7,19.2-42.9,42.9-42.9c23.7,0,42.9,19.2,42.9,42.9
							c0,23.7-19.2,42.9-42.9,42.9C226.3,103.7,207.1,84.4,207.1,60.8z M244.6,121.3c1.8,0.2,3.6,0.3,5.4,0.3c1.8,0,3.6-0.1,5.4-0.3V155
							h-10.7V121.3z M401.1,373.9c-2-2-4.9-3-7.8-2.5c-2.8,0.5-5.3,2.3-6.6,4.8l-17,34c-4.9,9.9-12.5,18.2-21.8,24.2
							c-9.3,5.9-20.1,9.2-31.1,9.5c-18.9,0.5-37.1,7.8-51.1,20.6L250,479l-15.9-14.5c-7-6.3-14.9-11.4-23.7-14.9
							c-8.7-3.5-17.9-5.4-27.4-5.7c-22.5-0.6-42.8-13.5-52.9-33.7l-17-34c-1.3-2.6-3.7-4.4-6.6-4.8c-0.5-0.1-1-0.1-1.4-0.1
							c-2.4,0-4.6,0.9-6.3,2.6l-5.8,5.8l1-56.2l42.3,34.1l-10.8,1.2c-3.2,0.4-5.9,2.4-7.2,5.3c-1.3,2.9-0.9,6.3,1,8.9l13.9,18.6
							c13.5,18.1,34.1,28.5,56.8,28.6h0.1c25.2,0,46.7-18.3,51.1-43.5c2.2-12.6,3.3-25.5,3.3-38.3v-146c0-4.9-4-8.9-8.9-8.9h-37.5
							c-3,0-5.4-2.4-5.4-5.4s2.4-5.4,5.4-5.4h103.7c3,0,5.4,2.4,5.4,5.4s-2.4,5.4-5.4,5.4h-37.5c-4.9,0-8.9,4-8.9,8.9v146
							c0,12.8,1.1,25.7,3.3,38.3c4.4,25.2,25.9,43.5,51.1,43.5h0.1c22.6-0.1,43.3-10.5,56.8-28.6l13.8-18.6c1.9-2.6,2.3-6,1-8.9
							c-1.3-2.9-4-5-7.2-5.3l-10.8-1.2l42.3-34.1l1,56.2L401.1,373.9z"/>
					</g>
					</svg>
    			</i>
    		</div>
    	</div>
    </section>
    <section class="feedbacks" id="feedbacks_scr">
    	<div class="site-wrapper">
    		<h2 class="feedbacks__title">
    			Видеоотзывы
    		</h2>
    		<span class="feedbacks__subtitle">
    			Мы не хвастаемся, но нам приятно, что о нас говорят так:
    		</span>
    		<div class="feedbacks__container">
    			<div class="container__item">
            <iframe  src="https://www.youtube.com/embed/wd5ihwBeJl8"  allowfullscreen></iframe>
    				<div class="item__overlay">
            </div>
    			</div>
    			<div class="container__item">
    				<iframe  src="https://www.youtube.com/embed/Wv6yhJL9ukM"  allowfullscreen></iframe>
    				<div class="item__overlay">
            </div>
    			</div>
    			<div class="container__item">
    				<iframe  src="https://www.youtube.com/embed/lWp0DnWkmAw"  allowfullscreen></iframe>
    				<div class="item__overlay">
            </div>
    			</div>
    		</div>
    	</div>
    </section>
    <section class="dreams">
    	<div class="site-wrapper">
    		<span class="dreams__subtitle">
    			Мечты ближе, чем кажутся
    		</span>
    		<h2 class="dreams__title">
    			Воплотите их с Hungry Monkey!
    		</h2>
    	</div>
    </section>
	<section class="form-section" id="form_scr">
		<div class="site-wrapper clear-fix">
			<div class="form-sectiion__col timer-col">
				 <span class="time-col__subtitle">
<!--           Полная стоимость обучения
        </span><br>
        <span class="timer-col__title strikeout">
12 000 грн
        </span><br>
        <span class="time-col__subtitle">
При оплате до 30.05. 2016
        </span><br> -->
        <h2 class="timer-col__title" style="margin-bottom: 5px">Стоимость 4 500 грн</h2>
        <span>Возможность онлайн обучения.</span>
				<div class="timer-col__container" id="abtest">
<!-- 					<span class="container__header">
						До старта осталось:
					</span> -->
          <!-- <div id="tmr_first">
  					<script src="https://megatimer.ru/s/ca2e32c8b917c7aba04a39beb0fa70e7.js"></script>
          </div> -->
          <div id="tmr_second">
            <script src="https://megatimer.ru/s/8f7a2c09dbb4d96bd2e815958af95714.js"></script>
          </div>
				</div>
			</div>
			<div class="form-sectiion__col form-col clear-fix">
				<div class="form__wrapper">
					<span class="form__header">
						Регистрация
					</span>
					<form id="mian_register_form" class="main-form">
						<div class="form__container">
							<input name="name" type="text"  data-validation="required, length" data-validation-length="min2"  data-validation-error-msg="Введите, пожалуйста, Ваше имя" >
							<span class="container__label">
								Имя
							</span>
							<i class="container__icon">
								<svg class="icon icon-form_icon1"><use xlink:href="#icon-form_icon1"></use></svg>
							</i>
							<svg class="graphic " width="100%" height="42" viewBox="0 0 404 77" preserveAspectRatio="none">
							<path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
						</svg>
						</div>
						<div class="form__container">
							<input data-validation="required"  type="text" name="phone" class="rfiled user_phone" data-validation-error-msg="Введите, пожалуйста, Ваш номер телефона">
							<span class="container__label">
								Телефон
							</span>
							<i class="container__icon">
								<svg class="icon icon-form_icon1"><use xlink:href="#icon-form_icon2"></use></svg>
							</i>
							<svg class="graphic " width="100%" height="42" viewBox="0 0 404 77" preserveAspectRatio="none">
							<path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
						</svg>
						</div>
						<div class="form__container">
							<input id="mail_main-form" name="mail" type="text"  data-validation="email" data-validation-error-msg="Введите, пожалуйста, E-mail в формате yourmail@gmail.com ">
							<span class="container__label">
								E-mail
							</span>
							<i class="container__icon">
								<svg class="icon icon-form_icon1"><use xlink:href="#icon-form_icon3"></use></svg>
							</i>
							<svg class="graphic " width="100%" height="42" viewBox="0 0 404 77" preserveAspectRatio="none">
							<path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
						</svg>
						</div>
						<button class="form__button" id="main-form_button">
							<svg class="button_ok" enable-background="new 0 0 24 24"  version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" id="Done__x2014__Displayed_on_the_left_side_of_a_contextual_action_bar__x28_CAB_x29__to_allow_the_user_to_dismiss_it._1_" points="  20,6 9,17 4,12 " stroke="#fff" stroke-miterlimit="10" stroke-width="2"/></svg>
              <p>полный вперёд</p>
						</button>
						<input id="geoloc"  type="hidden"  name="city" value="">
					</form>
					<div class="form_shturval">
						<svg version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
						<path d="M481.2,231.2h-35.6c-3.8-40.2-19.8-76.8-44.2-106.3l25.2-25.3c7.3-7.4,7.3-19.3,0-26.6c-7.4-7.3-19.3-7.3-26.6,0l-25.2,25.3
							c-29.4-24.2-65.9-40.1-105.9-43.9V18.8C268.8,8.4,260.4,0,250,0c-10.4,0-18.8,8.4-18.8,18.8v35.6c-40.2,3.8-76.8,19.8-106.3,44.2
							L99.6,73.4c-7.4-7.3-19.3-7.3-26.6,0c-7.3,7.4-7.3,19.3,0,26.6l25.3,25.2c-24.2,29.4-40.1,65.9-43.9,105.9H18.8
							C8.4,231.2,0,239.6,0,250c0,10.4,8.4,18.8,18.8,18.8h35.6c3.8,40.2,19.8,76.8,44.2,106.3l-25.2,25.3c-7.3,7.4-7.3,19.3,0,26.6
							c3.7,3.7,8.5,5.5,13.3,5.5c4.8,0,9.6-1.8,13.3-5.5l25.2-25.3c29.4,24.2,65.9,40.1,106,43.9v35.6c0,10.4,8.4,18.8,18.8,18.8
							c10.4,0,18.8-8.4,18.8-18.8v-35.6c40.2-3.8,76.8-19.8,106.3-44.2l25.3,25.2c3.7,3.7,8.5,5.5,13.3,5.5c4.8,0,9.6-1.8,13.3-5.5
							c7.3-7.4,7.3-19.3,0-26.6l-25.3-25.2c24.2-29.4,40.1-65.9,43.9-106h35.6c10.4,0,18.8-8.4,18.8-18.8
							C500,239.6,491.6,231.2,481.2,231.2z M395.1,231.2h-83.9c-1.2-3.9-2.8-7.6-4.7-11.2l59.3-59.4C381.3,180.7,391.7,204.8,395.1,231.2z
							 M339.1,134.1l-59.3,59.4c-3.5-1.9-7.2-3.4-11-4.6v-83.9C295,108.3,319.1,118.7,339.1,134.1z M231.2,104.9v83.9
							c-3.9,1.2-7.6,2.8-11.2,4.7l-59.4-59.3C180.7,118.8,204.8,108.3,231.2,104.9z M134.1,160.9l59.4,59.3c-1.9,3.5-3.4,7.2-4.6,11h-83.9
							C108.3,205,118.7,180.9,134.1,160.9z M104.9,268.8h83.9c1.2,3.9,2.8,7.6,4.7,11.2l-59.3,59.4C118.7,319.3,108.3,295.2,104.9,268.8z
							 M160.9,365.9l59.3-59.4c3.5,1.9,7.2,3.4,11,4.6v83.9C205,391.7,180.9,381.3,160.9,365.9z M268.8,395.1v-83.9
							c3.9-1.2,7.6-2.8,11.2-4.7l59.4,59.3C319.3,381.3,295.2,391.7,268.8,395.1z M365.9,339.1l-59.4-59.3c1.9-3.5,3.4-7.2,4.6-11h83.9
							C391.7,295,381.3,319.1,365.9,339.1z"/>
						</svg>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer class="main-footer">
		<div class="site-wrapper clear-fix">
			<div class="policy__container">
				<a href="https://willcatchfire.com/" class="sub-footer-fire" target="_blank">
				</a>
				<a href="privacy.html" class="policy">
					Политика конфиденциальности
				</a>
			</div>
			<div class="social__container">
				<a href="https://www.youtube.com/channel/UClqJwHqbQAyFlIjqhwt0Jyg" class="social__item">
					<svg class="icon icon-youtube"><use xlink:href="#icon-youtube"></use></svg>
				</a>
				<a href="https://www.facebook.com/hmlsproject" class="social__item">
					<svg class="icon icon-fb"><use xlink:href="#icon-fb"></use></svg>
				</a>
				<a href="https://www.instagram.com/hungry.monkey/" class="social__item">
					<svg class="icon icon-inst"><use xlink:href="#icon-inst"></use></svg>
				</a>
			</div>
			<div class="lsg">
				<svg ><use xlink:href="#iconlogo"></use></svg>
			</div>
		</div>
	</footer>
	<div class="price_popup">

		<div class="form__wrapper">
			<div class="popup__close"></div>
			<span class="form__header">
				Узнать цену
			</span>
			<form id="price_form" class="main-form has-validation-callback">
			<input id="geoloc_second"  type="hidden"  name="city" value="">
				<div class="form__container">
					<input name="name" type="text" data-validation="required, length" data-validation-length="min2" data-validation-error-msg="Введите, пожалуйста, Ваше имя">
					<span class="container__label">
						Имя
					</span>
					<i class="container__icon">
						<svg class="icon icon-form_icon1"><use xmlns:xlink="https://www.w3.org/1999/xlink" xlink:href="#icon-form_icon1"></use></svg>
					</i>
					<svg class="graphic " width="100%" height="42" viewBox="0 0 404 77" preserveAspectRatio="none">
					<path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
				</svg>
				</div>
				<div class="form__container">
					<input data-validation="required" type="text" name="phone" class="rfiled user_phone" data-validation-error-msg="Введите, пожалуйста, Ваш номер телефона">
					<span class="container__label">
						Телефон
					</span>
					<i class="container__icon">
						<svg class="icon icon-form_icon1"><use xmlns:xlink="https://www.w3.org/1999/xlink" xlink:href="#icon-form_icon2"></use></svg>
					</i>
					<svg class="graphic " width="100%" height="42" viewBox="0 0 404 77" preserveAspectRatio="none">
					<path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
				</svg>
				</div>
				<div class="form__container">
					<input id="mail_main-form" name="mail" type="text" data-validation="email" data-validation-error-msg="Введите, пожалуйста, E-mail в формате yourmail@gmail.com ">
					<span class="container__label">
						E-mail
					</span>
					<i class="container__icon">
						<svg class="icon icon-form_icon1"><use xmlns:xlink="https://www.w3.org/1999/xlink" xlink:href="#icon-form_icon3"></use></svg>
					</i>
					<svg class="graphic " width="100%" height="42" viewBox="0 0 404 77" preserveAspectRatio="none">
					<path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
				</svg>
				</div>
				<button class="form__button" id="price_popup_button">
					<svg class="button_ok" enable-background="new 0 0 24 24"  version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" id="Done__x2014__Displayed_on_the_left_side_of_a_contextual_action_bar__x28_CAB_x29__to_allow_the_user_to_dismiss_it._1_" points="  20,6 9,17 4,12 " stroke="#fff" stroke-miterlimit="10" stroke-width="2"/></svg>
              <p>полный вперёд</p>
				</button>
			</form>
			<div class="form_shturval">
				<svg version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
				<path d="M481.2,231.2h-35.6c-3.8-40.2-19.8-76.8-44.2-106.3l25.2-25.3c7.3-7.4,7.3-19.3,0-26.6c-7.4-7.3-19.3-7.3-26.6,0l-25.2,25.3
					c-29.4-24.2-65.9-40.1-105.9-43.9V18.8C268.8,8.4,260.4,0,250,0c-10.4,0-18.8,8.4-18.8,18.8v35.6c-40.2,3.8-76.8,19.8-106.3,44.2
					L99.6,73.4c-7.4-7.3-19.3-7.3-26.6,0c-7.3,7.4-7.3,19.3,0,26.6l25.3,25.2c-24.2,29.4-40.1,65.9-43.9,105.9H18.8
					C8.4,231.2,0,239.6,0,250c0,10.4,8.4,18.8,18.8,18.8h35.6c3.8,40.2,19.8,76.8,44.2,106.3l-25.2,25.3c-7.3,7.4-7.3,19.3,0,26.6
					c3.7,3.7,8.5,5.5,13.3,5.5c4.8,0,9.6-1.8,13.3-5.5l25.2-25.3c29.4,24.2,65.9,40.1,106,43.9v35.6c0,10.4,8.4,18.8,18.8,18.8
					c10.4,0,18.8-8.4,18.8-18.8v-35.6c40.2-3.8,76.8-19.8,106.3-44.2l25.3,25.2c3.7,3.7,8.5,5.5,13.3,5.5c4.8,0,9.6-1.8,13.3-5.5
					c7.3-7.4,7.3-19.3,0-26.6l-25.3-25.2c24.2-29.4,40.1-65.9,43.9-106h35.6c10.4,0,18.8-8.4,18.8-18.8
					C500,239.6,491.6,231.2,481.2,231.2z M395.1,231.2h-83.9c-1.2-3.9-2.8-7.6-4.7-11.2l59.3-59.4C381.3,180.7,391.7,204.8,395.1,231.2z
					 M339.1,134.1l-59.3,59.4c-3.5-1.9-7.2-3.4-11-4.6v-83.9C295,108.3,319.1,118.7,339.1,134.1z M231.2,104.9v83.9
					c-3.9,1.2-7.6,2.8-11.2,4.7l-59.4-59.3C180.7,118.8,204.8,108.3,231.2,104.9z M134.1,160.9l59.4,59.3c-1.9,3.5-3.4,7.2-4.6,11h-83.9
					C108.3,205,118.7,180.9,134.1,160.9z M104.9,268.8h83.9c1.2,3.9,2.8,7.6,4.7,11.2l-59.3,59.4C118.7,319.3,108.3,295.2,104.9,268.8z
					 M160.9,365.9l59.3-59.4c3.5,1.9,7.2,3.4,11,4.6v83.9C205,391.7,180.9,381.3,160.9,365.9z M268.8,395.1v-83.9
					c3.9-1.2,7.6-2.8,11.2-4.7l59.4,59.3C319.3,381.3,295.2,391.7,268.8,395.1z M365.9,339.1l-59.4-59.3c1.9-3.5,3.4-7.2,4.6-11h83.9
					C391.7,295,381.3,319.1,365.9,339.1z"></path>
				</svg>
			</div>
		</div>
	</div>
	<form action="POST" id="hidden_form">
    <input type="hidden" name="entry.1224343693">
    <input type="hidden" name="entry.548606585">
    <input type="hidden" name="entry.314428706">
    <input type="hidden" name="entry.1458904476">
</form>


	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->

<script>
    function loadCSS(hf) {
      var ms=document.createElement("link");ms.rel="stylesheet";
      ms.href=hf;document.getElementsByTagName("head")[0].appendChild(ms);
    }
    loadCSS("css/style.css");
    loadCSS("libs/owl-carousel/owl.carousel.css");
    loadCSS("https://fonts.googleapis.com/css?family=Roboto:400,300&subset=latin,cyrillic");
    loadCSS("https://fonts.googleapis.com/css?family=Roboto+Slab:400,300&subset=latin,cyrillic");

</script>

  <script>var scr = {"scripts":[
    {"src" : "libs/jquery/jquery-2.1.4.min.js", "async" : false},
    {"src" : "//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js", "async" : false},
    {"src" : "libs/owl-carousel/owl.carousel.min.js", "async" : false},
    {"src" : "libs/maskedinput/jquery.maskedinput.min.js", "async" : false},
    {"src" : "libs/snapsvg/snap.svg-min.js", "async" : false},
    // {"src" : "https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU", "async" : false},
    {"src" : "js/common.js", "async" : false}
    ]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
  </script>
	<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter --><!-- /Google Analytics counter -->
</body>
</html>
