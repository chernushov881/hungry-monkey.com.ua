//MENU TOGLE
$(document).ready(function(){
	// var refer = document.referrer
	// if (refer ==! 'http://hungry-monkey.com.ua'){
	// 	document.location.href = 'http://hungry-monkey.com.ua';
	// 	}
	// console.log(refer);
  var menu =  $('.main-menu');
  var morphEl = $('.morph-shape');
  var s = Snap( $( '.morph-shape svg' )[0] );
  var pathEl = s.select( 'path' );
  var start = "M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z";
  var pathOpen = $(morphEl).attr( 'data-morph-open')
  
  $('.menu__buton, .menu_close').click(function(){
	  if ($(menu).hasClass('opened')) {
		$(menu).removeClass('opened');
		pathEl.stop().animate({
		  "path" : start
		}, 350, mina.easeinout);
		$('.main-menu nav').removeClass('opened');
		$('.main-menu__contacts').removeClass('opened');
		$(".menu__buton").removeClass("cross");
	  }
	  else {
		$(menu).addClass('opened');
		pathEl.stop().animate({
		  "path" : pathOpen
		}, 350, mina.easeinout);
		$('.main-menu nav').addClass('opened');
		$('.main-menu__contacts').removeClass('opened');
		$(".menu__buton").addClass("cross");
	  }
  });
  $('.callback__button').click(function(){
	if ($(menu).hasClass('opened')) {
		$('.main-menu__contacts').addClass('opened');
		$('.main-menu nav').removeClass('opened');
	}
	else {
	  $(menu).addClass('opened');
	  $('.main-menu__contacts').addClass('opened');
	  pathEl.stop().animate({
		  "path" : pathOpen
		}, 350, mina.easeinout);
	  $(".menu__buton").addClass("cross");
	}
  });
  //Плавный скролл до блока по клику
  $(".main-menu a, #detail_of_webinar").click(function() {
	var link = $(this).attr('href');
	var elem = document.getElementById(link);
	var offset = $(elem).offset();
	$("html, body").animate({
	  scrollTop:offset.top
	}, 500);
	$(".menu__buton").removeClass("cross");
	$('.main-menu nav').removeClass('opened');
	$(menu).removeClass('opened');
	return false;
  });

}); 


$('.owl-carousel').owlCarousel({
	margin:10,
	nav: true,
	navText: ["",""],
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:5
		}
	}
});
$(document).ready(function(){
	var date, time;
	var server_date = {
		month : "",
		day : "",
		week_day : ""
	};
	var week = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"];
	// функция которая возвращает серверную дату и подставляет значения
	(function(){
		$.ajax({
			type: "POST",
			url: "get_date.php",
			data: "",
			dataType: "JSON",
			success: function(result){
				for (e in result ) {
					server_date[e] = result[e];
				}
				$('.owl-carousel .item .date').each(function(){
					$(this).html(server_date.day+"."+server_date.month);
					server_date.day++;
				}); 
				$('.owl-carousel .item .day').each(function(index){
						$(this).html(week[server_date.week_day]);
						server_date.week_day++;
						if (server_date.week_day == 7) {
							server_date.week_day =0;
						}
				}); 
			}
		});

	})();
	// "Сегодня"
	$('.owl-item:nth-child(1) .day').html('Сегодня');
	// функция отправки хука в amoCRM
	function send_data(){
		date =  Date.parse(date+' '+time)/1000;
		var data = "my_date="+date;
		$.ajax({
			type: "POST",
			url: "amo/amo_action.php",
			data: data,
			success: function(result){
				console.log(result);
				console.log(data);
			}
		});

	};
	// CALENDAR CLICK
	$('.callback__calendar .owl-carousel .item').click(function(){
		$('.callback__calendar .owl-carousel .item').removeClass('item__cheked');
		date = $(this).addClass('item__cheked').find('.date').html()+'.2016';
		date = date.split('.')[2]+'-'+date.split('.')[1]+'-'+date.split('.')[0];
		console.log(date);
		$('.callback__calendar, .callback__time').addClass('active');
	});
	// TIME CLICK
	$('.callback__time .time__item .item__time').click(function(){
		time = $(this).data('time');
		console.log(time);
		$('.callback__time .time__item').removeClass('active');
		$(this).parent().toggleClass('active');
	});
	//FINAL CLICK
	$('.callback__time .time__item .item__button').click(function(){
		$(this).addClass('active');
		send_data();
		setTimeout(function(){
			$('.item__button.active .button_ok').css({
				'transform': 'translate(-50%, -50%) scale(1)',
				'-ms-transform': 'translate(-50%, -50%) scale(1)',
				'-webkit-transform': 'translate(-50%, -50%) scale(1)'
			});
		}, 500);
		 setTimeout( function(){
			$('.callback__time, .callback__calendar, .calendar__title').css({
/*                'transform': 'translate(-2000px, 0)',
				'-ms-transform': 'translate(-2000px, 0)',
				'-webkit-transform': 'translate(-2000px, 0)',*/
				'opacity' : 0 
			});
		},1000);
		 setTimeout( function(){
			$('.callback__time, .callback__calendar, .calendar__title').css({
/*                'transform': 'translate(-2000px, 0)',
				'-ms-transform': 'translate(-2000px, 0)',
				'-webkit-transform': 'translate(-2000px, 0)',*/
				'display' : 'none' 
			});
		},1700);
	});
});
