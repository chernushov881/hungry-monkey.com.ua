;

// UTM, LEADS, SQL
function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
 
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

    /*geolocaion*/
// $('form button').click(function(){
//   var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
//   $("#geoloc, #geoloc_second").val(loc);
// });



var href = document.location.href;
var new_url = href.split('?')[1];
var url = href.split('?')[0];
var utm_catch = '&' + new_url + "&page_url="+url;
var ref = '&ref=' + document.referrer;
var invite_id = "&invite_id="+href.split('invite_id=')[1];

    // присваиваем hmid и ложим в базу
$(document).ready(function(){
  
  var cookie_check = setInterval(function(){
    var ga = get_cookie('_ga');
    if (ga == null) {
      console.log('111');
    }
    else {
      var cook_ga = "&_ga="+get_cookie('_ga');
      var hmid =  cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
      var hmid_link =  "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
      clearInterval(cookie_check);
      // var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
      // var city = "&city=" +loc;
      var temp_date = new Date();
      var temp_month = temp_date.getMonth();
      temp_month++;
      var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
      var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
      var data; 
      data += utm_catch;
      data += date_submitted;
      data += time_submitted;
      data += ref;
      // data += city;
      data += cook_ga;
      data += hmid_link;  
      data += invite_id; 
      

      $.ajax({
            type: "GET",
            url: "visits.php",
            data: data,
            success: function() {
        console.log('done');
            }
          });
    }
  } ,500);
});

//FORM VALIDATION
$.validate({
  form : 'form',
  addValidClassOnAll : true,
  borderColorOnError: "#F15445 ",
  onSuccess : function($form){
      if ($form.attr('id') == 'mian_register_form' ) {
        var data = $("#mian_register_form").serialize();

        var temp_date = new Date();
        var temp_month = temp_date.getMonth();
        temp_month++;
        var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
        var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
        var cook_ga = "&_ga="+get_cookie('_ga');
        var hmid_link =  "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
        data += utm_catch;
        data += date_submitted;
        data += time_submitted;
        data += ref; 
        data += hmid_link; 
        data += invite_id; 
        $("#mian_register_form").find('.form__button').addClass('disabled').prop('disabled', true);
        $.ajax({
          type: "POST",
          url:"amo/amocontactlist.php",
          data: data
        });
        $.ajax({
        type: "POST",
        url: "register_mail.php",
        data: data,
          success: function() {
            setTimeout(function() {
             window.location.href = "thanks.html";
            }, 1500);
          }
        });
    return false;
      }
      else if ($form.attr('id') == 'price_form' ) {
        var data = $("#price_form").serialize();

        var temp_date = new Date();
        var temp_month = temp_date.getMonth();
        temp_month++;
        var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
        var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
        var cook_ga = "&_ga="+get_cookie('_ga');
        var hmid_link =  "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
        data += utm_catch;
        data += date_submitted;
        data += time_submitted;
        data += ref; 
        data += hmid_link; 
        data += invite_id; 
        $("#price_form").find('.form__button').addClass('disabled').prop('disabled', true);
        $.ajax({
          type: "POST",
          url:"amo/amocontactlist.php",
          data: data
        });
        $.ajax({
        type: "GET",
        url: "price_mail.php",
        data: data,
          success: function() {

            setTimeout(function() {
             window.location.href = "thanks.html";
            }, 1500);

          }
        });
    return false;
      }
  }

});

//FORM SHOW 
$('#price_button').click(function(){
  $('.price_popup').toggleClass('visible');
});

$('#top_callback, #footer_callback').click(function(){
  $('.popup__callback').show(400);
});

//FORM HIDE 
$('.popup__close').click(function(){
  $('.price_popup').toggleClass('visible');
});



$('#terms_popup_show').click(function(){
  $('.popup_terms').addClass('terms_show');
});
$('.close_terms').click(function(){
  $('.popup_terms').removeClass('terms_show');
});




// OWL CAROUSEL CHOICE
$('.owl-carousel').owlCarousel({
    center: true,
    margin:0,
    loop:true,
    autoWidth:true,
    items:6,
    nav: true,
    navText: ["",""]
})

$(document).ready(function(){
  $('.owl-prev, .owl-next').append('<span class="control_left"></span><span class="control_right"></span>');
});

// OWL CAROUSEL INSRUCTORS
$('.owl-instructors').owlCarousel({
    margin:0,
    items:2,
    nav: true,
    loop: true,
    slideBy: 2,
    navText: ["",""],
    dots: false,
    responsive : {
    // breakpoint from 0 up
    0 : {
        items:1,
        slideBy: 1
    },
    // breakpoint from 480 up
    480 : {
        items:1
    },
    1200 : {
        items:2
    }
}
})
/*if ($(window).width() < 1200) {
    var clon = $('.right_pic').clone();
    $(clon[0]).appendTo($('.empty_item').parent());
    $('.empty_item').hide();
}*/
$(document).ready(function(){
  $('.insctructors .owl-prev,.insctructors .owl-next').append('<span class="control_left"></span><span class="control_right"></span>');
});

//MASKED INPUT
jQuery(function($){
$(".user_phone").mask("+38(999) 999-9999");
});


//ANIMATION 
$(document).ready(function(){
  function isVisible(e) {
    var wh = $(window).height() + $(window).scrollTop(); 
    var el = $(e);
    var eh = el.offset().top ;
    if (eh < wh) {
      return true;
    }
    
  }
  function isVisibleFull(e) {
    var wh = $(window).height() + $(window).scrollTop(); 
    var el = $(e);
    var eh = el.offset().top + el.outerHeight();
    if (eh < wh) {
      return true;
    }
    
  }

  $(window).scroll(function(){

    var whymonkey = isVisible('.whymonkey__container .container__item');
    var doyoutry__header = isVisibleFull('.doyoutry__header');
    var doyoutry__container = isVisible('.doyoutry__container');
    var videoscreen__question = isVisibleFull('.videoscreen__question');
    var teach__col = isVisibleFull('.teach__col:nth-child(1) .col__wrapper .col__text:nth-child(1)');
    var doubts__subtitle = isVisible('.doubts .site-wrapper');
    var icons_sections = isVisible('.icons-sections__container');
    var icons_sections__container = isVisible('.icons-sections__container');
    var whatyouget = isVisible('.whatyouget__row .whatyouget__col .wrapper_left');
    var insctructors__title = isVisible('.insctructors .site-wrapper');
    var price__description = isVisible('.price__title');
    var numbers__title = isVisible('.numbers__container');
    var issa = isVisibleFull('.issa__title');
    var issa__item1 = isVisible('.issa__item:nth-child(2)');
    var issa__item2 = isVisible('.issa__item:nth-child(3)');
    var issa__item3 = isVisible('.issa__item:nth-child(4)');
    var issa__item4 = isVisible('.issa__item:nth-child(5)');
    var feedbacks__title = isVisibleFull('.feedbacks__subtitle');
    var feedbacks__container = isVisible('.feedbacks__container');
    var dreams__title = isVisible('.dreams .site-wrapper');
    var main_footer = isVisible('.main-footer');

    if(whymonkey) {
      $('.whymonkey__container .container__item:nth-child(1)').addClass('visible');
      $('.whymonkey__container .container__item:nth-child(2)').addClass('visible');
      $('.whymonkey__container .container__item:nth-child(3)').addClass('visible');
      $('.whymonkey__container .container__item:nth-child(4)').addClass('visible');
    }

    if(doyoutry__header) {
     $('.doyoutry__header').addClass('visible');
    }

    if(doyoutry__container) {
     $('.doyoutry__container .container__item:nth-child(1)').addClass('visible');
      $('.doyoutry__container .container__item:nth-child(2)').addClass('visible');
      $('.doyoutry__container .container__item:nth-child(3)').addClass('visible');
    }

    if(videoscreen__question) {
     setTimeout(function(){
      $('.videoscreen__question').addClass('visible');
     }, 100);
     setTimeout(function(){
      $('.videoscreen__text').addClass('visible');
     }, 400);
     setTimeout(function(){
      $('.videoscreen__title').addClass('visible');
     }, 700);   
    }


    if(teach__col) {
     $('.teach__col .col__text').addClass('visible');
    }

    if(doubts__subtitle) {
     $('.doubts__subtitle, .doubts__description, .doubts__title, .button__wrapper').addClass('visible');
    }

     if(icons_sections) {
     $('.icons-sections__title').addClass('visible');
    }
    
     if(icons_sections__container) {
     $('.icons-sections__container .container__item').addClass('visible');
    }

    if(icons_sections) {
     $('.icons-sections__title').addClass('visible');
    }

   if(whatyouget) {
      $('.whatyouget__row').addClass('visible');
    }

    if(insctructors__title) {
      $('.insctructors__title').addClass('visible');
      $('.owl-instructors').addClass('visible');
    }

    if(price__description) {
      $('.price__description, .price__title').addClass('visible');
    }
    
    if(numbers__title) {
      $('.numbers__title, .container__item').addClass('visible');
    }

    if(issa) {
      $('.issa__title').addClass('visible');
    }

    if(issa__item1) {
      $('.issa__item:nth-child(2)').addClass('visible');
    }
    if(issa__item2) {
      $('.issa__item:nth-child(3)').addClass('visible');
    }
    if(issa__item3) {
      $('.issa__item:nth-child(4)').addClass('visible');
    }
    if(issa__item4) {
      $('.issa__item:nth-child(5)').addClass('visible');
    }
    
    if(feedbacks__title) {
      $('.feedbacks__title, .feedbacks__subtitle').addClass('visible');
    }
    if(feedbacks__container) {
      $('.feedbacks__container').addClass('visible');
    }

     if(dreams__title) {
      $('.dreams__title, .dreams__subtitle').addClass('visible');
    } 
  });

    var whymonkey = isVisible('.whymonkey__container .container__item');
    var doyoutry__header = isVisibleFull('.doyoutry__header');
    var doyoutry__container = isVisible('.doyoutry__container');
    var videoscreen__question = isVisibleFull('.videoscreen__question');
    var teach__col = isVisibleFull('.teach__col:nth-child(1) .col__wrapper .col__text:nth-child(1)');
    var doubts__subtitle = isVisible('.doubts .site-wrapper');
    var icons_sections = isVisible('.icons-sections__container');
    var icons_sections__container = isVisible('.icons-sections__container');
    var whatyouget = isVisible('.whatyouget__row .whatyouget__col .wrapper_left');
    var insctructors__title = isVisible('.insctructors .site-wrapper');
    var price__description = isVisible('.price__title');
    var numbers__title = isVisible('.numbers__container');
    var issa = isVisibleFull('.issa__title');
    var issa__item1 = isVisible('.issa__item:nth-child(2)');
    var issa__item2 = isVisible('.issa__item:nth-child(3)');
    var issa__item3 = isVisible('.issa__item:nth-child(4)');
    var issa__item4 = isVisible('.issa__item:nth-child(5)');
    var feedbacks__title = isVisibleFull('.feedbacks__subtitle');
    var feedbacks__container = isVisible('.feedbacks__container');
    var dreams__title = isVisible('.dreams .site-wrapper');
    var main_footer = isVisible('.main-footer');

    if(whymonkey) {
      $('.whymonkey__container .container__item:nth-child(1)').addClass('visible');
      $('.whymonkey__container .container__item:nth-child(2)').addClass('visible');
      $('.whymonkey__container .container__item:nth-child(3)').addClass('visible');
      $('.whymonkey__container .container__item:nth-child(4)').addClass('visible');
    }

    if(doyoutry__header) {
     $('.doyoutry__header').addClass('visible');
    }

    if(doyoutry__container) {
     $('.doyoutry__container .container__item:nth-child(1)').addClass('visible');
      $('.doyoutry__container .container__item:nth-child(2)').addClass('visible');
      $('.doyoutry__container .container__item:nth-child(3)').addClass('visible');
    }

    if(videoscreen__question) {
     setTimeout(function(){
      $('.videoscreen__question').addClass('visible');
     }, 100);
     setTimeout(function(){
      $('.videoscreen__text').addClass('visible');
     }, 400);
     setTimeout(function(){
      $('.videoscreen__title').addClass('visible');
     }, 700);   
    }


    if(teach__col) {
     $('.teach__col .col__text').addClass('visible');
    }

    if(doubts__subtitle) {
     $('.doubts__subtitle, .doubts__description, .doubts__title, .button__wrapper').addClass('visible');
    }

     if(icons_sections) {
     $('.icons-sections__title').addClass('visible');
    }
    
     if(icons_sections__container) {
     $('.icons-sections__container .container__item').addClass('visible');
    }

    if(icons_sections) {
     $('.icons-sections__title').addClass('visible');
    }

   if(whatyouget) {
      $('.whatyouget__row').addClass('visible');
    }

    if(insctructors__title) {
      $('.insctructors__title').addClass('visible');
      $('.owl-instructors').addClass('visible');
    }

    if(price__description) {
      $('.price__description, .price__title').addClass('visible');
    }
    
    if(numbers__title) {
      $('.numbers__title, .container__item').addClass('visible');
    }

    if(issa) {
      $('.issa__title').addClass('visible');
    }

    if(issa__item1) {
      $('.issa__item:nth-child(2)').addClass('visible');
    }
    if(issa__item2) {
      $('.issa__item:nth-child(3)').addClass('visible');
    }
    if(issa__item3) {
      $('.issa__item:nth-child(4)').addClass('visible');
    }
    if(issa__item4) {
      $('.issa__item:nth-child(5)').addClass('visible');
    }
    
    if(feedbacks__title) {
      $('.feedbacks__title, .feedbacks__subtitle').addClass('visible');
    }
    if(feedbacks__container) {
      $('.feedbacks__container').addClass('visible');
    }

     if(dreams__title) {
      $('.dreams__title, .dreams__subtitle').addClass('visible');
    } 
  

});


//VIDEO START AFTER PAGE LOAD
$(document).ready(function(){
  var video = $('#hero_header'),
  videoElement = $(video)[0];

  var callback = function(video){
    $('#hero_header').get(0).play();
    $('.overlay').hide();
    clearInterval(tmr);
  };


  var tmr = setInterval(function(){
    if (videoElement.readyState > 3) {
      callback();
      console.log('1');
    }
  },500);
  if($(window).width() < 600) {
    $(video).remove();
    clearInterval(tmr);
  }
});


// DOUDTS 
$('.doubts__title').click(function(){
  $('.doubts__container').slideToggle();
  $('.doubts__title').toggleClass('opened');
  setTimeout( function(){
    $('.doubts').toggleClass('opened');
  },500);
});

// form 
$('form input').change(function(){
  if ($(this).val() !== "") {
      $(this).next('.container__label').hide();
  }
  else {
    $(this).next('.container__label').show()
  }
});
$('form input').focus(function(){
  $(this).parent().addClass('focused');
  $('.form_shturval').addClass('focused');
});
$('form input').focusout(function(){
  $(this).parent().removeClass('focused');
  $('.form_shturval').removeClass('focused');
});

//MENU TOGLE
$(document).ready(function(){
  var menu =  $('.main-menu');
  var morphEl = $('.morph-shape');
  var s = Snap( $( '.morph-shape svg' )[0] );
  var pathEl = s.select( 'path' );
  var start = "M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z";
  var pathOpen = $(morphEl).attr( 'data-morph-open')
  
  $('.menu__buton, .menu_close').click(function(){
      if ($(menu).hasClass('opened')) {
        $(menu).removeClass('opened');
        pathEl.stop().animate({
          "path" : start
        }, 350, mina.easeinout);
        $('.main-menu nav').removeClass('opened');
        $('.main-menu__contacts').removeClass('opened');
        $(".menu__buton").removeClass("cross");
      }
      else {
        $(menu).addClass('opened');
        pathEl.stop().animate({
          "path" : pathOpen
        }, 350, mina.easeinout);
        $('.main-menu nav').addClass('opened');
        $('.main-menu__contacts').removeClass('opened');
        $(".menu__buton").addClass("cross");
      }     
  });
  $('.callback__button').click(function(){
    if ($(menu).hasClass('opened')) {
        $('.main-menu__contacts').addClass('opened');
        $('.main-menu nav').removeClass('opened');
    }
    else {
      $(menu).addClass('opened');
      $('.main-menu__contacts').addClass('opened');
      pathEl.stop().animate({
          "path" : pathOpen
        }, 350, mina.easeinout);
      $(".menu__buton").addClass("cross");
    }
  });
  //Плавный скролл до блока по клику
  $(".main-menu a, #detail_of_webinar").click(function() {
    var link = $(this).attr('href');
    var elem = document.getElementById(link);
    var offset = $(elem).offset();
    $("html, body").animate({
      scrollTop:offset.top
    }, 500);
    $(".menu__buton").removeClass("cross");
    $('.main-menu nav').removeClass('opened');
    $(menu).removeClass('opened');
    return false;
  });

}); 

// Временное решение для кнопки главного экрана
 $("#header_button, #teach_button,  #doubts_button").click(function() {
    var offset = $('#mian_register_form').offset();
    $("html, body").animate({
      scrollTop:offset.top
    }, 500);
    
    return false;
  });

//FEEDBACKS
var item3 = $('.feedbacks__container .container__item:nth-child(3)');
var item2 = $('.feedbacks__container .container__item:nth-child(2)');
var item1 = $('.feedbacks__container .container__item:nth-child(1)');
$(document).ready(function(){
  $(item2).find('.item__overlay').addClass('showed');
});

$(item3).click(function(){
  $(this).addClass('hovered');
  $(item2).addClass('smalledR').find('.item__overlay').addClass('hiden');
  $(item2).removeClass('smalledL');
  $(this).find('.item__overlay').addClass('showed').removeClass('hiden');
  $(item1).removeClass('hovered').find('.item__overlay').addClass('hiden').removeClass('showed');
});

$(item2).click(function(){
  $(this).removeClass('smalledR').find('.item__overlay').addClass('showed').removeClass('hiden');
  $(item3).removeClass('hovered').find('.item__overlay').addClass('hiden').removeClass('showed');
  $(item1).removeClass('hovered').find('.item__overlay').addClass('hiden').removeClass('showed');
  $(this).removeClass('smalledL');
});

$(item1).click(function(){
  $(this).addClass('hovered').find('.item__overlay').addClass('showed').removeClass('hiden');
  $(item2).addClass('smalledL').find('.item__overlay').addClass('hiden');
  $(item3).removeClass('hovered').find('.item__overlay').addClass('hiden').removeClass('showed');
});

// LIKES
$(document).ready(function() {
function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
 
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}
function delete_cookie ( cookie_name )
{
  var cookie_date = new Date ( );  // Текущая дата и время
  cookie_date.setTime ( cookie_date.getTime() - 1 );
  document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}



  $(".overlay__button").bind("click", function(event) {
    var thisEl = $(this).parent().parent().find('.overlay__like');

    if ($(this).hasClass('disabled')) {
      $('.overlay__button, .owl-item .overlay__text ').show();
      $('.owl-item .overlay__button').removeClass('disabled').show(); 
      $('.owl-item .overlay__text').show().html('Выбрать');
      var id = $(thisEl).attr("data-id");
      delete_cookie(id);
      $.ajax({
        url: "unlike.php",
        type: "POST",
        data: ("id=" + $(thisEl).attr("data-id")),
        dataType: "text",
        success: function(result) {
          if (result) {
            $(thisEl).text(Number($(thisEl).text()) - 1);
          }
        }
      });
    }
    else {
      document.cookie = $(thisEl).attr("data-id") + "=yes";
      $.ajax({
        url: "likes.php",
        type: "POST",
        data: ("id=" + $(thisEl).attr("data-id")),
        dataType: "text",
        success: function(result) {
          if (result) {
            $(thisEl).text(Number($(thisEl).text()) + 1);
           if (get_cookie('id1')) {
      $('.overlay__button, .owl-item .overlay__text ').hide();
    $('#databutton1 .overlay__button').addClass('disabled').show(); 
    $('#databutton1 .overlay__text').show().html('Выбрано');
    }
    if (get_cookie('id2')) {
      $('.overlay__button, .owl-item .overlay__text ').hide();
      $('#databutton2 .overlay__button').addClass('disabled').show(); 
      $('#databutton2 .overlay__text').show().html('Выбрано');
    }
    if (get_cookie('id3')) {
      $('.overlay__button, .owl-item .overlay__text ').hide();
      $('#databutton3 .overlay__button').addClass('disabled').show(); 
      $('#databutton3 .overlay__text').show().html('Выбрано');
    }
    if (get_cookie('id4')) {
      $('.overlay__button, .owl-item .overlay__text ').hide();
      $('#databutton4 .overlay__button').addClass('disabled').show(); 
      $('#databutton4 .overlay__text').show().html('Выбрано');
    }
    if (get_cookie('id5')) {
      $('.overlay__button, .owl-item .overlay__text ').hide();
      $('#databutton5 .overlay__button').addClass('disabled').show(); 
      $('#databutton5 .overlay__text').show().html('Выбрано');
    }
    if (get_cookie('id6')) {
      $('.overlay__button, .owl-item .overlay__text ').hide();
      $('#databutton6 .overlay__button').addClass('disabled').show(); 
      $('#databutton6 .overlay__text').show().html('Выбрано');
    }
    if (get_cookie('id7')) {
      $('.overlay__button, .owl-item .overlay__text ').hide();
      $('#databutton7 .overlay__button').addClass('disabled').show(); 
      $('#databutton7 .overlay__text').show().html('Выбрано');
    }
          }
          else {
            console.log('eror');
          }
        }
      });

    }
});
  if (get_cookie('id1')) {
    $('.overlay__button, .owl-item .overlay__text ').hide();
    $('#databutton1 .overlay__button').addClass('disabled').show(); 
    $('#databutton1 .overlay__text').show().html('Выбрано');
  }
  if (get_cookie('id2')) {
    $('.overlay__button, .owl-item .overlay__text ').hide();
    $('#databutton2 .overlay__button').addClass('disabled').show(); 
    $('#databutton2 .overlay__text').show().html('Выбрано');
  }
  if (get_cookie('id3')) {
    $('.overlay__button, .owl-item .overlay__text ').hide();
    $('#databutton3 .overlay__button').addClass('disabled').show(); 
    $('#databutton3 .overlay__text').show().html('Выбрано');
  }
  if (get_cookie('id4')) {
    $('.overlay__button, .owl-item .overlay__text ').hide();
    $('#databutton4 .overlay__button').addClass('disabled').show(); 
    $('#databutton4 .overlay__text').show().html('Выбрано');
  }
  if (get_cookie('id5')) {
    $('.overlay__button, .owl-item .overlay__text ').hide();
    $('#databutton5 .overlay__button').addClass('disabled').show(); 
    $('#databutton5 .overlay__text').show().html('Выбрано');
  }
  if (get_cookie('id6')) {
    $('.overlay__button, .owl-item .overlay__text ').hide();
    $('#databutton6 .overlay__button').addClass('disabled').show(); 
    $('#databutton6 .overlay__text').show().html('Выбрано');
  }
  if (get_cookie('id7')) {
    $('.overlay__button, .owl-item .overlay__text ').hide();
    $('#databutton7 .overlay__button').addClass('disabled').show(); 
    $('#databutton7 .overlay__text').show().html('Выбрано');
  }


});




//video-section_button

$('#video-section_button').click(function(){
  $('.videopopup').toggleClass('visible');
});
$('.videopopup').click(function(){
  $(this).toggleClass('visible');
});