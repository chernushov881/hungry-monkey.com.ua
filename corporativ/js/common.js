$(function() {

		// OWL CARoUSELLE
	$(document).ready(function(){
		var owl = $(".video-wraper");
		owl.owlCarousel({
				items: 1,
				slideSpeed : 600,
				addClassActive: true,
				loop: true,
				smartSpeed: 600,
				fluidSpeed: 600,
				navSpeed: 600,
				dotsSpeed: 600,
				dragEndSpeed: 600,
				nav: false
			});
	// 	$(".video-prev").click(function(){
	// 		owl_testimonials.trigger('prev.owl.carousel');
	// 	});
	// 	$(".video-next").click(function(){
	// 		owl_testimonials.trigger('next.owl.carousel');
	// 	});
	});

		//HIDE CALCULATE BLOCK
	$('h4.calculate-arrow').click(function(){
		$('.calculate-hide').slideToggle(400);
	});

		//FOOTER FORM
	$('.form__container input').change(function(){
		if( $(this).val() !== '') {
			$(this).closest('.form__container').addClass('has-data');
		};
		if( $(this).val() == '') {
			$(this).closest('.form__container').removeClass('has-data');
		};
	});

		//POP-UP MOBILE MENU
	$('#menuToggle').click(function(){
		$('nav').toggleClass('faded');
		$('nav ul').toggleClass('active-menu');
	});

	//PREVIEW FOR IMAGES
	$('.select-yachtich-photo-2, .select-yachtich-photo-3').click(function(){
		cur_src_img = $(this).children('img').attr('src');
		active_src_img = $(this).prevAll('.select-yachtich-photo-1').children('img').attr('src');
		$(this).prevAll('.select-yachtich-photo-1').children('img').attr('src', cur_src_img);
		$(this).children('img').attr('src', active_src_img);
	});
		$('.select-location-photo-2, .select-location-photo-3').click(function(){
		cur_src_img = $(this).children('img').attr('src');
		active_src_img = $(this).prevAll('.select-location-photo-1').children('img').attr('src');
		$(this).prevAll('.select-location-photo-1').children('img').attr('src', cur_src_img);
		$(this).children('img').attr('src', active_src_img);
	});
	// 	//MASK INPUT
	$(".user_phone").mask("+38(999) 999-9999");

		//PLYR Player video
	var player = plyr.setup('.plyr-player')[0].plyr;
	
	$('.close-video-btn').click(function(){
		$('.video-wrapper').fadeOut();
		player.pause();
	});
	$('.video-icon').click(function(){
		var new_video_src = $(this).parent('.video-item').attr('data-video-id');
		var new_video_tittle = $(this).parent('.video-item').attr('data-video-title');
		$('.video-wrapper').fadeIn();
		player.source({
				type: 'video',
				title: new_video_tittle,
				autoplay: true,
				hideControls: true,
				clickToPlay: true,
				showPosterOnEnd: true,
				sources: [{
						src: new_video_src,
						type: 'youtube'
				}]
		});
	});

	//MENU ACTIVE CLASS & SCROLL
	$(window).scroll(function(){
		if ($(window).scrollTop() > $('header').outerHeight()) {
			$('.nav-wrapper').addClass('fixed');
		} else {
			$('.nav-wrapper').removeClass('fixed');
		}
	});

	$(document).ready(function(){
			$('nav ul a[href^="#"]').on('click',function (e) {
					e.preventDefault();
					var target = this.hash;
					var $target = $(target);
					var scroll;
					if($(window).scrollTop()==0){
							scroll =  ($target.offset().top) - 120
					}else{
							scroll =  ($target.offset().top) - 45
					}
					if($('nav ul').hasClass('active-menu')){
						$('nav').removeClass('faded');
						$('nav ul').removeClass('active-menu');
						$('#menuToggle input').prop("checked", false);
					}
					$('html, body').stop().animate({
							'scrollTop': scroll
					}, 900, 'swing', function () {
							//window.location.hash = target;
					});
			});
	});

	$(window).on('scroll', function () {
		var sections = $('section'),
		nav = $('nav'),
		nav_height = nav.outerHeight(),
		cur_pos = $(this).scrollTop();
		sections.each(function() {
			var top = $(this).offset().top - nav_height,
					bottom = top + $(this).outerHeight();
			if (cur_pos >= top && cur_pos <= bottom) {
				nav.find('li a').removeClass('active');
				sections.removeClass('active');
				$(this).addClass('active');
				nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
			}
		});
	});

	//Scroll to bottom form
	$('.header-btn, .link-contact, .location-reserve, .yachtich-reserve').on("click", function(){
		target = $("#main_form");
		$('html, body').animate({scrollTop: $(target).offset().top}, 1500);
	});
});


		//FORM VALIDATION + AJAX SEND
	function get_cookie ( cookie_name ){
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}

	var href = document.location.href;
	var new_url = href.split('?')[1];
	var id = 'marketing';
	var url = href.split('?')[0];
	var utm_catch = '&' + new_url + "&page_url=" + url;
	var ref = '&ref=' + document.referrer;
	var leade_name = "&lead_name=" + id;
	var lead_price = "&lead_price=" + $('#price').html();
	var invite_id = "&invite_id="+href.split('invite_id=')[1];
	var cook_ga;
	var hmid;
			// присваиваем hmid и ложим в базу
	$(document).ready(function(){
		var cookie_check = setInterval(function(){
			var ga = get_cookie('_ga');
			if (ga === null) {
				console.log('111');
			}
			else {
				cook_ga = "&_ga="+get_cookie('_ga');
				var hmid = "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				// var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
				var page_url = "&page_url="+url;
				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				var data = hmid+utm_catch+invite_id+date_submitted+time_submitted+ref;
				clearInterval(cookie_check);
				$.ajax({
							type: "GET",
							url: "visits.php",
							data: data,
							success: function() {
					console.log('done');
							}
						});
			}
		} ,500);
	});
	$.validate({
		form : 'form',
		addValidClassOnAll : true,
		borderColorOnError: "#ff5722",
		onSuccess : function($form){
			// var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
			// $form.find('#geoloc').val(loc);
			var data = $form.serialize();
			var data_form = $form.attr('data-form');
			var temp_date = new Date();
			var temp_month = temp_date.getMonth();
			temp_month++;
			var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
			var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
			data += utm_catch;
			data += date_submitted;
			data += time_submitted;
			data += ref;
			data += cook_ga;
			data += leade_name;
			data += '&data_form=' + data_form;
			data += '&hmid=' + hmid;
			$.ajax({
				type: "GET",
				url: "register_mail.php",
				data: data,
				beforeSend: function() {
					$form.find('button').prop( "disabled", true ).text('');
					$form.find('.spinner').fadeIn();
				}
			});
			$.ajax({
				type: "POST",
				url:"amo/amocontactlist.php",
				data: data,
				success: function() {
					// console.log('ok!');
					$form.find('.spinner').fadeOut();
					setTimeout(function() {
						$form.find('button').text('✔ Отправлено');
					}, 350);
					// dataLayer.push({'event': 'FormSumit_main', 'form_type': data_form});
				}
			});
		// setTimeout(function() {
		// 			window.location.href = "thanks.html";
		// 	}, 1500);

		return false;
		}
	});

