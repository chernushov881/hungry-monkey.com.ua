$(function() {

	$(document).ready(function(){
		$('header .header-head .header-nav').css('transition', 'transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0)');
	});

	//Menu
	$('#menuToggle').click(function(){
		$('#menuToggle input').prop('checked') ? $('header').addClass('active-menu') : $('header').removeClass('active-menu');
	});

	//Pop-up form
	$('.show-pop-up').click(function(e){
		e.preventDefault();
		$('.pop-up-form-wrap').addClass('pop-up-active');
		var ski_type = $(this).text();
		$('.pop-up-form-wrap form').data('form', 'pop-up-form ' + ski_type).attr('data-form', 'pop-up-form ' + ski_type);
		$('.pop-up-form-wrap form button').text(ski_type);
	});
	$('.pop-up-form-close').click(function(){
		$('.pop-up-form-wrap').removeClass('pop-up-active');
	});

	//Call me
	$('.hader-callme-btn').click(function(e){
		e.preventDefault();
		$('.call-me-form-wrap').addClass('call-me-active');
	});
	$('.call-me-form-close').click(function(){
		$('.call-me-form-wrap').removeClass('call-me-active');
	});

	//Hide pop-up on ESC
		$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$('.call-me-form-wrap').removeClass('call-me-active');
			$('.pop-up-form-wrap').removeClass('pop-up-active');
		}
	});

	//Hide pop-up on outside click
	$(document).mouseup(function(e) {
		if ($(e.target).hasClass("call-me-form-wrap") || $(e.target).hasClass("pop-up-form-wrap")) {
		$('.call-me-form-wrap').removeClass('call-me-active');
		$('.pop-up-form-wrap').removeClass('pop-up-active');
		};
	});

	//Scroll to bottom form
	$('.header-btn button').click(function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop: $('.footer-form-wrap form').offset().top}, 800);
	});

	//Menu Scroll
	$('nav ul a[href^="#"]').on('click',function (e) {
		e.preventDefault();
		var target = this.hash;
		var $target = $(target);
		var scroll = $target.offset().top;
		$('html, body').animate({
			'scrollTop': scroll
		}, 700, 'swing');
	});

	//Lights Slider
	$(document).ready(function(){
		var slider = $(".testimonials-slider");
		slider.lightSlider({
			item: 1,
			controls: false,
			pager: false,
			adaptiveHeight: true,
			slideMargin: 30,
		});
		$(".testimonial-next").click(function(){
			slider.goToNextSlide();
		});
		$(".testimonial-prev").click(function(){
			slider.goToPrevSlide();
		});
	});

	//MASK INPUT
	$(".user_phone").mask("+38(999) 999-9999");

	//FORM VALIDATION + AJAX SEND
	function get_cookie ( cookie_name ){
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}

	var href = document.location.href;
	var new_url = href.split('?')[1];
	var id = 'Ski';
	var url = href.split('?')[0];
	var utm_catch = '&' + new_url + "&page_url=" + url;
	var ref = '&ref=' + document.referrer;
	var leade_name = "&lead_name=" + id;
	var lead_price = "&lead_price=" + $('#price').html();
	var invite_id = "&invite_id="+href.split('invite_id=')[1];
	var cook_ga;
	var hmid;
			// присваиваем hmid и ложим в базу
	$(document).ready(function(){
		var cookie_check = setInterval(function(){
			var ga = get_cookie('_ga');
			if (ga === null) {
				console.log('111');
			}
			else {
				cook_ga = "&_ga="+get_cookie('_ga');
				var hmid = "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				// var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
				var page_url = "&page_url="+url;
				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+"."+temp_month+"." +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				var data = hmid+utm_catch+invite_id+date_submitted+time_submitted+ref;
				clearInterval(cookie_check);
				$.ajax({
							type: "GET",
							url: "visits.php",
							data: data,
							success: function() {
					console.log('done');
							}
						});
			}
		} ,500);
	});
	$.validate({
		form : 'form',
		addValidClassOnAll : true,
		onSuccess : function($form){
			// var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
			// $form.find('#geoloc').val(loc);
			var data = $form.serialize();
			var data_form = $form.attr('data-form');
			var temp_date = new Date();
			var temp_month = temp_date.getMonth();
			temp_month++;
			var date_submitted = '&date_submitted=' +temp_date.getDate()+"."+temp_month+"." +temp_date.getFullYear();
			var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
			data += utm_catch;
			data += date_submitted;
			data += time_submitted;
			data += ref;
			data += cook_ga;
			data += leade_name;
			data += '&data_form=' + data_form;
			data += '&hmid=' + hmid;
			$.ajax({
				type: "GET",
				url: "register_mail.php",
				data: data,
				beforeSend: function() {
					$form.find('button').prop( "disabled", true ).text('');
					$form.find('.spinner').fadeIn();
				}
			});
			$.ajax({
				type: "POST",
				url:"amo/amocontactlist.php",
				data: data,
				success: function() {
					$form.find('.spinner').fadeOut();
					setTimeout(function() {
						$form.find('button').text('✔ Отправлено');
					}, 350);
					// dataLayer.push({'event': 'FormSumit_main', 'form_type': data_form});
				}
			});
		return false;
		}
	});
});
