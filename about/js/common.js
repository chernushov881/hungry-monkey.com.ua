$(function() {

//ANIMATION ANIMATE.CSS
	$('.life-full-item-col').animated('fadeInUpSmall');
	$('header h1').animated('fadeInRightBig');
	$('header h2').animated('fadeInLeftBig');
	$('header .nav-wrapper').animated('fadeInDownBig');
	$('header .header-btn-wrap').animated('fadeInUpBig');
	$('.why-block, .wait-col, .video').animated('fadeIn');
	$('.wait-calendar-wrap').animated('bounce');


	//PLYR Player video
	var player = plyr.setup('.plyr-player')[0].plyr;

		//MASK INPUT
	$(".user_phone").mask("+38(099) 999-9999");

	// OWL CARoUSELLE
	$(document).ready(function(){
		var owl = $(".testimonials-owl");
		// var owl = $(".video-container, .testimonials-owl");
		var owl_testimonials = $(".testimonials-owl");
		owl.owlCarousel({
				items: 1,
				slideSpeed : 600,
				pagination: true,
				addClassActive: true,
				loop: true,
				smartSpeed: 600,
				fluidSpeed: 600,
				navSpeed: 600,
				dotsSpeed: 600,
				dragEndSpeed: 600
			});
		$(".testimonials-prev").click(function(){
			owl_testimonials.trigger('prev.owl.carousel');
		});
		$(".testimonials-next").click(function(){
			owl_testimonials.trigger('next.owl.carousel');
		});
	});

	//CUSTOM JS
	$('#menuToggle').click(function(){
		$('nav ul').toggleClass('active-menu');
		$('.mask').fadeToggle(300);
		// $('body').toggleClass('stop-scrolling');
		// if($('nav ul').hasClass('active-menu')){
		// 	$('body').bind('touchmove', function(e){e.preventDefault()});
		// 	// $('body').css({'padding-right': '17px'});
		// }else{
		// 	$('body').unbind('touchmove');
		// 	// $('body').css({'padding-right': '0px'});
		// }
	});

	$('.footer-sub-btn, .header-btn').click(function(){
		$('.pop-up-form-mask').fadeIn();
	});
	$('.close-pop-btn').click(function(){
		$('.pop-up-form-mask').fadeOut();
	});
	$('.close-video-btn').click(function(){
		$('.video-wrapper').fadeOut();
		player.pause();
	});


	// $('.video-icon').click(function(){
	// 	var new_video_src = $(this).parent('.video-item').attr('data-video-id');
	// 	var new_video_tittle = $(this).parent('.video-item').attr('data-video-title');
	// 	$('.video-wrapper').fadeIn();
	// 	player.source({
	// 			type: 'video',
	// 			title: new_video_tittle,
	// 			autoplay: true,
	// 			hideControls: true,
	// 			clickToPlay: true,
	// 			showPosterOnEnd: true,
	// 			sources: [{
	// 					src: new_video_src,
	// 					type: 'youtube'
	// 			}]
	// 	});
	// });

	//LABELS FORM
	$('.form__container input').change(function(){
		if( $(this).val() !== '') {
			$(this).closest('.form__container').addClass('has-data');
		};
		if( $(this).val() == '') {
			$(this).closest('.form__container').removeClass('has-data');
		};
	});
	$(document).mouseup(function(e) {
		if (!$(e.target).closest(".nav-wrapper").length) {
			$('nav ul').removeClass('active-menu');
			$('#menuToggle input').prop("checked", false);
			$('.mask').fadeOut(300);
			$('body').unbind('touchmove');
			$('body').removeClass('stop-scrolling');
			$('body').css({'padding-right': '0px'});
		};
		if (!$(e.target).closest('.main-form').length) {
			$('.pop-up-form-mask').fadeOut();
		};
	});

		//FORM VALIDATION + AJAX SEND
	function get_cookie ( cookie_name ){
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}

	var href = document.location.href;
	var new_url = href.split('?')[1];
	var id = 'main';
	var url = href.split('?')[0];
	var utm_catch = '&' + new_url + "&page_url="+url;
	var ref = '&ref=' + document.referrer;
	var leade_name = "&lead_name=" + id;
	var lead_price = "&lead_price=" + $('#price').html();
	var invite_id = "&invite_id="+href.split('invite_id=')[1];
	var cook_ga;
	var hmid;
	var hmid_link;

			// присваиваем hmid и ложим в базу
	$(document).ready(function(){

		var cookie_check = setInterval(function(){
			var ga = get_cookie('_ga');
			if (ga === null) {
				console.log('111');
			}
			else {
				cook_ga = "&_ga="+get_cookie('_ga');
				hmid = "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				// var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
				var page_url = "&page_url="+url;
				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				var id = "practice";
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				var data = hmid+utm_catch+invite_id+date_submitted+time_submitted+ref;
				clearInterval(cookie_check);
				$.ajax({
							type: "GET",
							url: "https://hungry-monkey.com.ua/visits.php",
							data: data,
							success: function() {
					console.log('done');
							}
						});
			}
		} ,500);
	});
	$.validate({
		form : 'form',
		addValidClassOnAll : true,
		borderColorOnError: "#eca29b",
		onSuccess : function($form){
			// var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
			// $form.find('#geoloc').val(loc);
			var data = $form.serialize();
			var data_form = $form.attr('data-form');
			var temp_date = new Date();
			var temp_month = temp_date.getMonth();
			temp_month++;
			var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
			var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
			data += utm_catch;
			data += date_submitted;
			data += time_submitted;
			data += ref;
			data += cook_ga;
			data += leade_name;
			data += hmid;
			data += '&data_form=' + data_form;
			if (data_form == 'letter') {
				$.ajax({
					type: "GET",
					url: "https://hungry-monkey.com.ua/register_mail_letter.php",
					data: data,
					beforeSend: function() {
						$form.find('button').prop( "disabled", true ).text('');
						$form.find('.spinner').fadeIn();
					},
					success: function() {
						console.log('ok letter!');
						$form.find('.spinner').fadeOut();
						setTimeout(function() {
							$form.find('button').text('✔ Подписка оформлена');
						}, 350);
						// dataLayer.push({'event': 'FormSumit_main', 'form_type': data_form});
					}
				});
				return false;
			}
			$.ajax({
				type: "GET",
				url: "https://hungry-monkey.com.ua/register_mail.php",
				data: data,
				beforeSend: function() {
					$form.find('button').prop( "disabled", true ).text('');
					$form.find('.spinner').fadeIn();
				}
			});
			$.ajax({
				type: "POST",
				url:"https://hungry-monkey.com.ua/amo/amocontactlist.php",
				data: data,
				success: function() {
					// console.log('ok!');
					$form.find('.spinner').fadeOut();
					setTimeout(function() {
						$form.find('button').text('✔ Отправлено');
					}, 350);
					dataLayer.push({'event': 'FormSumit_main', 'form_type': data_form});
				}
			});
		// setTimeout(function() {
		// 			window.location.href = "thanks.html";
		// 	}, 1500);

		return false;
		}
	});
});
