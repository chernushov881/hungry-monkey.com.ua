$(function() {

	//ANIMATION ANIMATE.CSS
	$('header nav').animated('fadeInDownBig');
	// $('.calendar-col').animated('fadeInRight');
	// $('.list li').animated('fadeInUp');
	// $('.list .img-container').animated('fadeInLeft');
	// $('.list .list-btn-wrap').animated('fadeInUp');
	// $('.list h2, .list h3').animated('fadeInRight');
	// $('.subscribe').animated('rollIn');

	//CUSTOM JS
	$('#menuToggle').click(function(){
		$('nav ul').toggleClass('active-menu');
		$('.mask').fadeToggle(300);
		// $('body').toggleClass('stop-scrolling');
		// if($('nav ul').hasClass('active-menu')){
		// 	$('body').bind('touchmove', function(e){e.preventDefault()});
		// 	// $('body').css({'padding-right': '17px'});
		// }else{
		// 	$('body').unbind('touchmove');
		// 	// $('body').css({'padding-right': '0px'});
		// }
	});

		//MASKED INPUT
	$("#phone").mask("+38(999) 999-9999");

	//Download calendar events
	function loadCalendar() {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', 'js/calendar.json', true);
		xhr.send();
		xhr.onreadystatechange = function () {
			if(xhr.readyState != 4) return;
			if(xhr.status != 200) {
				alert(xhr.status + ': ' + xhr.statusText);
			}else{
				try{
					var events = JSON.parse(xhr.responseText);
					showCalendar(events);
				}catch(e){
					alert("Некорректный ответ " + e.message);
				}
			}
		};
	}

	function showCalendar(events) {
		var calendarWrap, calendarEl, eventRemove, eventDate, eventStart, eventLocation, eventTitle, nowDate, i;
		nowDate = Date.now();
		i = 1;
		calendarWrap = document.getElementById('calendar__wrap');
		events.forEach(function(event){
			eventRemove = Date.parse(event.eventRemove.replace(/(..)\/(..)\/(....)/, '$2/$1/$3'));
			eventDate = event.eventDate;
			eventStart = event.eventStart.replace(/ /ig, ' <br>');
			eventLocation = event.eventLocation;
			eventTitle = event.eventTitle;
			if(nowDate < eventRemove){
				if(event.eventLink === ""){
					calendarEl = calendarWrap.insertBefore(document.createElement('div'), calendarBtn);
					// calendarEl.href = '/404/';
				}else{
					calendarEl = calendarWrap.insertBefore(document.createElement('a'), calendarBtn);
					calendarEl.href = event.eventLink;
					calendarEl.setAttribute('target', '_blank');
				}
				if(i < 11){
					calendarEl.className = 'calendar__el';
				}else{
					calendarEl.className = 'calendar__el calendar__el-hidden';
				}
				calendarEl.innerHTML = '<div class="calendar__start"><p><span>' + eventStart + '</span></p></div><div class="calendar__date"><span>' + eventLocation + '</span><span>' + eventDate + '</span></div><div class="calendar__loc"><p>' + eventTitle + '</p></div>';
				i++;
			}
		});
	}

	var calendarBtn = document.getElementById('calendar__more');
	calendarBtn.addEventListener('click', function(){
		calendarBtn.parentNode.removeChild(calendarBtn);
		var calendarEls = document.getElementsByClassName('calendar__el-hidden');
		calendarEls = [].slice.call(calendarEls);
		calendarEls.forEach(function(calendarEls){
			calendarEls.classList.remove('calendar__el-hidden');
		});
	});

	loadCalendar();

		//FORM VALIDATION + AJAX SEND
	function get_cookie ( cookie_name ){
		var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

		if ( results )
			return ( unescape ( results[2] ) );
		else
			return null;
	}

	var href = document.location.href;
	var new_url = href.split('?')[1];
	var id = 'main';
	var url = href.split('?')[0];
	var utm_catch = '&' + new_url + "&page_url="+url;
	var ref = '&ref=' + document.referrer;
	var leade_name = "&lead_name=" + id;
	var lead_price = "&lead_price=" + $('#price').html();
	var invite_id = "&invite_id="+href.split('invite_id=')[1];
	var cook_ga;
	var hmid;
	var hmid_link;

			// присваиваем hmid и ложим в базу
	$(document).ready(function(){

		var cookie_check = setInterval(function(){
			var ga = get_cookie('_ga');
			if (ga === null) {
				console.log('111');
			}
			else {
				cook_ga = "&_ga="+get_cookie('_ga');
				hmid = "&hmid=" + cook_ga.split('.')[2]+'.'+cook_ga.split('.')[3];
				// var loc = '&city='+ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
				var page_url = "&page_url="+url;
				var temp_date = new Date();
				var temp_month = temp_date.getMonth();
				var id = "practice";
				temp_month++;
				var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
				var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
				var data = hmid+utm_catch+invite_id+date_submitted+time_submitted+ref;
				clearInterval(cookie_check);
				$.ajax({
							type: "GET",
							url: "visits.php",
							data: data,
							success: function() {
					console.log('done');
							}
						});
			}
		} ,500);
	});
	$.validate({
		form : 'form',
		addValidClassOnAll : true,
		borderColorOnError: "#eca29b",
		onSuccess : function($form){
			// var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
			// $form.find('#geoloc').val(loc);
			var data = $form.serialize();
			var data_form = $form.attr('data-form');
			var temp_date = new Date();
			var temp_month = temp_date.getMonth();
			temp_month++;
			var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
			var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();
			data += utm_catch;
			data += date_submitted;
			data += time_submitted;
			data += ref;
			data += cook_ga;
			data += leade_name;
			data += hmid;
			data += '&data_form=' + data_form;
			$.ajax({
				type: "GET",
				url: "register_mail_letter.php",
				data: data,
				beforeSend: function() {
					$form.find('button').prop( "disabled", true );
					$form.find('.spinner').fadeIn();
				},
				success: function() {
					// console.log('ok letter!');
					$form.find('.spinner').fadeOut();
					setTimeout(function() {
						$form.find('button').text('✔ Подписка оформлена');
					}, 350);
					// dataLayer.push({'event': 'FormSumit_main', 'form_type': data_form});
				}
			});
			return false;
			}
	});
});
