
// GEOLOCATION
$('form button').click(function(){
  var loc = ymaps.geolocation.city+', '+ymaps.geolocation.region+', '+ymaps.geolocation.country;
  $("#geoloc").val(loc);
});
$(document).ready(function(){
  
});

//FORM VALIDATION + AJAX SEND
function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
 
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

var href = document.location.href;
var new_url = href.split('?')[1];
var url = href.split('?')[0];
var utm_catch = '&' + new_url + "&page_url="+url;
var ref = '&ref=' + document.referrer;
var cook_ga = "&_ga="+get_cookie('_ga');
console.log(cook_ga);
$.validate({
  form : 'form',
  addValidClassOnAll : true,
  borderColorOnError: "#eca29b ",
  onSuccess : function($form){
       if ($form.attr('id') == 'main_form' ) {
        var data = $("#main_form").serialize();
        var urlData = "?" + $("#main_form").serialize();
        var temp_date = new Date();
        var temp_month = temp_date.getMonth();
        temp_month++;
        var date_submitted = '&date_submitted=' +temp_date.getDate()+" "+temp_month+" " +temp_date.getFullYear();
        var time_submitted = '&time_submitted=' +temp_date.getHours() + ":" +temp_date.getMinutes();

        data += utm_catch;
        data += date_submitted;
        data += time_submitted;
        data += ref;
        data += cook_ga;
        $("#main_form").find('.form__button').addClass('disabled').prop('disabled', true);
	      	$.ajax({
	      		type: "POST",
	      		url:"amocontactlist.php",
	      		data: data
	      	});
          $.ajax({
          type: "POST",
          url: "register_mail.php",
          data: data,
          success: function() {
            setTimeout(function() {
            window.location.href = "thanks.html";
            }, 1500);
          }
        });
      return false;
      }
  }

});

$(document).ready(function(){
  var name = function(){
    var a = href.split('name=')[1];
    var b =a.split('&')[0];
    b=decodeURIComponent(b);
    return b;
  }; 
  var phone = function(){
    var a = href.split('phone=%2B')[1];
    var b =a.split('&')[0];
    var c = b.split('+')[0] + "-" + b.split('+')[1];
    return c;
  };
  var mail = function(){
    var a = href.split('mail=')[1];
    var b =a.split('&')[0];
    var c =b.split('%')[0]+"@"+b.split('%')[1];
    return c;
  };
  var form = $('#main_form');
  $(form).find('input[name="name"]').val(name());
  $(form).find('input[name="phone"]').val(phone());
  $(form).find('input[name="mail"]').val(mail());

});
//HM ID

//ANIMATION 
/*$(document).ready(function(){
	function isVisible(e) {
	    var wh = $(window).height() + $(window).scrollTop(); 
	    var el = $(e);
	    var eh = el.offset().top ;
	    if (eh < wh) {
	      return true;
	    }  
	}
	if (!$('body').hasClass('thankyou-page')) {
		if(isVisible('.main-footer')) {
			$('.main-footer').addClass('visible');
		}
		if(isVisible('.first-screen__container')) {
			$('.container__list, .first-screen__title').addClass('visible');
		}
		if(isVisible('.second-screen')) {
			$('.first-screen__banner').addClass('visible');
		}
		if(isVisible('.second-screen__container')) {
			$('.second-screen').addClass('visible');
		}
		if(isVisible('.header__col.book_col')) {
			$('.header__col.book_col, .header__title').addClass('visible');
			setTimeout(function(){
				$('.header__list').addClass('visible');
			},400);
			setTimeout(function(){
				$('.header__title_big').addClass('visible');
			},800);
			setTimeout(function(){
				$('.header__description').addClass('visible');
			},600);
			setTimeout(function(){
				$('.header__button').addClass('visible');
			},1300);
			setTimeout(function(){
				$('.button__container').addClass('visible');
			},1200);
		}
	}
	
	if (!$('body').hasClass('thankyou-page')) {
		$(window).scroll(function(){
			if(isVisible('.main-footer')) {
				$('.main-footer').addClass('visible');
			}
			if(isVisible('.first-screen__container')) {
				$('.container__list, .first-screen__title').addClass('visible');
			}
			if(isVisible('.second-screen')) {
				$('.first-screen__banner').addClass('visible');
			}
			if(isVisible('.second-screen__container')) {
				$('.second-screen').addClass('visible');
			}
			if(isVisible('.header__col.book_col')) {
				$('.header__col.book_col, .header__title').addClass('visible');
				setTimeout(function(){
					$('.header__list li:nth-child(1)').addClass('visible');
				},200);
				setTimeout(function(){
					$('.header__list li:nth-child(2)').addClass('visible');
				},400);
				setTimeout(function(){
					$('.header__list li:nth-child(3)').addClass('visible');
				},600);
				setTimeout(function(){
					$('.header__description').addClass('visible');
				},800);
				setTimeout(function(){
					$('.header__button').addClass('visible');
				},1300);
				setTimeout(function(){
					$('.button__container').addClass('visible');
				},1200);
			}
	});
	}
});
*/
//Плавный скролл до блока по клику
$("#header_button").click(function() {
	var offset = $('.main-footer').offset();
	$("html, body").animate({
	  scrollTop:offset.top
	}, 500);
	return false;
});
//MASKED INPUT
jQuery(function($){
$(".user_phone").mask("+38(099) 999-9999");
});


//  PARALAX
$(document).ready(function(){
	$('header').mousemove(function(e){

		//header 

		var x = -(e.pageX ) / 45;
		var y = -(e.pageY) / 45;
		var x2 = (e.pageX ) / 45;
		var y2 = (e.pageY) / 45;
		var x3 = -(e.pageX ) / 48;
		var y3 = -(e.pageY) / 48;
		var x4 = (e.pageX ) / 51;
		var y4 = (e.pageY) / 51;

		$('.header__cloud1').css({
			"transform" :"translate("+x+"px, "+y+"px)"
		});
		$('.header__cloud2').css({
			"transform" :"translate("+x2+"px, "+y2+"px)"
		});
		$('.header__cloud3').css({
			"transform" :"translate("+x3+"px, "+y3+"px)"
		});
		$('.header__cloud4').css({
			"transform" :"translate("+x4+"px, "+y4+"px)"
		});


		
		
	});

	$('.third-section, footer').mousemove(function(e){
		//footer

		var x = -(e.pageX ) / 45;
		var y = -(e.pageY) / 45;
		var x2 = (e.pageX ) / 45;
		var y2 = (e.pageY) / 45;
		var x3 = -(e.pageX ) / 48;
		var y3 = -(e.pageY) / 48;
		var x4 = (e.pageX ) / 51;
		var y4 = (e.pageY) / 51;

		$('.parallax__sun').css({
			"transform" :"translate("+(x-5)+"px, "+(y+60)+"px)"
		});

		$('.parallax_rock1').css({
			"transform" :"translate("+x3+"px, "+(y3+60)+"px)"
		});

		$('.parallax_rock2').css({
			"transform" :"translate("+x4+"px, "+(y4-60)+"px)"
		});
	});

});
